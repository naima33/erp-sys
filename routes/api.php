<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CategoryController;
use App\Models\CategoryModel;
use App\Http\Controllers\employeesController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\ExportToPdf;

use App\Http\Controllers\WarehouseController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ReceiptNoteController;
use App\Http\Controllers\AccountingController;
use App\Http\Controllers\AssetController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware'=>['auth:sanctum']],function()
{
    Route::get('AllCategory','App\Http\Controllers\CategoryController@GetCatogory');
    Route::post('AddCategoryPost','App\Http\Controllers\CategoryController@AddCategory');
    Route::get('logout1','App\Http\Controllers\employeesController@logout');
});

//Route::get('AllCategory','App\Http\Controllers\CategoryController@GetCatogory');
Route::view('AddCategoryView', 'AddItemCategory'); //not neededd
//Route::post('AddCategoryPost','App\Http\Controllers\CategoryController@AddCategory')->middleware('api-session');

Route::get('EditCategory/{id}','App\Http\Controllers\CategoryController@EditCategory'); //not needed
Route::post('UpdateCategory','App\Http\Controllers\CategoryController@UpdateCategory')->middleware('api-session');

Route::get('GetItemInCategory/{id}','App\Http\Controllers\CategoryController@GetItemInCategory')->middleware('api-session');

Route::get("SearchCategory/{name}",'App\Http\Controllers\CategoryController@SearchCategory')->middleware('api-session');


// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/register1',function(){
    return view('register');
});

Route::post('register2', 'App\Http\Controllers\employeesController@register')->middleware('api-session');

Route::get('/login1',function(){
    return view('login');
});
Route::post('login2', 'App\Http\Controllers\employeesController@login')->middleware('api-session');


Route::post('update','App\Http\Controllers\employeesController@update')->middleware('api-session');


/////items///
Route::post('addItem', 'App\Http\Controllers\itemsController@addItem')->middleware('api-session');
Route::get('showItem/{id}', 'App\Http\Controllers\itemsController@show')->middleware('api-session');
Route::put('updateItem', 'App\Http\Controllers\itemsController@update')->middleware('api-session');
Route::get('searchItem/{name}', 'App\Http\Controllers\itemsController@search')->middleware('api-session');
Route::get('getAllItem','App\Http\Controllers\itemsController@getAllItem')->middleware('api-session');
Route::get('Items','App\Http\Controllers\itemsController@Items');
//Route::get('getId', 'App\Http\Controllers\CategoryController@getId')->mid-dleware('api-session');

Route::Post('AddFriend/{id}','App\Http\Controllers\employeesController@AddFriend');
Route::get('AllFriend/{id}','App\Http\Controllers\employeesController@AllFriend');
Route::get('SearchFriend/{id}/{name}','App\Http\Controllers\employeesController@SearchFriends');

Route::post('SendMess/{id}','App\Http\Controllers\MessagingController@SendMessage');
Route::post('TransferOrder/{s}/{r}','App\Http\Controllers\MessagingController@Issuing_transfer_order');
Route::get('GetMessage/{MyId}/{FriendId}','App\Http\Controllers\MessagingController@GetMessage');
Route::get('Get_order','App\Http\Controllers\MessagingController@Get_order');

Route::post('DeliverAfterTransfer/{to_warehouse}','App\Http\Controllers\TransferController@deliver_to_after_transfer');
Route::post('ReceiptAfterTransfer/{from_warehouse}','App\Http\Controllers\TransferController@receipt_from_after_transfer');

Route::post('DeliverItems','App\Http\Controllers\DeliveryController@deliver_Items');
Route::get('OutputReport','App\Http\Controllers\DeliveryController@OutputReport');
Route::get('GetOutputs','App\Http\Controllers\DeliveryController@GetOutputs');
Route::get('InputReport','App\Http\Controllers\DeliveryController@InputReport');
Route::get('GetInputs','App\Http\Controllers\DeliveryController@GetInputs');
Route::get('PrintDeliveryNote/{id}','App\Http\Controllers\DeliveryController@PrintDeliveryNote');
Route::get('PrintBorrowNote/{id}','App\Http\Controllers\DeliveryController@PrintBorrowNote');
Route::get('PrintTransferNote/{id}/{type}','App\Http\Controllers\DeliveryController@PrintTransferNote');
Route::get('GetDeliveryNote/{id}','App\Http\Controllers\DeliveryController@GetDeliveryNote');
Route::get('GetBorrowNote/{id}','App\Http\Controllers\DeliveryController@GetBorrowNote');
Route::get('GetTransferNote/{id}/{type}','App\Http\Controllers\DeliveryController@GetTransferNote');

Route::get('/PrintReceiptNote/{id}', [ReceiptNoteController::class, 'PrintReceiptNote']);
//Route::get('PrintDeliveryNote/{id}','App\Http\Controllers\DeliveryController@PrintDeliveryNote');

Route::get('GetAllDelivery','App\Http\Controllers\DeliveryController@GetAllDelivery');
Route::get('getAllEntries','App\Http\Controllers\DeliveryController@getAllEntries');
Route::get('getAllBorrow','App\Http\Controllers\DeliveryController@getAllBorrow');
Route::get('getBorrowInf/{id}','App\Http\Controllers\DeliveryController@getBorrowInf');
Route::get('getAllTransfer','App\Http\Controllers\DeliveryController@getAllTransfer');
Route::post('BorrowItems','App\Http\Controllers\DeliveryController@Borrow_Items');


//Saja

//Role Management

Route::get('/GetAllRoles', [RoleController::class, 'GetAllRoles'])->middleware('api-session');
Route::post('/CreateRole', [RoleController::class, 'CreateRole'])->middleware('api-session');
Route::get('/GetRole/{id}', [RoleController::class, 'GetRole'])->middleware('api-session');
Route::post('/EditRole/{id}', [RoleController::class, 'EditRole'])->middleware('api-session');
Route::get('/DeleteRole/{id}', [RoleController::class, 'DeleteRole'])->middleware('api-session');

//Permission Management

Route::get('/GetAllPermissions', [PermissionController::class, 'GetAllPermissions'])->middleware('api-session');
Route::post('/CreatePermission', [PermissionController::class, 'CreatePermission'])->middleware('api-session');
Route::post('/DeletePermissionFromRole', [PermissionController::class, 'DeletePermissionFromRole'])->middleware('api-session');

//Warehouse Management

Route::get('/GetAllWarehouses', [WarehouseController::class, 'GetAllWarehouses'])->middleware('api-session');
Route::post('/CreateWarehouse', [WarehouseController::class, 'CreateWarehouse'])->middleware('api-session');
Route::post('/EditWarehouse/{id}', [WarehouseController::class, 'EditWarehouse'])->middleware('api-session');
Route::get('/DeleteWarehouse/{id}', [WarehouseController::class, 'DeleteWarehouse'])->middleware('api-session');
Route::get('/GetAllItemsInWarehouse/{id}', [WarehouseController::class, 'GetAllItemsInWarehouse'])->middleware('api-session');
Route::get('/GetAllCategoriesInWarehouse/{id}', [WarehouseController::class, 'GetAllCategoriesInWarehouse'])->middleware('api-session');
Route::get('/GetAllItemsInCategoriesInWarehouse/{warehouse_id}/{category_id}', [WarehouseController::class, 'GetAllItemsInCategoriesInWarehouse'])->middleware('api-session');
//get supplier
Route::get('GetAllSupplier','App\Http\Controllers\ReceiptNoteController@GetALlSupplier');
//ReceiptNote Management

Route::get('/GetAllReceiptNotes', [ReceiptNoteController::class, 'GetAllReceiptNotes']);
Route::get('/getReceiptNote/{id}', [ReceiptNoteController::class, 'getReceiptNote']);
Route::get('/PrintReceiptNote/{id}', [ReceiptNoteController::class, 'PrintReceiptNote']);

Route::get('/GetAllReceiptNotesInWarehouse/{id}', [ReceiptNoteController::class, 'GetAllReceiptNotesInWarehouse'])->middleware('api-session');
Route::get('/GetAllBills', [ReceiptNoteController::class, 'GetAllBills'])->middleware('api-session');
Route::get('/GetAllBillsInWarehouse/{id}', [ReceiptNoteController::class, 'GetAllBillsInWarehouse'])->middleware('api-session');
Route::get('/GetAllInstallationRecords', [ReceiptNoteController::class, 'GetAllInstallationRecords'])->middleware('api-session');
Route::get('/GetAllInstallationRecordsInWarehouse/{id}', [ReceiptNoteController::class, 'GetAllInstallationRecordsInWarehouse'])->middleware('api-session');
Route::get('/GetAllContracts', [ReceiptNoteController::class, 'GetAllContracts'])->middleware('api-session');
Route::get('/GetAllContractsInWarehouse/{id}', [ReceiptNoteController::class, 'GetAllContractsInWarehouse'])->middleware('api-session');
Route::get('/GetAllPresents', [ReceiptNoteController::class, 'GetAllPresents'])->middleware('api-session');
Route::get('/GetAllPresentsInWarehouse/{id}', [ReceiptNoteController::class, 'GetAllPresentsInWarehouse'])->middleware('api-session');
Route::post('/CreateReceiptNote', [ReceiptNoteController::class, 'CreateReceiptNote'])->middleware('api-session');
Route::post('/CreateSupplier', [ReceiptNoteController::class, 'CreateSupplier'])->middleware('api-session');
Route::post('/CreateBill', [ReceiptNoteController::class, 'CreateBill'])->middleware('api-session');
Route::post('/CreateContract', [ReceiptNoteController::class, 'CreateContract'])->middleware('api-session');
Route::post('/CreateInstallationRecord', [ReceiptNoteController::class, 'CreateInstallationRecord'])->middleware('api-session');
Route::post('/CreatePresent', [ReceiptNoteController::class, 'CreatePresent'])->middleware('api-session');
Route::post('/EditReceiptNote/{id}', [ReceiptNoteController::class, 'EditReceiptNote'])->middleware('api-session');

//Inventory Management
Route::get('/GetLog/{id}', [InventoryController::class, 'GetLog'])->middleware('api-session');

Route::get('/GetAllCommittees', [InventoryController::class, 'GetAllCommittees'])->middleware('api-session');
Route::get('/GetAllMembersInCommittee/{id}', [InventoryController::class, 'GetAllMembersInCommittee'])->middleware('api-session');
Route::post('/CreateCommittee', [InventoryController::class, 'CreateCommittee'])->middleware('api-session');
Route::post('/EditCommittee/{id}', [InventoryController::class, 'EditCommittee'])->middleware('api-session');
Route::get('/DeleteCommittee/{id}', [InventoryController::class, 'DeleteCommittee'])->middleware('api-session');
Route::get('/GetAllInventories', [InventoryController::class, 'GetAllInventories'])->middleware('api-session');
Route::get('/getInventoryByID/{id}', [InventoryController::class, 'getInventoryByID'])->middleware('api-session');

Route::get('/GetAllItemsInInventory/{id}', [InventoryController::class, 'GetAllItemsInInventory'])->middleware('api-session');
Route::post('/CreateCompletelyInventory', [InventoryController::class, 'CreateCompletelyInventory'])->middleware('api-session');
Route::post('/CreatePartlyInventory', [InventoryController::class, 'CreatePartlyInventory'])->middleware('api-session');
Route::post('/InventoryResult', [InventoryController::class, 'InventoryResult'])->middleware('api-session');
Route::get('/DeleteInventory/{id}', [InventoryController::class, 'DeleteInventory'])->middleware('api-session');
//added by naima
Route::get('/GetInventoryItem', [InventoryController::class, 'GetInventoryItem'])->middleware('api-session');
Route::post('CreateInventory', [InventoryController::class, 'CreateInventory'])->middleware('api-session');

Route::get('/GetEmployeesInWarehouse', [EmployeesController::class, 'GetEmployeesInWarehouse'])->middleware('api-session');
Route::get('/GetAllCommitteesInWarehouse', [InventoryController::class, 'GetAllCommitteesInWarehouse'])->middleware('api-session');

//
//Export to PDF
Route::get('/ExportBill', [ExportToPdf::class, 'ExportBill'])->middleware('api-session');


Route::get('/GetNamesOfEmployees', [EmployeesController::class, 'GetNamesOfEmployees'])->middleware('api-session');

Route::post('/GivingRoleToEmployee', [EmployeesController::class, 'GivingRoleToEmployee'])->middleware('api-session');


//نعيمة
//account
Route::post('AddAccount','App\Http\Controllers\AccountController@AddAccount');
Route::post('UpdateAccount/{res_id}/{name}','App\Http\Controllers\AccountController@UpdateAccount');
Route::get('GetAccountById/{id}','App\Http\Controllers\AccountController@GetAccountById');
Route::get('GetAccountLevelOne','App\Http\Controllers\AccountController@GetAccountLevelOne');
Route::get('GetAccountLevelTwo/{id}','App\Http\Controllers\AccountController@GetAccountLevelTwo');
Route::get('GetAccountLevelThree/{id}','App\Http\Controllers\AccountController@GetAccountLevelThree');
Route::get('GetAccountLevelFour/{id}','App\Http\Controllers\AccountController@GetAccountLevelFour');
Route::get('GetAccountLevelFive/{id}','App\Http\Controllers\AccountController@GetAccountLevelFive');

Route::get('GetAccountLevelFiveAll','App\Http\Controllers\AccountController@GetAccountLevelFiveAll');
//currency
Route::post('AddCurrency','App\Http\Controllers\CurrencyController@addCurrency');
Route::get('DeleteCurrency/{id}','App\Http\Controllers\CurrencyController@DeleteCurrency');
Route::get('getAllCurrency','App\Http\Controllers\CurrencyController@getAllCurrency');
Route::post('UpdateCurrency/{id_currency}/{exchange}','App\Http\Controllers\CurrencyController@UpdateCurrency');
Route::get('getCurrencyByDate','App\Http\Controllers\CurrencyController@showCurrencyByDate');
Route::get('printCurrencyByDate','App\Http\Controllers\CurrencyController@printCurrencyByDate');

//restriction
Route::get('ShowNon_Migrated_Restrictions/{id}','App\Http\Controllers\restrictionController@ShowNon_Migrated_Restrictions');
Route::get('Non_Migrated_Restrictions/{id}','App\Http\Controllers\restrictionController@Non_Migrated_Restrictions');
Route::post('add_restriction','App\Http\Controllers\restrictionController@add_restriction');
Route::get('showRestriction/{id}','App\Http\Controllers\restrictionController@showRestriction');
Route::post('RestrictionUpdate','App\Http\Controllers\restrictionController@RestrictionUpdate');
Route::get('Migrate_Restriction/{id}','App\Http\Controllers\restrictionController@Migrate_Restriction');

//income_list
Route::get('income_list_print/{id}','App\Http\Controllers\income_listController@income_list_print');
Route::get('CreateIncome_list/{id}','App\Http\Controllers\income_listController@income_list_create');
Route::get('income_list_get/{id}','App\Http\Controllers\income_listController@income_list_get');

/////Naima
Route::get('getAllTransferReceipt','App\Http\Controllers\DeliveryController@getAllTransferReceipt');
//statement_list
Route::get('CreateStatement_financial_list/{id}','App\Http\Controllers\Statement_financial_positionController@Create_Statement_financial');
Route::get('Print_Statement_financial/{id}','App\Http\Controllers\Statement_financial_positionController@Print_Statement_financial');
Route::get('get_Statement_financial/{id}','App\Http\Controllers\Statement_financial_positionController@get_Statement_financial');


Route::get('financial_analysis/{one}/{two}',
'App\Http\Controllers\Statement_financial_positionController@financial_Analayses');

Route::get('financial_Analayses_create/{one}/{two}',
'App\Http\Controllers\Statement_financial_positionController@financial_Analayses_create');

//Fund_Movement
Route::get('Fund_Movement_show','App\Http\Controllers\AccountController@Fund_Movement_show');
Route::get('Fund_Movement_print','App\Http\Controllers\AccountController@Fund_Movement_print');

//close dummy account
Route::get('ShowDummyAccount/{id}','App\Http\Controllers\AccountController@ShowDummyAccount');
Route::get('CloseDummyAccount/{Fisca_id}/{ledger_id}','App\Http\Controllers\AccountController@CloseDummyAccount');

// Route::get('count_balance/{Fisca_id}','App\Http\Controllers\AccountController@count_balance');


//Saja Accounting

Route::get('/GetAllFiscalPeriod', [AccountingController::class, 'GetAllFiscalPeriod'])->middleware('api-session');
Route::get('/GetRestrictionsInFiscalPeriod/{id}', [AccountingController::class, 'GetRestrictionsInFiscalPeriod'])->middleware('api-session');
Route::get('/GetStateOfTheRestriction/{id}', [AccountingController::class, 'GetStateOfTheRestriction'])->middleware('api-session');
Route::post('/CreateTrialBalance/{F_id}', [AccountingController::class, 'CreateTrialBalance'])->middleware('api-session');
Route::post('/DivTheFiscalPeriod', [AccountingController::class, 'DivTheFiscalPeriod'])->middleware('api-session');


Route::get('/log', function (){return addLog();})->middleware('api-session');

//Asset Management

Route::get('/GetAllCategoriesOfAssetInWarehouse/{id}', [AssetController::class, 'GetAllCategoriesOfAssetInWarehouse'])->middleware('api-session');


Route::get('PrintBill/{id}', [ReceiptNoteController::class, 'PrintBill'])->middleware('api-session');
Route::get('PrintContract/{id}', [ReceiptNoteController::class, 'PrintContract'])->middleware('api-session');
Route::get('PrintInstallationRecord/{id}', [ReceiptNoteController::class, 'PrintInstallationRecord'])->middleware('api-session');
Route::get('PrintPresent/{id}', [ReceiptNoteController::class, 'PrintPresent'])->middleware('api-session');


Route::get('GetBillById/{id}', [ReceiptNoteController::class, 'GetBillById'])->middleware('api-session');
Route::get('GetPresentById/{id}', [ReceiptNoteController::class, 'GetPresentById'])->middleware('api-session');
Route::get('GetInstallationById/{id}', [ReceiptNoteController::class, 'GetInstallationById'])->middleware('api-session');
Route::get('GetContractById/{id}', [ReceiptNoteController::class, 'GetContractById'])->middleware('api-session');

Route::get('getContracts', [ReceiptNoteController::class, 'getContracts'])->middleware('api-session');
Route::get('getInstallations', [ReceiptNoteController::class, 'getInstallations'])->middleware('api-session');
Route::get('getPresents', [ReceiptNoteController::class, 'getPresents'])->middleware('api-session');
Route::get('getBills', [ReceiptNoteController::class, 'getBills'])->middleware('api-session');
Route::get('accountPage/{Acc_id}/{fis_id}','App\Http\Controllers\AccountController@ShowAccountPageInLedger');
Route::get('count_balance/{acc_id}/{Fis_id}','App\Http\Controllers\AccountController@count_balance');
//naima
//employee getempolyees
Route::get('getempolyees/{id}','App\Http\Controllers\employeesController@getempolyees');
Route::get('getempolyees','App\Http\Controllers\employeesController@getempolyees');

// Notifications
Route::get('notifications','App\Http\Controllers\NotificationController@getNotifications')->middleware('api-session');

Route::get('DeleteAccount/{acc_id}','App\Http\Controllers\AccountController@DeleteAccount');


Route::get('GetAlCommittees', [InventoryController::class, 'GetAlCommittees'])->middleware('api-session');

//Route::post('UpdateCurrency/{id}','App\Http\Controllers\CurrencyController@UpdateCurrency');

Route::get('/login', function () {
    return view('log_in');
});
