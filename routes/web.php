<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Models\CategoryModel;

use Illuminate\Http\Request;

use App\Http\Controllers\employeesController;
use App\Http\Controllers\ItemsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('AllCategory',[CategoryController::class,'GetCatogory']);
Route::view('AddCategoryView', 'AddItemCategory'); //not neededd
Route::post('AddCategoryPost',[CategoryController::class,'AddCategory']);

Route::get('EditCategory/{id}',[CategoryController::class,'EditCategory']); //not needed
Route::post('UpdateCategory',[CategoryController::class,'UpdateCategory']);

Route::get('GetItemInCategory/{id}',[CategoryController::class,'GetItemInCategory']);


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/register1',function(){
    return view('register');
});
Route::post('register2', 'App\Http\Controllers\employeesController@register');
Route::get('/login1',function(){
    return view('login');
});
Route::post('login2', 'App\Http\Controllers\employeesController@login');

Route::get('/logout1','App\Http\Controllers\employeesController@logout');
Route::get('/update','App\Http\Controllers\employeesController@update');


/////items///
Route::post('addItem', 'App\Http\Controllers\itemsController@addItem');
Route::get('showItem', 'App\Http\Controllers\itemsController@show');
Route::put('updateItem', 'App\Http\Controllers\itemsController@update');
