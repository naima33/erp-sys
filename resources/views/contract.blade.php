
<div style="display:inline-block;">
    <div style="display:inline-block;">الجمهورية العربية السورية</div>
    <img src="logo.jpg"  style="text-align: left; width:80px; height:80px;">
</div>


<div style="text-align: center;">عقد شراء</div>

<br><br>
<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
        <th style="border: 1px solid black;border-collapse: collapse">رقم المذكرة</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">رقم الأمر</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">رقم العقد</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">المادة</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">السعر</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">الكمية</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">اسم المستودع</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">التاريخ</th>
    </tr>
    @foreach ($results as $result)
        <tr>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['note_id']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['order_id']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['contract_id']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['item_name']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['price1']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['quantity']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['warehouse_name']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['date']}}</td>
        </tr>
    @endforeach


</table>

<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
        <th style="border: 1px solid black;border-collapse: collapse">تاريخ إنتهاء صلاحية العقد</th>
    </tr>
    @foreach ($results as $result)
        <tr>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['end_date']}}</td>
        </tr>
    @endforeach

</table>
