<div style="text-align: center;">التغيرات في صرف الليرة السورية</div>

<br><br>
<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
      <th style="border: 1px solid black;border-collapse: collapse">العملة الرئيسية</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">العملة الأخرى</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">القيمة</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">التاريخ</th>
    </tr>
    @foreach ($currency as $curr)
    <tr>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center; "> {{$curr['base']}} </td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$curr['other']}}</td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$curr['exchange_rate']}}</td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$curr['date']}}</td>

    </tr>
    @endforeach


  </table>
