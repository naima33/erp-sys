<!DOCTYPE html>
<html>
  <head>
    <title>Login Page</title>
    <script src="https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.9.1/firebase-messaging.js"></script>
    <script>
        var firebaseConfig = {
            apiKey: "AIzaSyC7hCEZC6nWUFeEMgnsxF-KJ1QtSnxXSuo",
            authDomain: "grad-test-ac06e.firebaseapp.com",
            projectId: "grad-test-ac06e",
            storageBucket: "grad-test-ac06e.appspot.com",
            messagingSenderId: "576993711119",
            appId: "1:576993711119:web:e5c2346ee90977e700ff7a",
            measurementId: "G-VSBCG2CW8S"
        };
        firebase.initializeApp(firebaseConfig);
        firebase.messaging().getToken().then((currentToken) => {
  if (currentToken) {
    console.log("Firebase registration token: " + currentToken);
  } else {
    console.log("No registration token available.");
  }
}).catch((error) => {
  console.log("An error occurred while retrieving the Firebase registration token: " + error);
});
      </script>
  </head>
  <body>
    <h1>Login Page</h1>
    <form action="login.php" method="POST">
      <label for="username">Username:</label>
      <input type="text" id="username" name="username" required><br>

      <label for="password">Password:</label>
      <input type="password" id="password" name="password" required><br>

      <input type="submit" value="Login">
    </form>
  </body>
</html>
