<!DOCTYPE html>
<html>
<head>
    <style>
        table, th, td {
            font-family: aealarabiya, sans-serif;
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: right;
        }
    </style>
</head>
<body>

<h2  style="text-align:center;">قيمة المبيعات</h2>

<table style="width:100%;">
    <tr>
        <th>الكمية</th>
        <th>الشهر</th>
    </tr>
    <tr>
        <td>2000</td>
        <td>1</td>
    </tr>
    <tr>
        <td>10000</td>
        <td>2</td>
    </tr>
    <tr>
        <td>10000</td>
        <td>3</td>
    </tr>
    <tr>
        <td>10000</td>
        <td>4</td>
    </tr>
    <tr>
        <td>50000</td>
        <td>5</td>
    </tr>
    <tr>
        <td>10000</td>
        <td>6</td>
    </tr>
    <tr>
        <td>6000</td>
        <td>7</td>
    </tr>
    <tr>
        <td>1000</td>
        <td>8</td>
    </tr>
    <tr>
        <td>65000</td>
        <td>9</td>
    </tr>
    <tr>
        <td>765</td>
        <td>10</td>
    </tr>
    <tr>
        <td>10000</td>
        <td>11</td>
    </tr>
    <tr>
        <td>998823</td>
        <td>12</td>
    </tr>

</table>

</body>

</html>
