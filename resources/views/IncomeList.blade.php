<?php
use App\Models\CurrencyModel;
use App\Models\credit_entriesModel;
use App\Models\debit_entriesModel;
use App\Models\accountModel;
?>
<div style="display:inline-block;">
    <div style="display:inline-block;">الجمهورية العربية السورية</div>
    <img src="logo.jpg"  style="text-align: left; width:80px; height:80px;">
</div>

<div style="text-align: center;"> قائمة الدخل </div>

<br><br>
<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
      <th style="border: 1px solid black;border-collapse: collapse">الفترة المالية </th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">تاريخ بداية الفترة </th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">تاريخ نهاية الفترة </th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">الدائنين</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">المدينين</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">مجموع الدائنين </th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">مجموع المدينين </th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">النتيجة  </th>
    </tr>

    <tr>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['fiscal_period_name']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['from_date']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['to_date']}}</td>

        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">

        <table>
        @foreach ($results['expences'] as $ex)
        <tr style="border: 1px solid black;border-collapse: collapse;text-align: center; ">
           <?php
            $account_name=accountModel::where('id',$ex->account_id)->first();
           ?>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center; "> {{$account_name->name}} </td>
        </tr>
         @endforeach
        </table>
        </td>

        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">
        <table>
            @foreach ($results['revenue'] as $rev)
            <tr style="border: 1px solid black;border-collapse: collapse;text-align: center; ">
           <?php
            $account_name=accountModel::where('id',$rev->account_id)->first();
           ?>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center; "> {{$account_name->name}} </td>
            </tr>
         @endforeach
        </table>
        </td>

        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['total_revenue']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['total_expences']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['result']}}</td>
    </tr>



  </table>
