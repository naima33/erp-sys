<div style="display:inline-block;">
    <div style="display:inline-block;">الجمهورية العربية السورية</div>
    <img src="logo.jpg"  style="text-align: left; width:80px; height:80px;">
</div>


<div style="text-align: center;">تقرير عن حركة الصندوق</div>
<br><br>
<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
      <th style="border: 1px solid black;border-collapse: collapse">الاسم</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">نوع الحساب</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">القيمة</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">الحساب الآخر</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">البيان</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">التاريخ</th>
    </tr>
    @foreach ($Fund as $fund)
    <tr>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center; "> {{$fund['namebase']}} </td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$fund['type']}}</td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$fund['amount']}}</td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$fund['other account']}}</td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$fund['explain_procces']}}</td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$fund['date']}}</td>
    </tr>
    @endforeach


  </table>
