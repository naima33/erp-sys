<div style="display:inline-block;">
    <div style="display:inline-block;">الجمهورية العربية السورية</div>
    <img src="logo.jpg"  style="text-align: left; width:80px; height:80px;">
</div>


<div style="text-align: center;">تحليل المالي لمقارنة قائمتي المركز المالي للفترتين</div>

<div style="text-align: center;">{{$result['base_year']}} & {{$result['other_year']}}</div>

<br><br>
<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
      <th style="border: 1px solid black;border-collapse: collapse">{{'          '}}</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['first_year']}}</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['second_year']}}</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">الفرق</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">التغير</th>
    </tr>
    <tr>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">إجمالي الخصوم</td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['totalCB']}}</td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['totalCo']}}</td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['changeC']}}</td>
      <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['credit']}}</td>
    </tr>
    <tr>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">إجمالي الأصول</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['totalDB']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['totalDo']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['changeD']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['debit']}}</td>
      </tr>
  </table>
