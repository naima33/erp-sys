
<div   style="overflow: hidden;background-color: red;width=20 ;height=20">
    <div style="margin: 2px;float:right;background-color: green;width: 10;height : 100%;">الجمهورية العربية السورية</div>
    <img src="logo.jpg" alt="Italian Trulli" style="float: left;width: 100%;height : 100%;object-fit: cover;">
</div>


<div style="display:inline-block;">
    <div style="display:inline-block;">الجمهورية العربية السورية</div>
    <img src="logo.jpg"  style="text-align: left; width:80px; height:80px;">
</div>

<div style="text-align: center;">منحة</div>

<br><br>
<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
        <th style="border: 1px solid black;border-collapse: collapse">رقم المذكرة</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">رقم الأمر</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">رقم المنحة</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">المادة</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">الكمية</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">اسم المستودع</th>
        <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">التاريخ</th>
    </tr>
    @foreach ($results as $result)
        <tr>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['note_id']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['order_id']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['present_id']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['item_name']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['quantity']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['warehouse_name']}}</td>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['date']}}</td>
        </tr>
    @endforeach


</table>

<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
        <th style="border: 1px solid black;border-collapse: collapse">الجهة المانحة</th>
    </tr>
    @foreach ($results as $result)
        <tr>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['presenter']}}</td>
        </tr>
    @endforeach

</table>
