// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');
/*
Initialize the Firebase app in the service worker by passing in the messagingSenderId.
*/
firebase.initializeApp({
    apiKey: "AIzaSyDNlRn6x9rPA70V2-8RCB1QTsELXCL11LA",
    authDomain: "warehouse1-5475f.firebaseapp.com",
    projectId: "warehouse1-5475f",
    storageBucket: "warehouse1-5475f.appspot.com",
    messagingSenderId: "960445979374",
    appId: "1:960445979374:web:46dcafc9598e39735af1ff",
    measurementId: "G-NCT7SLVDWM"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
// const messaging = firebase.messaging();
// messaging.setBackgroundMessageHandler(function(payload) {
//     console.log("Message received.", payload);
//     const title = "Hello world is awesome";
//     const options = {
//         body: "Your notificaiton message .",
//         icon: "/firebase-logo.png",
//     };
//     return self.registration.showNotification(
//         title,
//         options,
//     );
// });



