<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemBorrowModel extends Model
{
    use HasFactory;
    protected $table="item borrowing";
    public $timestamps=false;

}
