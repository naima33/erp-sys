<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class restrictionModel extends Model
{
    use HasFactory;
    protected $table="restriction";
    public $timestamps=false;
}
