<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receipt_noteModel extends Model
{
    use HasFactory;
    protected $table="receipt note";
    public $timestamps=false;
}
