<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class debit_entriesModel extends Model
{
    use HasFactory;
    protected $table="debit_entries";
    public $timestamps=false;

}
