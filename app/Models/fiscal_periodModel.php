<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fiscal_periodModel extends Model
{
    use HasFactory;
    protected $table="fiscal_period";
    public $timestamps=false;
}
