<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statement_financial_positionModel extends Model
{
    use HasFactory;
    protected $table="statement_of_financial_position";
    public $timestamps=false;
}
