<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class credit_entriesModel extends Model
{
    use HasFactory;
    protected $table="credit_entries";
    public $timestamps=false;
}
