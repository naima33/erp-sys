<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class debit_listModel extends Model
{
    use HasFactory;
    protected $table="debit_list";
    public $timestamps=false;
}
