<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class EmployeesModel extends Authenticatable
{
    use HasFactory,HasApiTokens,Notifiable;
    protected $table="employee";
    public $timestamps=false;
    protected $fillable = [
        'f_name',
        'l_name',
        'phone',
        'email',
        'salary',
        'role_id',
        'password',
        'address',
        'emp_statu',
        'warehouse_id',
    ];
}
