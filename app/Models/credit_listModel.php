<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class credit_listModel extends Model
{
    use HasFactory;
    protected $table="credit_list";
    public $timestamps=false;
}
