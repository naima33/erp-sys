<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class historical_exchangeModel extends Model
{
    use HasFactory;
    protected $table="historical _exchange";
    public $timestamps=false;
}
