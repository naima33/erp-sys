<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    use HasFactory;

    protected $table="category";
    public $timestamps=false;
    // public function items()
    // {
    //     return $this->hasMany('App\Models\item','category_id','id');
    // }
}
