<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery_noteModel extends Model
{
    use HasFactory;
    protected $table="delivery note";
    public $timestamps=false;
}
