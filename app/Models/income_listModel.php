<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class income_listModel extends Model
{
    use HasFactory;
    protected $table="income_list";
    public $timestamps=false;
}
