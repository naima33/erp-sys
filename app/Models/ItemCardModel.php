<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemCardModel extends Model
{
    use HasFactory;
    protected $table="item_card";
    public $timestamps=false;
}
