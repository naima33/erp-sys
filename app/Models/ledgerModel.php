<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ledgerModel extends Model
{
    use HasFactory;
    protected $table="ledger";
    public $timestamps=false;
}
