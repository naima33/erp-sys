<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransferToWarehouseModel extends Model
{
    use HasFactory;
    protected $table="transfer to warehouse";
    public $timestamps=false;
}
