<?php

use Illuminate\Support\Facades\DB;

function addLog($type , $emp_id , $process){

    DB::table('log')->insertGetId(
        ['type' => $type , 'date' => \Carbon\Carbon::now(),'emp_id' => $emp_id,'process' => $process  ]
    );

}
