<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CurrencyModel;
use App\Models\historical_exchangeModel;
use AmrShawky\LaravelCurrency\Facade\Currency;
use Carbon\Carbon;
class CurrencyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Command:Currency';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $currency=CurrencyModel::all();
        $size=count($currency);
        for($i=0;$i<$size;$i++)
        {
        $result= Currency::convert()
        ->from($currency[$i]->code)
        ->to('SYP')
        ->amount(1)
        ->get();
        $currency1=CurrencyModel::find($currency[$i]->id);
        $currency1->exchange_rate=$result;
        $currency1->save();
        $Hitory=new historical_exchangeModel();
        $Hitory->id_currency1=$currency[$i]->id;
        $Hitory->exchange_rate=$result;
        $Hitory->date=Carbon::now();
        $Hitory->save();
        }

        return response()->json('UPDATEd THE CURRENCY');
    }
}
