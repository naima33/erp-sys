<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FriendModel;
use App\Models\EmployeesModel;
use App\Models\NotificationModel;
use Carbon\Carbon;
use App\Models\User;

class MessagingController extends Controller
{
    // public function SendMessage(Request $req, $id)
    // {
    //     //permission
    //     $token = $req->bearerToken();
    //     $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
    //     if ($emp1 == null)
    //         return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
    //     else {
    //         // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
    //         // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
    //         //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

    //         $emp_id = $emp1->id;

    //         //log
    //         addLog("create",$emp_id,"SendMessage");
    //         $receiver_name = $req->friend_name;
    //         $receiver = FriendModel::select('id')->where('user_id', $id)->where('friend_name', $receiver_name)->first();
    //         if ($receiver != null) {

    //             $data = [
    //                 'title' => $req->title,
    //                 'message' => $req->message,
    //             ];
    //             $rec_id = EmployeesModel::select('id')->where('f_name', '=', $receiver_name)->first();

    //             $notification = new NotificationModel();
    //             //$notification->sender_id=2;
    //             $notification->sender_id = $id;
    //             $notification->receiver_id = $rec_id->id;
    //             $notification->message = $req->message;
    //             $notification->title = $req->title;
    //             $notification->type = $req->type;
    //             $notification->date = $req->date;
    //             if ($req->pic != null) {
    //                 $file_extension = $req->pic->getClientOriginalExtension();
    //                 $file_name = time() . '.' . $file_extension;
    //                 $path = 'images/messaging';
    //                 $req->pic->move($path, $file_name);
    //                 $notification->pic = $file_name;
    //             }
    //             $notification->date = Carbon::now();

    //             if ($notification->save()) {
    //                 // echo " saved ";
    //                 $url = 'https://fcm.googleapis.com/fcm/send';

    //                 // $token =$device_token->device_token;
    //                 $token = 'dsFajIsI69yk1vhrOE4iaY:APA91bFXfcShQX7ptwIq5jyZqJxFEqpiM6v7aIxljLzPTSsKb6TFyUhwrW8w1MWvA1OlYnqpk2L5lgS3FpmJezO6GwzWqXX6hCR6vbDGbIG7YNfBZuzGm37_HGaXparJZTyWkPcBKKE1';
    //                 $from = 'AAAA358Kmu4:APA91bHfA3xGCD_hLILdhVHD5KDhaHbwqPT98Kw3Oj5Yv6-ivQuAGN8KqCz_YFTXUQX8BQBiOoIa_U7e2Sr_A8ax-m8z43_3ZWAJNyUaKmtXVt4xRgoV6iOJgeje8CPTaMhjvuhveDmc'; // ADD SERVER KEY HERE PROVIDED BY FCM

    //                 $msg = array(
    //                     'body'  => $data['message'],
    //                     'title' => $data['title'],
    //                     'receiver' => 'erw',
    //                     'icon'  => "https://image.flaticon.com/icons/png/512/270/270014.png",/*Default Icon*/
    //                     'sound' => 'mySound'/*Default sound*/
    //                 );

    //                 $fields = array(
    //                     'to' => $token,
    //                     'notification' => $msg
    //                 );

    //                 $headers = array(
    //                     'Authorization: key=' . $from,
    //                     'Content-Type: application/json'
    //                 );

    //                 $encodedData = json_encode($fields);
    //                 $ch = curl_init();

    //                 curl_setopt($ch, CURLOPT_URL, $url);
    //                 curl_setopt($ch, CURLOPT_POST, true);
    //                 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    //                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //                 curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    //                 curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    //                 // Disabling SSL Certificate support temporarly
    //                 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //                 curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
    //                 // Execute post
    //                 $result = curl_exec($ch);
    //                 if ($result === FALSE) {
    //                     die('Curl failed: ' . curl_error($ch));
    //                 }
    //                 // Close connection
    //                 curl_close($ch);
    //                 // FCM response
    //                 // echo $result;
    //                 //  return response()->json("notification send");
    //                 return response()->json(['status' => 'success', 'message' => 'notification send']);
    //             }
    //         } else

    //             return response()->json(['status' => 'error', 'message' => 'add the reciever first']);
    //     }
    // }
    public function SendMessage(Request $req, $id)
    {
        //permission
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("create",$emp_id,"SendMessage");
            $receiver_name = $req->friend_name;
            $receiver = FriendModel::select('id')->where('user_id', $id)->where('friend_email', $receiver_name)->first();
            if ($receiver != null) {

                $data = [
                    'title' => $req->title,
                    'message' => $req->message,
                ];
                $rec_id = EmployeesModel::select('id')->where('email', '=', $receiver_name)->first();

                $notification = new NotificationModel();
                //$notification->sender_id=2;
                $notification->sender_id = $id;
                $notification->receiver_id = $rec_id->id;
                $notification->message = $req->message;
                $notification->title = $req->title;
                $notification->type = $req->type;
                $notification->date = $req->date;
                if ($req->pic != null) {
                    $file_extension = $req->pic->getClientOriginalExtension();
                    $file_name = time() . '.' . $file_extension;
                    $path = 'images/messaging';
                    $req->pic->move($path, $file_name);
                    $notification->pic = $file_name;
                }
                $notification->date = Carbon::now();
                $notification->save();
//                if ($notification->save()) {
//                    // echo " saved ";
//                    $url = 'https://fcm.googleapis.com/fcm/send';
//
//                    // $token =$device_token->device_token;
//                    $token = 'dsFajIsI69yk1vhrOE4iaY:APA91bFXfcShQX7ptwIq5jyZqJxFEqpiM6v7aIxljLzPTSsKb6TFyUhwrW8w1MWvA1OlYnqpk2L5lgS3FpmJezO6GwzWqXX6hCR6vbDGbIG7YNfBZuzGm37_HGaXparJZTyWkPcBKKE1';
//                    $from = 'AAAA358Kmu4:APA91bHfA3xGCD_hLILdhVHD5KDhaHbwqPT98Kw3Oj5Yv6-ivQuAGN8KqCz_YFTXUQX8BQBiOoIa_U7e2Sr_A8ax-m8z43_3ZWAJNyUaKmtXVt4xRgoV6iOJgeje8CPTaMhjvuhveDmc'; // ADD SERVER KEY HERE PROVIDED BY FCM
//
//                    $msg = array(
//                        'body'  => $data['message'],
//                        'title' => $data['title'],
//                        'receiver' => 'erw',
//                        'icon'  => "https://image.flaticon.com/icons/png/512/270/270014.png",/*Default Icon*/
//                        'sound' => 'mySound'/*Default sound*/
//                    );
//
//                    $fields = array(
//                        'to' => $token,
//                        'notification' => $msg
//                    );
//
//                    $headers = array(
//                        'Authorization: key=' . $from,
//                        'Content-Type: application/json'
//                    );
//
//                    $encodedData = json_encode($fields);
//                    $ch = curl_init();
//
//                    curl_setopt($ch, CURLOPT_URL, $url);
//                    curl_setopt($ch, CURLOPT_POST, true);
//                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//                    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
//                    // Disabling SSL Certificate support temporarly
//                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//                    curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
//                    // Execute post
//                    $result = curl_exec($ch);
//                    if ($result === FALSE) {
//                        die('Curl failed: ' . curl_error($ch));
//                    }
//                    // Close connection
//                    curl_close($ch);
//                    // FCM response
//                    // echo $result;
//                    //  return response()->json("notification send");
//                    return response()->json(['status' => 'success', 'message' => 'notification send']);
//                }
                return response()->json(['status' => 'success', 'message' => 'notification send']);
            } else

                return response()->json(['status' => 'error', 'message' => 'add the reciever first']);
        }
    }
    public function Issuing_transfer_order(Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("create",$emp_id,"Issuing_transfer_order");


            $data = [
                'title' => $req->title,
                'message' => $req->message,
            ];
            // echo " ".$name->name." ";
            // event(new ItemEvent($data));
            $notification = new NotificationModel();
            //$notification->sender_id=2;
            $notification->sender_id = $req->sender;
            $notification->receiver_id = $req->receiver;
            $notification->message = $req->message;
            $notification->title = $req->title;
            $notification->type = $req->type;
            $notification->date = $req->date;
            $notification->date = Carbon::now();
            $notification->save();
            // if ($notification->save()) {
            //     //echo " saved ";
            //     $url = 'https://fcm.googleapis.com/fcm/send';

            //     // $token =$device_token->device_token;
            //     $token = 'dsFajIsI69yk1vhrOE4iaY:APA91bFXfcShQX7ptwIq5jyZqJxFEqpiM6v7aIxljLzPTSsKb6TFyUhwrW8w1MWvA1OlYnqpk2L5lgS3FpmJezO6GwzWqXX6hCR6vbDGbIG7YNfBZuzGm37_HGaXparJZTyWkPcBKKE1';
            //     $from = 'AAAA358Kmu4:APA91bHfA3xGCD_hLILdhVHD5KDhaHbwqPT98Kw3Oj5Yv6-ivQuAGN8KqCz_YFTXUQX8BQBiOoIa_U7e2Sr_A8ax-m8z43_3ZWAJNyUaKmtXVt4xRgoV6iOJgeje8CPTaMhjvuhveDmc'; // ADD SERVER KEY HERE PROVIDED BY FCM

            //     $msg = array(
            //         'body'  => $data['message'],
            //         'title' => $data['title'],
            //         'receiver' => 'erw',
            //         'icon'  => "https://image.flaticon.com/icons/png/512/270/270014.png",/*Default Icon*/
            //         'sound' => 'mySound'/*Default sound*/
            //     );

            //     $fields = array(
            //         'to' => $token,
            //         'notification' => $msg
            //     );

            //     $headers = array(
            //         'Authorization: key=' . $from,
            //         'Content-Type: application/json'
            //     );

            //     $encodedData = json_encode($fields);
            //     $ch = curl_init();

            //     curl_setopt($ch, CURLOPT_URL, $url);
            //     curl_setopt($ch, CURLOPT_POST, true);
            //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            //     curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            //     // Disabling SSL Certificate support temporarly
            //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            //     curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
            //     // Execute post
            //     $result = curl_exec($ch);
            //     if ($result === FALSE) {
            //         die('Curl failed: ' . curl_error($ch));
            //     }
            //     // Close connection
            //     curl_close($ch);
            //     // FCM response
            //     // echo $result;
            //     // return response()->json("notification send");
            // }
            // $data = [
            //     'title' => $req->title . ' 2 ',
            //     'message' => $req->message,
            // ];
            // if ($notification->save()) {

            //     $url = 'https://fcm.googleapis.com/fcm/send';

            //     // $token =$device_token->device_token;
            //     $token = 'dsFajIsI69yk1vhrOE4iaY:APA91bFXfcShQX7ptwIq5jyZqJxFEqpiM6v7aIxljLzPTSsKb6TFyUhwrW8w1MWvA1OlYnqpk2L5lgS3FpmJezO6GwzWqXX6hCR6vbDGbIG7YNfBZuzGm37_HGaXparJZTyWkPcBKKE1';
            //     $from = 'AAAA358Kmu4:APA91bHfA3xGCD_hLILdhVHD5KDhaHbwqPT98Kw3Oj5Yv6-ivQuAGN8KqCz_YFTXUQX8BQBiOoIa_U7e2Sr_A8ax-m8z43_3ZWAJNyUaKmtXVt4xRgoV6iOJgeje8CPTaMhjvuhveDmc'; // ADD SERVER KEY HERE PROVIDED BY FCM

            //     $msg = array(
            //         'body'  => $data['message'],
            //         'title' => $data['title'],
            //         'receiver' => 'erw',
            //         'icon'  => "https://image.flaticon.com/icons/png/512/270/270014.png",/*Default Icon*/
            //         'sound' => 'mySound'/*Default sound*/
            //     );

            //     $fields = array(
            //         'to' => $token,
            //         'notification' => $msg
            //     );

            //     $headers = array(
            //         'Authorization: key=' . $from,
            //         'Content-Type: application/json'
            //     );

            //     $encodedData = json_encode($fields);
            //     $ch = curl_init();

            //     curl_setopt($ch, CURLOPT_URL, $url);
            //     curl_setopt($ch, CURLOPT_POST, true);
            //     curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            //     curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            //     // Disabling SSL Certificate support temporarly
            //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            //     curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);
            //     // Execute post
            //     $result = curl_exec($ch);
            //     if ($result === FALSE) {
            //         die('Curl failed: ' . curl_error($ch));
            //     }
            //     // Close connection
            //     curl_close($ch);
            //     // FCM response
            //     // echo $result;
            //     return response()->json(['status' => 'success', 'message' => 'notification send']);
            // }
            return response()->json(['status' => 'success', 'message' => 'notification send']);
        }
    }

    public function GetMessage(Request $req, $MyId, $FriendId)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $mess = NotificationModel::where('sender_id', $MyId)->OrWhere('receiver_id', $MyId)
                ->where('sender_id', $FriendId)->OrWhere('receiver_id', $FriendId)->get();
            return response()->json(['status' => 'success', 'message' => $mess]);
        }
    }
}
