<?php

namespace App\Http\Controllers;

use App\Models\EmployeesModel;
use Illuminate\Http\Request;
use App\Models\NotificationModel;

class NotificationController extends Controller
{
    public function getNotifications(Request $req) {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','warehouse_id')->where('token', $token)->first();
        $notifications = NotificationModel::where('receiver_id', $emp->id)->orwhere('sender_id', $emp->id)
        ->orwhere('sender_id', $emp->warehouse_id)->orwhere('receiver_id', $emp->warehouse_id)
        ->get();
        return response()->json([
            'status' => 'success',
            'notifications' => $notifications
        ]);
    }
}
