<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ItemModel;
use App\Models\Delivery_noteModel;
use App\Models\EmployeesModel;
use App\Models\ItemCardModel;
use App\Models\Receipt_noteModel;
use App\Models\TransferModel;
use App\Models\TransferToWarehouseModel;
use App\Models\WarehouseModel;
use Carbon\Carbon;
use App\Models\User;

class TransferController extends Controller
{
    public function deliver_to_after_transfer(Request $req,$to_warehouse)
    {
        //permission

        // $emp_id=session()->get('id');
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            //log
            // addLog("create",$emp_id,"deliver_to_after_transfer");


            $emp_id = $emp1->id;
            // echo $emp_id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            //echo $warehouse_id->warehouse_id;
            if (count($req->input('item_name')) < 1) {
                return response()->json(['status' => "error", 'message' => "من فضلك اختر قيمة"]);
            }
            for ($i = 0; $i < count($req->input('item_name')); $i++) {
                $item_name = $req->input('item_name')[$i];

                // $item_id = ItemModel::select('id')->where('name', $item_name)->first();

                $item_in = ItemCardModel::select('quantity', 'current_quantity')->where('item_id', $item_name)
                    ->where('type', 'input')->where('warehouse_id', $warehouse_id->warehouse_id)
                    ->where('current_quantity', '>', '0')
                    ->get();
                $size = count($item_in);

                $sum_in = 0;

                for ($j = 0; $j < $size; $j++) {
                    $sum_in = $sum_in + $item_in[$j]->current_quantity;
                }

                $curr_quantity = $sum_in;
                if ($curr_quantity < $req->input('quantity')[$i]) {
                    // return response()->json(['error'=>'quantity of the item '.$item_name. ' is not enough']);
                    return response()->json(['status' => 'error', 'message' => 'quantity of the item ' . $item_name . ' is not enough']);
                }

                $sum_in = 0;
                $sum_out = 0;
            }

            $delivery_note = new Delivery_noteModel();
            $delivery_note->date = Carbon::now();
            $delivery_note->type = 'transfer';
            $delivery_note->order_id = $req->order_id;
            $delivery_note->save();
            $del_id = $delivery_note->id;

            for ($i = 0; $i < count($req->input('item_name')); $i++) {
                $item_name = $req->input('item_name')[$i];
                $item_id = ItemModel::select('id', 'code')->where('name', $item_name)->first();
                $item_card = new ItemCardModel();
                $item_card->item_id = $item_id->id;
                $item_card->type = "output";
                $item_card->date = Carbon::now();
                $item_card->code = $item_id->code;
                $item_card->quantity = $req->input('quantity')[$i];
                $item_card->del_id = $del_id;


                $item_card->warehouse_id = $warehouse_id->warehouse_id;
                $item_card->save();
            }

            $trasfer_to = new TransferToWarehouseModel();
            $trasfer_to->to_warehouse_id = $to_warehouse;
            $trasfer_to->id_delivery_note = $del_id;
            $trasfer_to->status = 1;
            if ($trasfer_to->save())
                return response()->json(['status' => 'success', 'message' => 'items have been delivered to the warehouse '.$to_warehouse]);
        }
    }

    public function receipt_from_after_transfer(Request $req,$from_warehouse)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("create",$emp_id,"receipt_from_after_transfer");


            $receipt_note = new Receipt_noteModel();
            $receipt_note->date = Carbon::now();
            $receipt_note->type = 'transfer';
            $receipt_note->order_id = $req->order_id;
            $receipt_note->save();
            //return $delivery_note->id;
            $rec_id = $receipt_note->id;
            for ($i = 0; $i < count($req->input('item_name')); $i++) {
                $item_name = $req->input('item_name')[$i];
                $item_id = ItemModel::select('id', 'code')->where('name', $item_name)->first();
                $item_card = new ItemCardModel();
                $item_card->item_id = $item_id->id;
                $item_card->type = "input";
                $item_card->date = Carbon::now();
                $item_card->code = $item_id->code;
                $item_card->quantity = $req->input('quantity')[$i];
                $item_card->current_quantity = $req->input('quantity')[$i];
                $item_card->rec_id = $rec_id;
                //$emp_id=session()->get('id');
                $emp_id = $emp1->id;
               // echo $emp_id;
                $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
               // echo $warehouse_id->warehouse_id;
                $item_card->warehouse_id = $warehouse_id->warehouse_id;
                $item_card->save();
            }
            $transfer = new TransferModel();
            //$emp_id=session()->get('id');
            $emp_id = $emp1->id;
           // echo $emp_id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            //echo $warehouse_id->warehouse_id;

            $transfer->from_warehouse_id = $from_warehouse;
            $transfer->note_id = $rec_id;
            $transfer->status = 1;
            if ($transfer->save())
                return response()->json(['status' => 'success', 'message' => 'items have been delivered from the warehouse '.$from_warehouse]);
        }
    }

}
