<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AccountModel;
use App\Models\User;
use App\Models\fiscal_periodModel;
use App\Models\EmployeesModel;
use App\Models\ledgerModel;
use App\Models\CurrencyModel;
use Carbon\Carbon;
use Elibyy\TCPDF\Facades\TCPDF;
use PDF;
use Illuminate\Support\Facades\View;

class AccountController extends Controller
{
    public function AddAccount(Request $req)
    {

        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }

            //log
            addLog("create", $emp_id, "AddAccount");

            $account = new AccountModel();
            $account->name = $req->name;
            $account->type = $req->type;
            $account->level_number = $req->level_number;
            $acc = AccountModel::select('id')->where('name', $req->father_name)->first();
            $account->father_id = $acc->id;
            if ($account->save())
                return response()->json(['status' => 'success', 'message' => 'account is added']);
            else
                return response()->json(['status' => 'error', 'message' => 'account is  not added']);
        }
    }

    public function GetAccountById(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $account = AccountModel::where('id', $id)->get();
            return response()->json(['status' => 'success', 'message' => $account]);
        }
    }

    public function GetAccountLevelOne(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $account = AccountModel::where('level_number', 1)->get();
            return response()->json(['status' => 'success', 'message' => $account]);
        }
    }

    public function GetAccountLevelTwo(Request $req, $F_id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $account = AccountModel::where('level_number', 2)
                ->where('father_id', $F_id)->get();
            return response()->json(['status' => 'success', 'message' => $account]);
        }
    }

    public function GetAccountLevelThree(Request $req, $F_id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $account = AccountModel::where('level_number', 3)
                ->where('father_id', $F_id)->get();
            return response()->json(['status' => 'success', 'message' => $account]);
        }
    }

    public function GetAccountLevelFour(Request $req, $F_id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            if($F_id==0)
            {
                $account = AccountModel::where('level_number', 4)->get();
            return response()->json(['status' => 'success', 'message' => $account]);
            }
            else
            {
                $account = AccountModel::where('level_number', 4)
                ->where('father_id', $F_id)->get();
              return response()->json(['status' => 'success', 'message' => $account]);
            }

        }
    }



    public function GetAccountLevelFive(Request $req, $F_id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $account = AccountModel::where('level_number', 5)
                ->where('father_id', $F_id)->get();
            return response()->json(['status' => 'success', 'message' => $account]);
        }
    }

    public function DeleteAccount(Request $req, $Acc_id)
    {
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("delete",$emp_id,"DeleteAccount");
            $Account = new AccountModel();
            if ($Account->find($Acc_id)) {
                $Account->where('id', $Acc_id)->delete();

                return response()->json(['status' => "success", 'message' => "نم حذف الحساب بنجاح"]);
            }

            return response()->json(['status' => "error", 'message' => "هذا الحساب غير موجود"]);

            }
    }
    public function GetAccountLevelFiveAll(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;

            $account = AccountModel::where('level_number', 5)->get();
            return response()->json(['status' => 'success', 'message' => $account]);
        }
    }
    public function UpdateAccount(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }

            //log
            addLog("update", $emp_id, "UpdateAccount");

            $account = AccountModel::find($req->id);
            if ($account != null) {
                $account->name = $req->name;
                $account->type = $req->type;
                $account->level_number = $req->level_number;
                $acc = AccountModel::select('id')->where('name', $req->father_name)->first();
                $account->father_id = $acc->id;
                if ($account->save())
                    return response()->json(['status' => 'success', 'message' => 'account is updated']);
                else
                    return response()->json(['status' => 'error', 'message' => 'account is  not updated']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'account is  not found']);
            }
        }
    }

    public function ShowDummyAccount(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $result = array();
            $fiscal_period = fiscal_periodModel::find($id);
            $AllAcount = ledgerModel::where('fiscal_period_id', $id)->get();
            $size = count($AllAcount);

            for ($i = 0; $i < $size; $i++) {
                $Base_account = AccountModel::where('id', $AllAcount[$i]->account_id)->first();
                // $balance[$i]=$Base_account->balance();
                $j = $Base_account->level_number;
                $account = $Base_account;
                while ($j > 1) {

                    $father = $account->father_id;
                    $account = AccountModel::where('id', $father)->first();
                    $j = $account->level_number;
                }
                $type = $account->name;

                if ($type == 'مصروفات' || $type == 'إيرادات') //مفروض مصروفات و إيرادات
                {
                    // $account1=new AccountModel();
                    $account1 = ledgerModel::join('account as baseAccount', 'ledger.account_id', '=', 'baseAccount.id')
                        ->join('account as other', 'ledger.id_auther_account', '=', 'other.id')
                        ->join('restriction', 'ledger.restriction_id', '=', 'restriction.id')->select(
                            'baseAccount.name as name',
                            'baseAccount.balance',
                            'restriction.date',
                            'baseAccount.type',
                            'restriction.explain_procces',
                            'other.name as other account'
                        )
                        ->where('ledger.account_id', $AllAcount[$i]->account_id)
                        ->get();
                    $result[$i] = $account1;
                }
            }
            return response()->json(['status' => 'success', 'message' => $result]);
        }
    }

    public function CloseDummyAccount(Request $req, $fiscal_id, $ledger_id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }

            //log
            addLog("create", $emp_id, "CloseDummyAccount");

            $fiscal_period = fiscal_periodModel::find($fiscal_id);
            $Now = Carbon::now();
            echo $Now . ' ';
            echo $fiscal_period->to_date . ' ';
            if ($fiscal_period->to_date < $Now) {
                $AllAcount = ledgerModel::where('fiscal_period_id', $fiscal_id)->where('id', $ledger_id)->first();

                $account = AccountModel::find($AllAcount->account_id);
                if ($account != null) {
                    $account->balance = 0;
                    $account->save();
                }
                return response()->json(['status' => 'success', 'message' => 'done']);
            } else
                return "هذه الفترة لم تنتهي بعد";
        }
    }

    public function Fund_Movement(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $fund_id = AccountModel::select('id')->where('name', 'صندوق')->first();
            if ($fund_id != null) {
                $Fund = ledgerModel::join('account as baseAccount', 'ledger.account_id', '=', 'baseAccount.id')
                    ->join('account as other', 'ledger.id_auther_account', '=', 'other.id')
                    ->join('restriction', 'ledger.restriction_id', '=', 'restriction.id')->select(
                        'baseAccount.name as namebase',
                        'ledger.amount',
                        'restriction.date',
                        'baseAccount.type',
                        'restriction.explain_procces',
                        'other.name as other account'
                    )
                    ->where('ledger.account_id', $fund_id->id)
                    ->orWhere('ledger.id_auther_account', $fund_id->id)->get();
                //  return response()->json(['status'=>'success','message'=>$Fund]);
                $filename = 'FundMovement.pdf';
                $view = View::make('FundMovement', ['Fund' => $Fund]);
                $html = $view->render();

                $pdf = new TCPDF;

                $pdf::SetTitle('Fund Movement');
                $pdf::SetFont('aealarabiya', '', 18);
                $pdf::setRTL(true);
                $pdf::AddPage();

                $pdf::writeHTML($html, true, false, true, false, '');

                $pdf::Output(public_path($filename), 'F');

                return response()->download(public_path($filename));
                return response()->json(['status' => 'success', 'message' => $Fund]);
            } else
                return response()->json(['status' => 'success', 'message' => 'done']);
        }
    }

    // public function count_balance(Request $req, $fiscal_id)
    // {
    //     $token = $req->bearerToken();
    //     $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

    //     if ($emp == null) {
    //         return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
    //     } else {
    //         $emp_id = $emp->id;
    //         $debit = 0;
    //         $credit = 0;
    //         $fiscal = fiscal_periodModel::find($fiscal_id);
    //         if ($fiscal != null) {
    //             $AllLedger = ledgerModel::where('fiscal_period_id', $fiscal_id)->get();
    //             $size = count($AllLedger);
    //             for ($i = 0; $i < $size; $i++) {
    //                 $account_id = $AllLedger[$i]->account_id;
    //                 $all = ledgerModel::where('fiscal_period_id', $fiscal_id)->where('account_id', $account_id)->orwhere('id_auther_account', $account_id)->get();
    //                 $sizeACC = count($all);
    //                 for ($j = 0; $j < $sizeACC; $j++) {
    //                     if ($all[$j]->type == 'debit')
    //                         $debit = $debit + $all[$j]->amount;
    //                     else
    //                         $credit = $credit + $all[$j]->amount;
    //                 }
    //                 $sum = 0;
    //                 if ($debit > $credit)
    //                     $sum = $debit - $credit;
    //                 else
    //                     $sum = $credit - $debit;
    //                 $acc = AccountModel::find($account_id);
    //                 if ($acc != null) {
    //                     $acc->balance = $sum;
    //                     if ($acc->save())
    //                         return 'balacing done';
    //                 }
    //             }
    //         } else {
    //             return 'fiscal not found';
    //         }
    //     }
    // }

    public function count_balance(Request $req,$acc_id,$fiscal_id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            $debit = 0;
            $credit = 0;
            $fiscal = fiscal_periodModel::find($fiscal_id);
            if ($fiscal != null) {
                // $AllLedger = ledgerModel::where('fiscal_period_id', $fiscal_id)

                // ->where('account_id',$res_id)->get();
                // $size = count($AllLedger);
                // for ($i = 0; $i < $size; $i++) {
                     $account_id = $acc_id;
                    $all = ledgerModel::where('fiscal_period_id', $fiscal_id)->where('account_id', $acc_id)->orwhere('id_auther_account', $acc_id)->get();
                    $sizeACC = count($all);
                    for ($j = 0; $j < $sizeACC; $j++) {
                        if ($all[$j]->type == 'debit')
                            $debit = $debit + $all[$j]->amount;
                        else
                            $credit = $credit + $all[$j]->amount;
                    }
                    $sum = 0;

                    if ($debit > $credit)
                        $sum = $debit - $credit;
                    else
                        $sum = $credit - $debit;
                    $acc = AccountModel::find($account_id);

                    if ($acc != null) {
                        $acc->balance = $sum;
                        if ($acc->save())
                            return 'balacing done';
                    }
               // }
            } else {
                return 'fiscal not found';
            }
        }
    }

    public function ShowAccountPageInLedger($Acc_id,$fis_id)
    {
         $accountPage=ledgerModel::where('account_id',$Acc_id)
         ->join('account as a1','a1.id','=','ledger.account_id')
         ->join('account as a2','a2.id','=','ledger.id_auther_account')
         ->join('restriction as r','r.id','=','ledger.restriction_id')
         ->select('a1.name as base_account','a2.name as other_account','ledger.account_id as base_account_id','ledger.fiscal_period_id',
         'ledger.type','ledger.amount','ledger.date','r.explain_procces')
         ->where('ledger.fiscal_period_id',$fis_id)->get();

         return $accountPage;
    }

}
