<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use App\Models\User;
use App\Models\WarehouseModel;
use App\Models\EmployeesModel;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class WarehouseController extends Controller
{
    public $roles;
    public $messages;

    public function GetAllWarehouses(Request $req)
    {
        //permission
        // echo session()->get('id');
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض المستودعات')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $warehouse = new WarehouseModel();
            return response()->json($warehouse
                ->join("employee", "employee.warehouse_id", "=", "warehouse.id")
                ->join("roles", "roles.id", "=", "employee.role_id")
                ->where("roles.name", "=", "مدير المستودعات")
                ->Orwhere("roles.name", "=", "مدير النظام")

                ->select("warehouse.id as warehouse_id", "warehouse.name as warehouse_name", "warehouse.address", "warehouse.type", "employee.f_name as manager_f_name", "employee.l_name as manager_l_name")
                ->get());
        }
    }

    public function GetAllCategoriesInWarehouse($id, Request $req)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض الأصناف في كل مستودع')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $warehouse = new WarehouseModel();
            if ($warehouse->find($id)) {
                return response()->json(DB::table('category')
                    ->join("item", "item.category_id", "=", "category.id")
                    ->join("item_card", "item_card.item_id", "=", "item.id")
                    ->where("item_card.warehouse_id", $id)->groupBy('category.name',"category.id")
                    ->select("category.name","category.id")
                    ->get());
            }

            return response()->json(['status' => "error", 'message' => "هذا المستودع غير موجود"]);
        }
    }

    public function GetAllItemsInCategoriesInWarehouse($warehouse_id, $category_id, Request $req)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض المواد في كل مستودع في صنف معين')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $warehouse = new WarehouseModel();
            $category = new CategoryModel();
            if ($warehouse->find($warehouse_id)) {
                if ($category->find($category_id)) {
                    return response()->json(DB::table('warehouse')->join("item_card", "item_card.warehouse_id", "=", "warehouse.id")
                        ->join("item", "item.id", "=", "item_card.item_id")
                        ->join("category", "category.id", "=", "item.category_id")
                        ->where("item_card.warehouse_id", $warehouse_id)
                        ->where("item.category_id", $category_id)
                        ->select("item.name", "item.code", "price", "unit", "item_card.type", "date", "quantity", "rec_id as receipt_note_id")
                        ->get());
                }
                return response()->json(['status' => "error", 'message' => "هذا الصنف غير موجود"]);
            }
            return response()->json(['status' => "error", 'message' => "هذا المستودع غير موجود"]);
        }
    }

    public function GetAllItemsInWarehouse($id, Request $req)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض المواد في كل مستودع')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $warehouse = new WarehouseModel();
            if ($warehouse->find($id)) {
                return response()->json(DB::table('warehouse')->join("item_card", "item_card.warehouse_id", "=", "warehouse.id")
                    ->join("item", "item.id", "=", "item_card.item_id")
                    ->join("category", "category.id", "=", "item.category_id")
                    ->where("item_card.warehouse_id", $id)
                    ->select("item.name", "item.code", "price", "unit", "item_card.type", "date", "quantity", "rec_id as receipt_note_id")
                    ->get());
            }

            return response()->json(['status' => "error", 'message' => "هذا المستودع غير موجود"]);
        }
    }

    public function CreateWarehouse(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("create",$emp_id,"CreateWarehouse");


            //validation
            $validator = $this->CreateValidator($request);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $warehouse = new WarehouseModel();
            $warehouse->name = $request->name;
            $warehouse->address = $request->address;
            $warehouse->type = $request->type;

            if ($warehouse->save())
                return response()->json(['status' => "success", 'message' => "نمت إضافة المستودع بنجاح"]);
            else
                return response()->json(['status' => "error", 'message' => "حدث خطأ ما يرجى إعادة المحاولة"]);
        }
    }

    public function EditWarehouse(Request $request, $id)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("update",$emp_id,"EditWarehouse");


            $warehouse = WarehouseModel::find($id);
            if ($warehouse) {
                //validation
                $validator = $this->EditValidator($request);
                if ($validator->fails()) {
                    return response()->json($validator->errors());
                }

                $warehouse->name = $request->name;
                $warehouse->address = $request->address;
                $warehouse->type = $request->type;

                if ($warehouse->save())
                    return response()->json(['status' => "success", 'message' => "نم تعديل معلومات المستودع بنجاح"]);
                else
                    return response()->json(['status' => "error", 'message' => "حدث خطأ ما يرجى إعادة المحاولة"]);
            }

            return response()->json(['status' => "error", 'message' => "هذا المستودع غير موجود"]);
        }
    }

    public function DeleteWarehouse($id, Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("delete",$emp_id,"DeleteWarehouse");


            $warehouse = new WarehouseModel();
            if ($warehouse->find($id)) {
                $warehouse->where('id', $id)->delete();

                return response()->json(['status' => "success", 'message' => "نم حذف المستودع بنجاح"]);
            }

            return response()->json(['status' => "error", 'message' => "هذا المستودع غير موجود"]);
        }
    }

    public function CreateValidator(Request $request)
    {
        $this->roles = [
            'name' => 'required|unique:warehouse|max:100',
            'address' => 'required|max:100',
            'type' => 'required|max:100',
        ];

        $this->messages = [
            'name.required' => 'حقل الاسم مطلوب',
            'name.unique' => 'هذا الاسم موجود مسبقاً',
            'name.max' => 'حقل الاسم يجب ألا يتجاوز 100 محرف',
            'address.required' => 'حقل العنوان مطلوب',
            'address.max' => 'حقل العنوان يجب ألا يتجاوز 100 محرف',
            'type.required' => 'حقل النوع مطلوب',
            'type.max' => 'حقل النوع يجب ألا يتجاوز 100 محرف',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function EditValidator(Request $request)
    {
        $this->roles = [
            'name' => 'required|max:100',
            'address' => 'required|max:100',
            'type' => 'required|max:100',
        ];

        $this->messages = [
            'name.required' => 'حقل الاسم مطلوب',
            'name.max' => 'حقل الاسم يجب ألا يتجاوز 100 كيلوبايت',
            'address.required' => 'حقل العنوان مطلوب',
            'address.max' => 'حقل العنوان يجب ألا يتجاوز 100 كيلوبايت',
            'type.required' => 'حقل النوع مطلوب',
            'type.max' => 'حقل النوع يجب ألا يتجاوز 100 كيلوبايت',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }
}
