<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CategoryModel;
use App\Models\CurrencyModel;
use App\Models\EmployeesModel;
use AmrShawky\LaravelCurrency\Facade\Currency;
use AshAllenDesign\LaravelExchangeRates\Classes\ExchangeRate;
use App\Models\User;

class CurrencyController extends Controller
{
    public function addCurrency(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }

            //log
            addLog("create",$emp_id,"addCurrency");

            $currency = new CurrencyModel();
            $currency->name = $req->name;
            $currency->code = $req->code;
            $currency->exchange_rate = $req->exchange_rate;
            if ($currency->save())
                return response()->json(['status' => 'success', 'message' => 'تم إضافة العملة بنجاح']);
            else
                return response()->json(['status' => 'success', 'message' => ' لم يتم إضافة العملة بنجاح']);
        }
    }

    public function DeleteCurrency(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }

            //log
            addLog("delete",$emp_id,"DeleteCurrency");

            $currency = CurrencyModel::find($id);
            if ($currency != null) {
                if ($currency->delete())
                    return response()->json(['status' => 'success', 'mesaage' =>'تم حذف العملة بنجاح']);
            } else
                return response()->json(['status' => 'error', 'mesaage' => 'لم يتم إضافة العملة بنجاح']);
        }
    }

    public function getAllCurrency(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $currency = CurrencyModel::all();
            return response()->json(['status' => 'success', 'mesaage' => $currency]);
        }
    }

    public function UpdateCurrency(Request $req,$id,$exchange)
    {
         CategoryModel::find($req->id);
        $curr=CurrencyModel::find($id);
        $curr->exchange_rate=$exchange;
        if($curr->save())
        return response()->json('تم تعديل العملة بنجاح');

        else
        return response()->json(['status' => 'success', 'mesaage' =>'لم يتم تعديل العملة بنجاح']);

    }

    public function getCurrencyByDate(Request $req, $date)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $currency = CurrencyModel::all();
            $size = count($currency);
            for ($i = 0; $i < $size; $i++) {
                $result = Currency::convert()
                    ->from($currency[$i]->code)
                    ->to('EGP')
                    ->date($date)
                    ->amount(1)
                    ->get();
            }
            return response()->json($result);
        }
    }
}
