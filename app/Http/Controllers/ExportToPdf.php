<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

use PDF;

class ExportToPdf extends Controller
{
    public function ExportBill()
    {
        //permission
        if (session()->get('id')==null)
            return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        if (!app('auth')->guard()->getUser()->can('تصدير فاتورة')){
            return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        }

        //$html = '<h1 style="color:red;">مرحباا</h1>';
        $html = view('pdf');

        PDF::SetFont('aealarabiya', '', 18);

        PDF::SetTitle('مرحبا');
        PDF::AddPage();
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::Output('hello_world2.pdf');
    }
}
