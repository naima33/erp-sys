<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\EmployeesModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\RoleController;
use App\Models\FriendModel;
use App\Models\rolesModel;
use App\Models\WarehouseModel;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class EmployeesController extends Controller
{

    public function register(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp->id;

            //log
            addLog("create",$emp_id,"register");



            $emp = EmployeesModel::Create(
                [
                    'f_name' => $req->f_name,
                    'l_name' => $req->l_name,
                    'email' => $req->email,
                    'password' => bcrypt($req->password),
                    'salary' => $req->salary,
                    'role_id' => $req->role_id,
                    'address' => $req->address,
                    'phone' => $req->phone,
                    'warehouse_id' => $req->warehouse_id,
                ]
            );



            $token = $emp->createToken('MyToken')->plainTextToken;
            $response = [
                'emp' => $emp,
                'token' => $token
            ];
            return "تم إضافة الموظف بنجاح";
        }
    }

    // public function login(Request $req)
    // {

    //     $emp = EmployeesModel::where('email', $req->email)->first();
    //     if (!$emp) {
    //         return response('error');
    //     }
    //     $role_name = rolesModel::select('name')->where('id', $emp->role_id)->first();
    //     $token = $emp->createToken('MyToken')->plainTextToken;
    //     $response = [
    //         'id' => $emp->id,
    //         'role' => $role_name->name,
    //         'token' => $token
    //     ];
    //     $emp->token = $token;
    //     $emp->save();
    //     return response($response);
    // }
    public function login(Request $req)
    {

        $emp = EmployeesModel::where('email', $req->email)->first();
        ////Maysaa
        // || !Hash::check($req->password, $emp->password)
        if (!$emp ) {
            return response('error');
        }
        $role_name = rolesModel::select('name')->where('id', $emp->role_id)->first();
        $token = $emp->createToken('MyToken')->plainTextToken;

        $permissions=DB::table('roles')->join('role_has_permissions','role_has_permissions.role_id','roles.id')
            ->join('model_has_roles','model_has_roles.role_id','roles.id')
            ->join('permissions','permissions.id','role_has_permissions.permission_id')
            ->where('model_has_roles.model_id','=',$emp->id)
            ->select('permissions.name')->get();

        $response = [
            'id' => $emp->id,
            'role' => $role_name->name,
            'token' => $token,
            'state' => 'success',
            'permissions' => $permissions
        ];
        $emp->token = $token;
        $emp->save();



        return response($response  );
    }
    protected function getRuleslog()
    {
        return  $rules = [
            'email' => 'required|string|email',
            'password' => 'required|numeric',

        ];
    }

    protected function getMessagelog()
    {
        return  $message = [
            'email.required' => 'email is required',
            'email.string' => 'email must be string',
            'email.email' => 'email must be email',
            'password.required' => 'password is required',
            'password.numeric' => 'password must be numeric',
        ];
    }

    protected function getRulesreg()
    {
        return  $rules = [
            'email' => 'required|string|email|unique:employee,email',
            'password' => 'required|numeric',
            'f_name' => 'required|string',
            'l_name' => 'required|string',
            'address' => 'required',
            'phone' => 'required|numeric',
        ];
    }

    protected function getMessagereg()
    {
        return  $message = [
            'email.required' => 'email is required',
            'email.string' => 'email must be string',
            'email.email' => 'email must be email',
            'email.unique' => 'email must be unique',
            'password.required' => 'password is required',
            'password.numeric' => 'password must be numeric',
            'password.max' => 'password must be more than 8 charecter',
            'f_name.required' => 'first name is required',
            'f_name.string' => 'first name must be string',
            'l_name.required' => 'last name is required',
            'l_name.string' => 'last name must be string',
            'address.required' => 'employee address is required',
            'phone.required' => 'employee phone is required',
            'phone.numeric' => 'employee phone is required',
        ];
    }


    public function logout(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::where('token', $token)->first();
        $emp->token = null;
        $emp->save();

        Auth::user()->tokens()->delete();
        return response('log out');
    }

    public function update(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            $emp_id = $emp->id;

            //log
            addLog("update",$emp_id,"updateEmployee");



            $employees = EmployeesModel::find($request->id);

            $employees->f_name = $request->f_name;
            $employees->l_name = $request->l_name;
            $employees->email = $request->email;
            $employees->password = $request->password;
            $employees->phone = $request->phone;

            $employees->role_id = $request->role_id;

            if ($employees->save())
                return response()->json(["massege" => "informations updated successfully!"]);
        }
    }



    public function rulesup()
    {
        return  $rules = [
            'f_name' => 'required|string|max:100',
            'l_name' => 'required|string|max:100',
            'email' => 'required|string|email|unique:employee,email',
            'password' => 'required|numeric',
            'phone.required' => 'employee phone is required',
            'phone.numeric' => 'employee phone is required',

        ];
    }


    public function messagesup()
    {
        return $message = [
            'f_name.required' => 'first name is required',
            'f_name.string' => 'first name must be string',
            'l_name.required' => 'last name is required',
            'l_name.string' => 'last name must be string',
            'email.required' => 'email is required',
            'password.required' => 'password is required',
            'phone.required' => 'enter phone ',

        ];
    }

    public function AddFriend(Request $req, $id)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("create",$emp_id,"AddFriend");


            $emp = new FriendModel();
            $emp->user_id = $id;
            $emp->friend_email = $req->email;
            $emp->friend_name = $req->name;
            if ($emp->save()) {
                // return response()->json("You added ".$req->email);
                return response()->json(['status' => 'success', 'message' => "You added " . $req->email]);
            } else
                return response()->json(['status' => 'error', 'message' => "friend does not added "]);
        }
    }

    public function AllFriend($id, Request $req)
    {
        //permission
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp = EmployeesModel::find($id);
            if ($emp != null) {
                $allFr = FriendModel::where('user_id', $id)->get();
                return response()->json(['status' => 'success', 'message' => $allFr]);
            } else
                return response()->json(['status' => 'error', 'message' => "user not found"]);
        }
    }

    public function SearchFriends($id, $name, Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $Fr = FriendModel::where('user_id', $id)->Where('friend_name', 'like', "%" . $name . "%")->OrWhere('friend_email', 'like', "%" . $name . "%")
                ->get();
            return response()->json(['status' => 'success', 'message' => $Fr]);
        }
    }

    public function GetNamesOfEmployees(Request $req)
    {
        //permission
        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض الموظفين')) {
        //     return response()->json(['status' => "error", 'message' => "عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            return response()->json(DB::table('employee')->select('f_name', 'l_name')->get());
        }
    }

    public function GetEmployees(Request $req)
    {
        //permission
        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض الموظفين')) {
        //     return response()->json(['status' => "error", 'message' => "عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            return response()->json(DB::table('employee')->select('f_name', 'l_name', 'id')->get());
        }
    }
    public function getempolyees(Request $req)
    {


        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);


            $emp_id = $emp1->id;
            //echo $emp1->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            //echo $warehouse_id->warehouse_id;


       $role_id=EmployeesModel::select('role_id','warehouse_id')->where('id',$emp_id)->first();
       $role_name=rolesModel::where('id',$role_id->role_id)->first();
    //    echo $role_name->name;
       if($role_name->name=='مدير النظام')
       {
        $emps=DB::table('warehouse')->join('employee','employee.warehouse_id','warehouse.id')->get();
        return response()->json($emps);
       }
       elseif($role_name->name=='مدير المستودعات')
       {
        // echo 'manager';
           $warehouse_id=$role_id->warehouse_id;
           $emps=EmployeesModel::where('warehouse_id',$warehouse_id)->where('id','!=',$warehouse_id)->get();
           return response()->json($emps);
              //1-بجيب مستودع ال هو مدير فيه
              //2- بجيب كل موظفين هاد المستودع
       }
       elseif($role_name->name=='accounting manager')
       {
        // echo 'accounting';
        // بجيب كل موظفيم المحاسبة
        $emps=EmployeesModel::where('role_id',4)->get();
        return response()->json($emps);
       }
    }
    }
    public function GivingRoleToEmployee(Request $request)
    {
        //permission
        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض الموظفين')) {
        //     return response()->json(['status' => "error", 'message' => "عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {

            $emp_id = $emp1->id;

            //log
            addLog("create",$emp_id,"GivingRoleToEmployee");


            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $user = new User();
            $user = User::select('id')->where('id', $request->input('user_id'))->first();
            $user->assignRole([$request->input('role_id')]);

            DB::table('employee')->where('id', $request->input('user_id'))->update(['role_id' => $request->input('role_id')]);
            return response()->json(['status' => "error", 'message' => "تم إضافة الدور للمستخدم بنجاح"]);
        }
    }

}
