<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use App\Models\EmployeesModel;
use App\Models\ItemModel;
use Illuminate\Http\Request;
use App\Models\User;

class CategoryController extends Controller
{
    public function GetCatogory(Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token','id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id=$emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }



            $Category = new CategoryModel();
            $Category = CategoryModel::all();
            return response()->json($Category);
        }
    }

    public function AddCategory(Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token')->where('token', $token)->first();
        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {

            $emp_id = $emp->id;

            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            //log
            // addLog("create",$emp_id,"AddCategory");

            $Category = new CategoryModel();
            $Category->name = $req->name;
            $Category->description = $req->description;
            if ($Category->save()) {
                return "category added";
            }
        }
    }

    public function EditCategory($id,Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token')->where('token', $token)->first();
        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;

            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            //log
         //   addLog("update",$emp_id,"EditCategory");

            $data = CategoryModel::find($id);
            return view('EditCategory', ['data' => $data]);
        }
    }

    public function UpdateCategory(Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token')->where('token', $token)->first();
        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {

            $emp_id = $emp->id;

            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            //log
         //   addLog("update",$emp_id,"UpdateCategory");

            $data = CategoryModel::find($req->id);
            $data->name = $req->name;
            $data->description = $req->description;
            if ($data->save()) {
                return "category Updated";
            }
        }
    }

    public function GetItemInCategory($id, Request $req)
    {
        //permission


        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token')->where('token', $token)->first();
        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $data = CategoryModel::find($id);
            if ($data != null) {
                $Item = new ItemModel();
                $Item = ItemModel::where('category_id', $id)->get();
                return response()->json($Item);
            } else {
                return "category not found";
            }
        }
    }

    public function SearchCategory($name, Request $req)
    {
        //permission


        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token')->where('token', $token)->first();
        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $category = CategoryModel::where('name', 'like', "%" . $name . "%")->get();
            return response()->json($category);
        }
    }

}
