<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\EmployeesModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AssetController extends Controller
{
    public function GetAllCategoriesOfAssetInWarehouse(Request $request, $warehouse_id)
    {

        //        $token = $request->bearerToken();
        //        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        //
        //        if ($emp == null) {
        //            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        //        }
        //        $id = $emp->id;


        //        //permission
        //
        //        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        //        if (!app('auth')->guard()->getUser()->can('عرض كل اللجان')){
        //            return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        //        }

        return response()->json(DB::table('asset_category')
            ->where('warehouse_id', $warehouse_id)->get());
    }

    public function GetAllAssets(Request $request)
    {
        $token = $request->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;



        //permission

        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        if (!app('auth')->guard()->getUser()->can('عرض كل اللجان')) {
            return response()->json(['status' => "error", 'message' => "عذراً ليس لديك صلاحية الوصول"]);
        }

        return response()->json(DB::table('asset')->get());
    }

    public function GetAllAssetsInCategory(Request $request, $category_id)
    {
        $token = $request->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;



        //permission

        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        if (!app('auth')->guard()->getUser()->can('عرض كل اللجان')) {
            return response()->json(['status' => "error", 'message' => "عذراً ليس لديك صلاحية الوصول"]);
        }

        return response()->json(DB::table('asset')
            ->where('cotegory_id', $category_id)
            ->get());
    }

    public function CreateAsset(Request $request)
    {
        $token = $request->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;


        //permission

        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        if (!app('auth')->guard()->getUser()->can('إنشاء لجنة')) {
            return response()->json(['status' => "error", 'message' => "عذراً ليس لديك صلاحية الوصول"]);
        }


        //log
        addLog("create", $id, "CreateAsset");



        // id	name	type	responsible_id	code	cotegory_id	location
        //warehouse_id	quantity	supplier_id	statu	abstract_value	depreciation_vale
        //service_period	methode_of_position	price	date	annihhiliting_value	annihhiliting_ratio

        //	id	type	date	emp_id	process
        //validation
        $validator = $this->CommitteeCreateValidator($request);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $committee_id = DB::table('committee')->insertGetId(
            ['name' => $request->input('name')]
        );

        return response()->json(['status' => "success", 'message' => "نمت إضافة اللجنة بنجاح"]);
    }

    public function CreatePartOfAsset(Request $request)
    {
        $token = $request->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;

        //permission

        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        if (!app('auth')->guard()->getUser()->can('إنشاء لجنة')) {
            return response()->json(['status' => "error", 'message' => "عذراً ليس لديك صلاحية الوصول"]);
        }


        //log
        addLog("create", $id, "CreatePartOfAsset");
    }

    public function EditAsset(Request $request, $id)
    {
        $token = $request->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;

        //permission

        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        if (!app('auth')->guard()->getUser()->can('إنشاء لجنة')) {
            return response()->json(['status' => "error", 'message' => "عذراً ليس لديك صلاحية الوصول"]);
        }


        //log
        addLog("update", $id, "EditAsset");

        if (DB::table('committee')->find($id)) {
            //validation
            $validator = $this->CommitteeEditValidator($request);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            DB::table('committee')
                ->where('id', $id)
                ->update(
                    ['name' => $request->input('name')]
                );

            return response()->json(['status' => "success", 'message' => "نم تعديل معلومات اللجنة بنجاح"]);
        }
        return response()->json(['status' => "error", 'message' => "هذه اللجنة غير موجودة"]);
    }

    public function DeleteAsset(Request $request, $id)
    {
        $token = $request->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;

        //permission

        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        if (!app('auth')->guard()->getUser()->can('إنشاء لجنة')) {
            return response()->json(['status' => "error", 'message' => "عذراً ليس لديك صلاحية الوصول"]);
        }


        //log
        addLog("delete", $id, "DeleteAsset");

        if (DB::table('committee')->find($id)) {
            DB::table('committee')->where('id', $id)->delete();

            return response()->json(['status' => "success", 'message' => "نم حذف اللجنة بنجاح"]);
        }

        return response()->json(['status' => "error", 'message' => "هذه اللجنة غير موجودة"]);
    }

    public function CommitteeCreateValidator(Request $request)
    {
        $this->roles = [
            'name' => 'required|unique:committee|max:100',
            'members' => 'required|max:10',
        ];

        $this->messages = [
            'name.required' => 'حقل الاسم مطلوب',
            'name.unique' => 'هذا الاسم موجود مسبقاً',
            'name.max' => 'حقل الاسم يجب ألا يتجاوز 100 كيلوبايت',
            'members.required' => 'يجب تحديد عضو واحد على الأقل',
            'members.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function CommitteeEditValidator(Request $request)
    {
        $this->roles = [
            'name' => 'required|max:100',
            'members' => 'required|max:10',
        ];

        $this->messages = [
            'name.required' => 'حقل الاسم مطلوب',
            'members.required' => 'يجب تحديد عضو واحد على الأقل',
            'members.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }
}
