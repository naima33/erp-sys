<?php

namespace App\Http\Controllers;

use App\Models\EmployeesModel;
use App\Models\ItemCardModel;
use App\Models\ItemModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use App\Models\Model_has_roles;
use App\Models\Receipt_noteModel;
use App\Models\rolesModel;
use Elibyy\TCPDF\Facades\TCPDF;
use PDF;
use Illuminate\Support\Facades\View;

class ReceiptNoteController extends Controller
{
    public function PrintReceiptNote($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = ItemCardModel
            ::join('receipt note','receipt note.id','item_card.rec_id')
            ->join('receipt as rec','receipt note.id','rec.recept_note_id')
            ->join('item','item.id','item_card.item_id')
            ->join('supplier as s','s.id','rec.Supplier_id')
            ->join('warehouse as w','w.id','item_card.warehouse_id')
            ->select(
              'item_card.date',
              'item_card.quantity',
              'w.id as ware_id',
              'w.name as warehouse_name',
               'receipt note.type',
               'receipt note.picture',
               'receipt note.order_id as order_id',
               'receipt note.id as note_id',
               's.name as Supp_name',
               'item.name as item_name'
                )
            ->where('receipt note.id',$id)->where('receipt note.type', 'receipt')
            ->Where('item_card.warehouse_id', $warehouse_id->warehouse_id)->get();
           // return response()->json(['status' => 'success', 'message' => $all]);

           $filename = 'receiptnote.pdf';
           $view = View::make('receiptNote', ['results' => $all]);
           $html = $view->render();

           $pdf = new TCPDF;

           $pdf::SetTitle('receiptNote');
           $pdf::SetFont('aealarabiya', '', 18);
           $pdf::setRTL(true);
           $pdf::AddPage();

           $pdf::writeHTML($html, true, false, true, false, '');

           $pdf::Output(public_path($filename), 'F');

           return response()->download(public_path($filename));
        }
    }

    public function getReceiptNote($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = Receipt_noteModel
            ::join('receipt as rec','receipt note.id','=','rec.recept_note_id')
            ->join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
            ->join('item','item.id','=','i.item_id')
            ->join('supplier as s','s.id','=','rec.Supplier_id')
            ->join('warehouse as w','w.id','=','i.warehouse_id')
            ->select(
              'i.date',
              'i.quantity',
              'w.id as ware_id',
              'w.name as warehouse_name',
               'receipt note.type',
               'receipt note.picture',
               'receipt note.order_id as order_id',
               'receipt note.id as note_id',
               's.name as Supp_name',
               'item.name as item_name'
                )
            ->where('receipt note.id',$id)->where('receipt note.type', 'receipt')
            ->Where('i.warehouse_id', $warehouse_id->warehouse_id)

            ->get();
            return response()->json(['status' => 'success', 'message' => $all]);



          // return response()->json($all);
        }
    }

    public function GetAllReceiptNotes(Request $req)
    {

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            //  $role= Model_has_roles
            //      ::join('employee','employee.id','model_has_roles.model_id')
            //      ->join('roles','roles.id','model_has_roles.role_id')
            //      ->where('model_id','=',$emp1->id)
            //      ->first();

            $role_id=$emp1->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

             if ($role_name->name =='مدير النظام'){
                 return response()->json(DB::table('item_card')
                     ->join('receipt note as rec_note','rec_note.id','item_card.rec_id')
                     ->join('receipt as rec','rec.recept_note_id','rec_note.id')
                     ->join('supplier as s','s.id','rec.Supplier_id')
                     ->join('warehouse as w','w.id','item_card.warehouse_id')
                     ->select(
                        'item_card.date',
                        'item_card.quantity',
                        'w.id as ware_id',
                        'w.name as warehouse_name',
                         'rec_note.type',
                         'rec_note.picture',
                         'rec_note.order_id as order_id',
                         'rec_note.id as note_id',
                         's.name as Supp_name',

                         )
                         ->where('rec_note.type','=','receipt')
                         ->groupBy('item_card.date','item_card.quantity','ware_id','warehouse_name','rec_note.type','rec_note.picture','order_id'
                         ,'note_id','Supp_name')
                     ->orderBy('rec_note.id')
                     ->get());
            }

             else{
                 $warehouse_id = DB::table('employee')->find($emp1->id);
                 return response()->json(DB::table('item_card')
                     ->join('receipt note as rec_note','rec_note.id','item_card.rec_id')
                     ->join('receipt as rec','rec.recept_note_id','rec_note.id')
                     ->join('supplier as s','s.id','rec.Supplier_id')
                     ->join('warehouse as w','w.id','item_card.warehouse_id')
                     ->select(
                        'item_card.date',
                        'item_card.quantity',
                        'w.id as ware_id',
                        'w.name as warehouse_name',
                         'rec_note.type',
                         'rec_note.picture',
                         'rec_note.order_id as order_id',
                         'rec_note.id as note_id',
                         's.name as Supp_name',

                         )
                         ->where('rec_note.type','=','receipt')
                         ->where('warehouse_id', '=', $warehouse_id->warehouse_id)
                         ->groupBy('item_card.date','item_card.quantity','ware_id','warehouse_name','rec_note.type','rec_note.picture','order_id'
                         ,'note_id','Supp_name')
                         ->orderBy('rec_note.id')
                         ->get()
                 );
                // $emp_id = $emp1->id;
                // $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                // $all = Receipt_noteModel
                //     ::join('receipt as r', 'receipt note.id', '=', 'r.recept_note_id')
                //     ->join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                //     ->join('item as it', 'i.item_id', '=', 'it.id')
                //     ->select(
                //         'i.date',
                //         'i.quantity',
                //         'i.warehouse_id',
                //         'receipt note.type',
                //         'receipt note.picture',
                //         'receipt note.order_id',
                //         'r.Supplier_id',
                //         'receipt note.id as receipt_note_id',
                //         'it.name'
                //     )
                //     ->where('receipt note.type', 'receipt')->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
                    return response()->json(['status' => 'success', 'message' => $all]);
             }

        }
    }

    public function GetAllBills(Request $req)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض كل الفواتير')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $role= Model_has_roles
                ::join('employee','employee.id','model_has_roles.model_id')
                ->join('roles','roles.id','model_has_roles.role_id')
                ->where('model_id','=',$emp1->id)
                ->first();

            if ($role->name =='مدير النظام') {
                return response()->json(
                    DB::table('item_card')
                        ->where('receipt note.type', '=', 'bill')
                        ->join('receipt note', 'receipt note.id', 'item_card.rec_id')
                        ->join('bill', 'bill.receipt_note_id', 'receipt note.id')
                        ->join('item', 'item.id', 'item_card.item_id')
                        ->join('warehouse', 'warehouse.id', 'item_card.warehouse_id')
                        ->select('bill.id as bill_id', 'receipt note.date', 'receipt note.picture', 'receipt note.order_id'
                            , 'item_card.code', 'quantity', 'warehouse.name as warehouse_name', 'current_quantity', 'item.name as item_name'
                            , 'total_price')
                        ->orderBy('bill.id')
                        ->get());
            }
            else{
                //return $warehouse_id
                $warehouse_id = DB::table('employee')->find($emp1->id);

                return response()->json(
                    DB::table('item_card')
                        ->where('receipt note.type','=','bill')
                        ->where('warehouse_id','=',$warehouse_id->warehouse_id)
                        ->join('receipt note','receipt note.id','item_card.rec_id')
                        ->join('bill', 'bill.receipt_note_id', 'receipt note.id')
                        ->join('item','item.id','item_card.item_id')
                        ->join('warehouse','warehouse.id','item_card.warehouse_id')
                        ->select('bill.id as bill_id','receipt note.date','receipt note.picture','receipt note.order_id'
                            ,'item_card.code','quantity','warehouse.name as warehouse_name','current_quantity','item.name as item_name'
                            ,'total_price')
                        ->orderBy('bill.id')
                        ->get());

            }
        }
    }


    public function GetAllInstallationRecords(Request $req)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض كل محاضر التركيب')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $role= Model_has_roles
                ::join('employee','employee.id','model_has_roles.model_id')
                ->join('roles','roles.id','model_has_roles.role_id')
                ->where('model_id','=',$emp1->id)
                ->first();

            if ($role->name == 'مدير النظام') {
                return response()->json(
                    DB::table('item_card')
                        ->where('receipt note.type', '=', 'record')
                        ->join('receipt note', 'receipt note.id', 'item_card.rec_id')
                        ->join('installation record', 'installation record.receipt_note_id', 'receipt note.id')
                        ->join('item', 'item.id', 'item_card.item_id')
                        ->join('warehouse', 'warehouse.id', 'item_card.warehouse_id')
                        ->join('employee', 'employee.id', 'installation record.user_id')
                        ->select('installation record.id as installation_record_id', 'receipt note.date', 'receipt note.picture', 'receipt note.order_id'
                            , 'item_card.code', 'quantity', 'warehouse.name as warehouse_name', 'current_quantity', 'item.name as item_name'
                            , 'employee.f_name as employee_first_name', 'employee.l_name as employee_last_name'
                        )
                        ->orderBy('installation record.id')
                        ->get());
            }
            else{
                //return $warehouse_id
                $warehouse_id = DB::table('employee')->find($emp1->id);

                return response()->json(
                    DB::table('item_card')
                        ->where('receipt note.type','=','record')
                        ->where('item_card.warehouse_id','=',$warehouse_id->warehouse_id)
                        ->join('receipt note','receipt note.id','item_card.rec_id')
                        ->join('installation record', 'installation record.receipt_note_id', 'receipt note.id')
                        ->join('item','item.id','item_card.item_id')
                        ->join('warehouse','warehouse.id','item_card.warehouse_id')
                        ->join('employee','employee.id','installation record.user_id')
                        ->select('installation record.id as installation_record_id','receipt note.date','receipt note.picture','receipt note.order_id'
                            ,'item_card.code','quantity','warehouse.name as warehouse_name','current_quantity','item.name as item_name'
                            ,'employee.f_name as employee_first_name','employee.l_name as employee_last_name'
                        )
                        ->orderBy('installation record.id')
                        ->get());

            }
        }
    }


    public function GetAllContracts(Request $req)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض كل العقود')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $role= Model_has_roles
                ::join('employee','employee.id','model_has_roles.model_id')
                ->join('roles','roles.id','model_has_roles.role_id')
                ->where('model_id','=',$emp1->id)
                ->first();

            if ($role->name == 'مدير النظام') {
                return response()->json(
                    DB::table('item_card')
                        ->where('receipt note.type', '=', 'contract')
                        ->join('receipt note', 'receipt note.id', 'item_card.rec_id')
                        ->join('contract', 'contract.receipt_note_id', 'receipt note.id')
                        ->join('item', 'item.id', 'item_card.item_id')
                        ->join('warehouse', 'warehouse.id', 'item_card.warehouse_id')
                        ->select('contract.id as contract_id', 'receipt note.date', 'receipt note.picture', 'receipt note.order_id'
                            , 'item_card.code', 'quantity', 'warehouse.name as warehouse_name', 'current_quantity', 'item.name as item_name'
                            , 'end_date', 'price1')
                        ->orderBy('contract.id')
                        ->get());
            }
            else{
                //return $warehouse_id
                $warehouse_id = DB::table('employee')->find($emp1->id);

                return response()->json(
                    DB::table('item_card')
                        ->where('receipt note.type','=','contract')
                        ->where('warehouse_id','=',$warehouse_id->warehouse_id)
                        ->join('receipt note','receipt note.id','item_card.rec_id')
                        ->join('contract', 'contract.receipt_note_id', 'receipt note.id')
                        ->join('item','item.id','item_card.item_id')
                        ->join('warehouse','warehouse.id','item_card.warehouse_id')
                        ->select('contract.id as contract_id','receipt note.date','receipt note.picture','receipt note.order_id'
                            ,'item_card.code','quantity','warehouse.name as warehouse_name','current_quantity','item.name as item_name'
                            ,'end_date','price1')
                        ->orderBy('contract.id')
                        ->get());

            }
        }
    }


    public function GetAllPresents(Request $req)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض كل المنح')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $role= Model_has_roles
                ::join('employee','employee.id','model_has_roles.model_id')
                ->join('roles','roles.id','model_has_roles.role_id')
                ->where('model_id','=',$emp1->id)
                ->first();

            if ($role->name == 'مدير النظام') {
                return response()->json(
                    DB::table('item_card')
                        ->where('receipt note.type', '=', 'present')
                        ->join('receipt note', 'receipt note.id', 'item_card.rec_id')
                        ->join('present', 'present.receipt_note_id', 'receipt note.id')
                        ->join('item', 'item.id', 'item_card.item_id')
                        ->join('warehouse', 'warehouse.id', 'item_card.warehouse_id')
                        ->select('present.id as present_id', 'receipt note.date', 'receipt note.picture', 'receipt note.order_id'
                            , 'item_card.code', 'quantity', 'warehouse.name as warehouse_name', 'current_quantity', 'item.name as item_name'
                            , 'presenter')
                        ->orderBy('present.id')
                        ->get());
            }
            else{
                //return $warehouse_id
                $warehouse_id = DB::table('employee')->find($emp1->id);
                //echo $warehouse_id->warehouse_id;
                return response()->json(
                    DB::table('item_card')
                        ->where('receipt note.type','=','present')
                        ->where('warehouse_id','=',$warehouse_id->warehouse_id)
                        ->join('receipt note','receipt note.id','item_card.rec_id')
                        ->join('present', 'present.receipt_note_id', 'receipt note.id')
                        ->join('item','item.id','item_card.item_id')
                        ->join('warehouse','warehouse.id','item_card.warehouse_id')
                        ->select('present.id as present_id','receipt note.date','receipt note.picture','receipt note.order_id'
                            ,'item_card.code','quantity','warehouse.name as warehouse_name','current_quantity','item.name as item_name'
                            ,'presenter')
                        ->orderBy('present.id')
                        ->get());

            }
        }
    }

    public function CreateReceiptNote(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("create",$id,"CreateReceiptNote");


            //validation
            $validator = $this->ReceiptNoteValidator($request);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $file_name = null;
            if ($request->picture != null) {
                $file_extension = $request->picture->getClientOriginalExtension();
                $path = 'images/receipt_note';
                $file_name =time() . '.' . $file_extension;
                $request->picture->move($path, $file_name);
            }

            $receipt_note = DB::table('receipt note')->insertGetId(
                ['date' => Carbon::now(), 'picture' => $file_name, 'type' => 'receipt', 'order_id' => 46]
            );

            //return $warehouse_id
            $warehouse_id = DB::table('employee')->find($id);

            for ($i = 0; $i < count($request->item_name); $i++) {
                $item_name = $request->item_name[$i];
                $item_id = ItemModel::select('id', 'code')->where('name', $item_name)->first();
                $item_card = new ItemCardModel();
                $item_card->item_id = $item_id->id;
                $item_card->type = "input";
                $item_card->date = Carbon::now();
                $item_card->code = $item_id->code;
                $item_card->quantity = $request->quantity[$i];
                $item_card->current_quantity = $request->quantity[$i];
                $item_card->rec_id = $receipt_note;
                $item_card->warehouse_id = $warehouse_id->warehouse_id;
                $item_card->save();
            }

            DB::table('receipt')->insertGetId(
                ['Supplier_id' => $request->Supplier_id, 'recept_note_id' => $receipt_note]
            );

            return response()->json(['status' => "success", 'message' => "نمت إضافة مذكرة الإدخال بنجاح"]);
        }
    }

    public function CreateSupplier(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("create",$id,"CreateSupplier");


            //validation
            $validator = $this->CreateSupplierValidator($request);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            DB::table('supplier')->insertGetId(
                ['name' => $request->name, 'email' => $request->email, 'phone' => $request->phone, 'address' => $request->address]
            );

            return response()->json(['status' => "success", 'message' => "نم الإدخال بنجاح"]);
        }
    }

    public function CreateBill(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("create",$id,"CreateBill");


            //validation
            $validator = $this->BillValidator($request);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }
            $file_name = null;
            if ($request->picture != null) {
                $file_extension = $request->picture->getClientOriginalExtension();
                $path = 'images\receipt_note\\';
                $file_name = base_path() . '\public\\' . $path . time() . '.' . $file_extension;
                $request->picture->move($path, $file_name);
            }

            $receipt_note = DB::table('receipt note')->insertGetId(
                ['date' => Carbon::now(), 'picture' => $file_name, 'type' => 'bill', 'order_id' =>46]
            );

            //return $warehouse_id
            $warehouse_id = DB::table('employee')->find($id);

            $total = 0;
            for ($i = 0; $i < count($request->item_name); $i++) {
                $item_name = $request->item_name[$i];
                $item_id = ItemModel::select('id', 'code', 'price')->where('name', $item_name)->first();
                $item_card = new ItemCardModel();
                $item_card->item_id = $item_id->id;
                $item_card->type = "input";
                $item_card->date = Carbon::now();
                $item_card->code = $item_id->code;
                $item_card->quantity = $request->quantity[$i];
                $item_card->current_quantity = $request->quantity[$i];
                $item_card->rec_id = $receipt_note;
                $item_card->warehouse_id = $warehouse_id->warehouse_id;
                $item_card->save();

                $total += $request->quantity[$i] * $item_id->price;
            }

            DB::table('bill')->insertGetId(
                ['total_price' => $total, 'receipt_note_id' => $receipt_note]
            );

            return response()->json(['status' => "success", 'message' => "نمت إضافة الفاتورة بنجاح"]);
        }
    }

    public function CreateContract(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("create",$id,"CreateContract");


            //validation
            $validator = $this->ContractValidator($request);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $file_name = null;
            if ($request->picture != null) {
                $file_extension = $request->picture->getClientOriginalExtension();
                $path = 'images\receipt_note\\';
                $file_name = base_path() . '\public\\' . $path . time() . '.' . $file_extension;
                $request->picture->move($path, $file_name);
            }

            $receipt_note = DB::table('receipt note')->insertGetId(
                ['date' => Carbon::now(), 'picture' => $file_name, 'type' => 'contract', 'order_id' => 46]
            );

            //return $warehouse_id
            $warehouse_id = DB::table('employee')->find($id);

            for ($i = 0; $i < count($request->item_name); $i++) {
                $item_name = $request->item_name[$i];
                $item_id = ItemModel::select('id', 'code', 'price')->where('name', $item_name)->first();
                $item_card = new ItemCardModel();
                $item_card->item_id = $item_id->id;
                $item_card->type = "input";
                $item_card->date = Carbon::now();
                $item_card->code = $item_id->code;
                $item_card->quantity = $request->quantity[$i];
                $item_card->current_quantity = $request->quantity[$i];
                $item_card->rec_id = $receipt_note;
                $item_card->warehouse_id = $warehouse_id->warehouse_id;
                $item_card->save();

                DB::table('contract')->insertGetId(
                    ['end_date' => $request->end_date, 'price1' => $item_id->price, 'receipt_note_id' => $receipt_note, 'item_id1' => $item_id->id]
                );
            }

            return response()->json(['status' => "success", 'message' => "نمت إضافة العقد بنجاح"]);
        }
    }

    public function CreateInstallationRecord(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("create",$id,"CreateInstallationRecord");


            //validation
            $validator = $this->InstallationRecordValidator($request);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $file_name = null;
            if ($request->picture != null) {
                $file_extension = $request->picture->getClientOriginalExtension();
                $path = 'images\receipt_note\\';
                $file_name = base_path() . '\public\\' . $path . time() . '.' . $file_extension;
                $request->picture->move($path, $file_name);
            }

            $receipt_note = DB::table('receipt note')->insertGetId(
                ['date' => Carbon::now(), 'picture' => $file_name, 'type' => 'install', 'order_id' => 46]
            );

            //return $warehouse_id
            $warehouse_id = DB::table('employee')->find($id);

            for ($i = 0; $i < count($request->item_name); $i++) {
                $item_name = $request->item_name[$i];
                $item_id = ItemModel::select('id', 'code')->where('name', $item_name)->first();
                $item_card = new ItemCardModel();
                $item_card->item_id = $item_id->id;
                $item_card->type = "input";
                $item_card->date = Carbon::now();
                $item_card->code = $item_id->code;
                $item_card->quantity = $request->quantity[$i];
                $item_card->current_quantity = $request->quantity[$i];
                $item_card->rec_id = $receipt_note;
                $item_card->warehouse_id = $warehouse_id->warehouse_id;
                $item_card->save();
            }

            DB::table('installation record')->insertGetId(
                ['user_id' => $request->user_id, 'receipt_note_id' => $receipt_note]
            );

            return response()->json(['status' => "success", 'message' => "نمت إضافة محضر التركيب بنجاح"]);
        }
    }

    public function CreatePresent(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("create",$id,"CreatePresent");


            //validation
            $validator = $this->PresentValidator($request);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $file_name = null;
            if ($request->picture != null) {
                $file_extension = $request->picture->getClientOriginalExtension();
                $path = 'images\receipt_note\\';
                $file_name = base_path() . '\public\\' . $path . time() . '.' . $file_extension;
                $request->picture->move($path, $file_name);
            }

            $receipt_note = DB::table('receipt note')->insertGetId(
                ['date' => Carbon::now(), 'picture' => $file_name, 'type' => 'present', 'order_id' =>46]
            );

            //return $warehouse_id
            $warehouse_id = DB::table('employee')->find($id);

            for ($i = 0; $i < count($request->item_name); $i++) {
                $item_name = $request->item_name[$i];
                $item_id = ItemModel::select('id', 'code')->where('name', $item_name)->first();
                $item_card = new ItemCardModel();
                $item_card->item_id = $item_id->id;
                $item_card->type = "input";
                $item_card->date = Carbon::now();
                $item_card->code = $item_id->code;
                $item_card->quantity = $request->quantity[$i];
                $item_card->current_quantity = $request->quantity[$i];
                $item_card->rec_id = $receipt_note;
                $item_card->warehouse_id = $warehouse_id->warehouse_id;
                $item_card->save();
            }

            DB::table('present')->insertGetId(
                ['presenter' => $request->presenter, 'receipt_note_id' => $receipt_note]
            );

            return response()->json(['status' => "success", 'message' => "نمت الإضافة بنجاح"]);
        }
    }

    public function EditReceiptNote(Request $request, $id)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id1 = $emp1->id;

            //log
            addLog("update",$id1,"EditReceiptNote");


            if (DB::table('receipt note')->find($id)) {
                //validation
                $validator = $this->ReceiptNoteValidator($request);
                if ($validator->fails()) {
                    return response()->json($validator->errors());
                }

                $file_name = null;
                if ($request->picture != null) {
                    $file_extension = $request->picture->getClientOriginalExtension();
                    $path = 'images\receipt_note\\';
                    $file_name = base_path() . '\public\\' . $path . time() . '.' . $file_extension;
                    $request->picture->move($path, $file_name);
                }

                DB::table('receipt note')
                    ->where('id', $id)
                    ->update(['date' => Carbon::now(), 'picture' => $file_name, 'type' => 'receipt', 'order_id' => 46]);

                DB::table('item_card')->where('rec_id', '=', $id)->delete();

                //return $warehouse_id
                $warehouse_id = DB::table('employee')->find($id1);

                for ($i = 0; $i < count($request->item_name); $i++) {
                    $item_name = $request->item_name[$i];
                    $item_id = ItemModel::select('id', 'code')->where('name', $item_name)->first();
                    $item_card = new ItemCardModel();
                    $item_card->item_id = $item_id->id;
                    $item_card->type = "input";
                    $item_card->date = Carbon::now();
                    $item_card->code = $item_id->code;
                    $item_card->quantity = $request->quantity[$i];
                    $item_card->current_quantity = $request->quantity[$i];
                    $item_card->rec_id = $id;
                    $item_card->warehouse_id = $warehouse_id->warehouse_id;
                    $item_card->save();
                }
                return response()->json(['status' => "success", 'message' => "نم تعديل معلومات مذكرة الإدخال بنجاح"]);
            }
            return response()->json(['status' => "error", 'message' => "هذه المذكرة غير موجودة"]);
        }
    }

    public function ReceiptNoteValidator(Request $request)
    {
        $this->roles = [
            'item_name' => 'required|max:100',
            'quantity' => 'required|max:10',
            'Supplier_id' => 'required|max:10',
        ];

        $this->messages = [
            'required' => 'هذا الحقل مطلوب',
            'item_name.max' => 'هذا الحقل يجب ألا يتجاوز 100 كيلوبايت',
            'quantity.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
            'Supplier_id.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function CreateSupplierValidator(Request $request)
    {
        $this->roles = [
            'name' => 'required|max:100',
            'email' => 'required|max:100',
            'phone' => 'required|max:10',
            'address' => 'required|max:100',
        ];

        $this->messages = [
            'required' => 'هذا الحقل مطلوب',
            'max' => 'هذا الحقل يجب ألا يتجاوز 100 كيلوبايت',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function BillValidator(Request $request)
    {
        $this->roles = [
            'item_name' => 'required|max:10',
            'quantity' => 'required|max:10',
        ];

        $this->messages = [
            'required' => 'هذا الحقل مطلوب',
            'item_name.max' => 'هذا الحقل يجب ألا يتجاوز 100 كيلوبايت',
            'quantity.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function ContractValidator(Request $request)
    {
        $this->roles = [
            'item_name' => 'required|max:100',
            'quantity' => 'required|max:10',
            'end_date' => 'required|date',
        ];

        $this->messages = [
            'required' => 'هذا الحقل مطلوب',
            'item_name.max' => 'هذا الحقل يجب ألا يتجاوز 100 كيلوبايت',
            'quantity.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
            'date' => 'يرجى إدخال تاريخ صالح',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function InstallationRecordValidator(Request $request)
    {
        $this->roles = [
            'item_name' => 'required|max:100',
            'quantity' => 'required|max:10',
            'user_id' => 'required|max:10',
        ];

        $this->messages = [
            'required' => 'هذا الحقل مطلوب',
            'item_name.max' => 'هذا الحقل يجب ألا يتجاوز 100 كيلوبايت',
            'quantity.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
            'user_id.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function PresentValidator(Request $request)
    {
        $this->roles = [
            'presenter' => 'required|max:10|string',
        ];

        $this->messages = [
            'required' => 'هذا الحقل مطلوب',
            'max' => 'هذا الحقل يجب ألا يتجاوز 100 كيلوبايت',
            'string' => 'هذا الحقل يجب أن يكون أحرف',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function GetALlSupplier(Request $request)
    {
        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;
            $all= DB::table('supplier')->get();
            return response()->json(['status' => "success", 'message' => $all]);
           }
    }
    public function PrintBill($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = ItemCardModel
            ::join('receipt note','receipt note.id','item_card.rec_id')
            ->join('bill','receipt note.id','bill.receipt_note_id')
            ->join('item','item.id','item_card.item_id')
            ->join('warehouse as w','w.id','item_card.warehouse_id')
            ->select(
              'item_card.date',
              'item_card.quantity',
              'w.id as ware_id',
              'w.name as warehouse_name',
               'receipt note.type',
               'receipt note.picture',
               'receipt note.order_id as order_id',
               'receipt note.id as note_id',
               'item.name as item_name',
                'bill.total_price',
                'item.price',
                'bill.id as bill_id'
                )
            ->where('bill.id',$id)->where('receipt note.type', 'bill')
            ->Where('item_card.warehouse_id', $warehouse_id->warehouse_id)->get();
           // return response()->json(['status' => 'success', 'message' => $all]);

            //return $all;

           $filename = 'bill.pdf';
           $view = View::make('bill', ['results' => $all]);
           $html = $view->render();

           $pdf = new TCPDF;

           $pdf::SetTitle('bill');
           $pdf::SetFont('aealarabiya', '', 18);
           $pdf::setRTL(true);
           $pdf::AddPage();

           $pdf::writeHTML($html, true, false, true, false, '');

           $pdf::Output(public_path($filename), 'F');

           return response()->download(public_path($filename));
        }
    }

    public function PrintContract($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = ItemCardModel
            ::join('receipt note','receipt note.id','item_card.rec_id')
            ->join('contract','receipt note.id','contract.receipt_note_id')
            ->join('item','item.id','item_card.item_id')
            ->join('warehouse as w','w.id','item_card.warehouse_id')
            ->select(
              'item_card.date',
              'item_card.quantity',
              'w.id as ware_id',
              'w.name as warehouse_name',
               'receipt note.type',
               'receipt note.picture',
               'receipt note.order_id as order_id',
               'receipt note.id as note_id',
               'item.name as item_name',
                'contract.end_date',
                'contract.price1',
                'contract.id as contract_id'
                )
            ->where('contract.id',$id)->where('receipt note.type', 'contract')
            ->Where('item_card.warehouse_id', $warehouse_id->warehouse_id)->get();
           // return response()->json(['status' => 'success', 'message' => $all]);

           $filename = 'contract.pdf';
           $view = View::make('contract', ['results' => $all]);
           $html = $view->render();

           $pdf = new TCPDF;

           $pdf::SetTitle('contract');
           $pdf::SetFont('aealarabiya', '', 18);
           $pdf::setRTL(true);
           $pdf::AddPage();

           $pdf::writeHTML($html, true, false, true, false, '');

           $pdf::Output(public_path($filename), 'F');

           return response()->download(public_path($filename));
        }
    }

    public function PrintInstallationRecord($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = ItemCardModel
            ::join('receipt note','receipt note.id','item_card.rec_id')
            ->join('installation record','receipt note.id','installation record.receipt_note_id')
            ->join('item','item.id','item_card.item_id')
            ->join('warehouse as w','w.id','item_card.warehouse_id')
            ->join('employee','employee.id','installation record.user_id')
            ->select(
              'item_card.date',
              'item_card.quantity',
              'w.id as ware_id',
              'w.name as warehouse_name',
               'receipt note.type',
               'receipt note.picture',
               'receipt note.order_id as order_id',
               'receipt note.id as note_id',
               'item.name as item_name',
               'installation record.id as installation_id',
                'employee.f_name',
                'employee.l_name'
                )
            ->where('installation record.id',$id)->where('receipt note.type', 'install')
            ->Where('item_card.warehouse_id', $warehouse_id->warehouse_id)->get();
           // return response()->json(['status' => 'success', 'message' => $all]);

            //return $all;
           $filename = 'installation record.pdf';
           $view = View::make('record', ['results' => $all]);
           $html = $view->render();

           $pdf = new TCPDF;

           $pdf::SetTitle('installation record');
           $pdf::SetFont('aealarabiya', '', 18);
           $pdf::setRTL(true);
           $pdf::AddPage();

           $pdf::writeHTML($html, true, false, true, false, '');

           $pdf::Output(public_path($filename), 'F');

           return response()->download(public_path($filename));
        }
    }

    public function PrintPresent($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = ItemCardModel
            ::join('receipt note','receipt note.id','item_card.rec_id')
            ->join('present','receipt note.id','present.receipt_note_id')
            ->join('item','item.id','item_card.item_id')
            ->join('warehouse as w','w.id','item_card.warehouse_id')
            ->select(
              'item_card.date',
              'item_card.quantity',
              'w.id as ware_id',
              'w.name as warehouse_name',
               'receipt note.type',
               'receipt note.picture',
               'receipt note.order_id as order_id',
               'receipt note.id as note_id',
               'present.presenter',
               'present.id as present_id',
               'item.name as item_name'
                )
            ->where('present.id',$id)->where('receipt note.type', 'present')
            ->Where('item_card.warehouse_id', $warehouse_id->warehouse_id)->get();
           // return response()->json(['status' => 'success', 'message' => $all]);

           $filename = 'present.pdf';
           $view = View::make('present', ['results' => $all]);
           $html = $view->render();

           $pdf = new TCPDF;

           $pdf::SetTitle('present');
           $pdf::SetFont('aealarabiya', '', 18);
           $pdf::setRTL(true);
           $pdf::AddPage();

           $pdf::writeHTML($html, true, false, true, false, '');

           $pdf::Output(public_path($filename), 'F');

           return response()->download(public_path($filename));
        }
    }

    public function GetBillById($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = ItemCardModel
                ::join('receipt note','receipt note.id','item_card.rec_id')
                ->join('bill','receipt note.id','bill.receipt_note_id')
                ->join('item','item.id','item_card.item_id')
                ->join('warehouse as w','w.id','item_card.warehouse_id')
                ->select(
                    'item_card.date',
                    'item_card.quantity',
                    'w.id as ware_id',
                    'w.name as warehouse_name',
                    'receipt note.type',
                    'receipt note.picture',
                    'receipt note.order_id as order_id',
                    'bill.id as bill_id',
                    'bill.total_price',
                    'item.name as item_name'
                )
                ->where('bill.id',$id)->where('receipt note.type', 'bill')
                ->Where('item_card.warehouse_id', $warehouse_id->warehouse_id)
            ->get();

            //echo $warehouse_id->warehouse_id;
            return response()->json(['status' => 'success', 'message' => $all]);
        }
    }


    public function GetContractById($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = ItemCardModel
                ::join('receipt note','receipt note.id','item_card.rec_id')
                ->join('contract','receipt note.id','contract.receipt_note_id')
                ->join('item','item.id','item_card.item_id')
                ->join('warehouse as w','w.id','item_card.warehouse_id')
                ->select(
                    'item_card.date',
                    'item_card.quantity',
                    'w.id as ware_id',
                    'w.name as warehouse_name',
                    'receipt note.type',
                    'receipt note.picture',
                    'receipt note.order_id as order_id',
                    'contract.id as contract_id',
                    'contract.end_date',
                    'contract.price1',
                    'item.name as item_name'
                )
                ->where('contract.id',$id)->where('receipt note.type', 'contract')
                ->Where('item_card.warehouse_id', $warehouse_id->warehouse_id)
            ->get();

            //echo $warehouse_id->warehouse_id;
            return response()->json(['status' => 'success', 'message' => $all]);
        }
    }


    public function GetInstallationById($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = ItemCardModel
                ::join('receipt note','receipt note.id','item_card.rec_id')
                ->join('installation record','receipt note.id','installation record.receipt_note_id')
                ->join('item','item.id','item_card.item_id')
                ->join('warehouse as w','w.id','item_card.warehouse_id')
                ->select(
                    'item_card.date',
                    'item_card.quantity',
                    'w.id as ware_id',
                    'w.name as warehouse_name',
                    'receipt note.type',
                    'receipt note.picture',
                    'receipt note.order_id as order_id',
                    'installation record.id as installation_id',
                    'installation record.user_id',
                    'item.name as item_name'
                )
                ->where('installation record.id',$id)->where('receipt note.type', 'install')
                ->Where('item_card.warehouse_id', $warehouse_id->warehouse_id)
            ->get();

           // echo $warehouse_id->warehouse_id;
            return response()->json(['status' => 'success', 'message' => $all]);
        }
    }


    public function GetPresentById($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = ItemCardModel
                ::join('receipt note','receipt note.id','item_card.rec_id')
                ->join('present','receipt note.id','present.receipt_note_id')
                ->join('item','item.id','item_card.item_id')
                ->join('warehouse as w','w.id','item_card.warehouse_id')
                ->select(
                    'item_card.date',
                    'item_card.quantity',
                    'w.id as ware_id',
                    'w.name as warehouse_name',
                    'receipt note.type',
                    'receipt note.picture',
                    'receipt note.order_id as order_id',
                    'present.id as present_id',
                    'present.presenter',
                    'item.name as item_name'
                )
                ->where('present.id',$id)->where('receipt note.type', 'present')
                ->Where('item_card.warehouse_id', $warehouse_id->warehouse_id)
            ->get();

            //echo $warehouse_id->warehouse_id;
            return response()->json(['status' => 'success', 'message' => $all]);
        }
    }


    public function getBills(Request $req)
    {


        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            //  $role= Model_has_roles
            //      ::join('employee','employee.id','model_has_roles.model_id')
            //      ->join('roles','roles.id','model_has_roles.role_id')
            //      ->where('model_id','=',$emp1->id)
            //      ->first();

            $role_id=$emp1->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            if ($role_name->name == 'مدير النظام'){

                $warehouse_id = DB::table('employee')->find($emp1->id);
                return response()->json(DB::table('item_card')
                    ->join('receipt note as rec_note','rec_note.id','item_card.rec_id')
                    ->join('bill','bill.receipt_note_id','rec_note.id')
                    ->join('warehouse as w','w.id','item_card.warehouse_id')
                    ->select(
                        'item_card.date',
                        'item_card.quantity',
                        'w.id as ware_id',
                        'w.name as warehouse_name',
                        'rec_note.type',
                        'rec_note.picture',
                        'rec_note.order_id as order_id',
                        'rec_note.id as note_id',
                        'bill.id as bill_id',
                        'bill.total_price'

                    )
                    ->where('rec_note.type','=','bill')
                    ->groupBy('item_card.date','item_card.quantity','ware_id','warehouse_name','rec_note.type','rec_note.picture','order_id'
                        ,'note_id','bill.total_price','bill_id')
                    ->orderBy('rec_note.id')
                    ->get()
                );

            }

            else{

                $warehouse_id = DB::table('employee')->find($emp1->id);
                return response()->json(DB::table('item_card')
                    ->join('receipt note as rec_note','rec_note.id','item_card.rec_id')
                    ->join('bill','bill.receipt_note_id','rec_note.id')
                    ->join('warehouse as w','w.id','item_card.warehouse_id')
                    ->select(
                        'item_card.date',
                        'item_card.quantity',
                        'w.id as ware_id',
                        'w.name as warehouse_name',
                        'rec_note.type',
                        'rec_note.picture',
                        'rec_note.order_id as order_id',
                        'rec_note.id as note_id',
                        'bill.id as bill_id',
                        'bill.total_price'

                    )
                    ->where('rec_note.type','=','bill')
                    ->where('warehouse_id', '=', $warehouse_id->warehouse_id)
                    ->groupBy('item_card.date','item_card.quantity','ware_id','warehouse_name','rec_note.type','rec_note.picture','order_id'
                        ,'note_id','bill.total_price','bill_id')
                    ->orderBy('rec_note.id')
                    ->get()
                );


                // $emp_id = $emp1->id;
                // $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                // $all = Receipt_noteModel
                //     ::join('receipt as r', 'receipt note.id', '=', 'r.recept_note_id')
                //     ->join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                //     ->join('item as it', 'i.item_id', '=', 'it.id')
                //     ->select(
                //         'i.date',
                //         'i.quantity',
                //         'i.warehouse_id',
                //         'receipt note.type',
                //         'receipt note.picture',
                //         'receipt note.order_id',
                //         'r.Supplier_id',
                //         'receipt note.id as receipt_note_id',
                //         'it.name'
                //     )
                //     ->where('receipt note.type', 'receipt')->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
                return response()->json(['status' => 'success', 'message' => $all]);
            }

        }

    }


    public function getPresents(Request $req)
    {
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            //  $role= Model_has_roles
            //      ::join('employee','employee.id','model_has_roles.model_id')
            //      ->join('roles','roles.id','model_has_roles.role_id')
            //      ->where('model_id','=',$emp1->id)
            //      ->first();

            $role_id=$emp1->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            if ($role_name->name == 'مدير النظام'){
                $warehouse_id = DB::table('employee')->find($emp1->id);
                return response()->json(DB::table('item_card')
                    ->join('receipt note as rec_note','rec_note.id','item_card.rec_id')
                    ->join('present','present.receipt_note_id','rec_note.id')
                    ->join('warehouse as w','w.id','item_card.warehouse_id')
                    ->select(
                        'item_card.date',
                        'item_card.quantity',
                        'w.id as ware_id',
                        'w.name as warehouse_name',
                        'rec_note.type',
                        'rec_note.picture',
                        'rec_note.order_id as order_id',
                        'rec_note.id as note_id',
                        'present.id as present_id',
                        'present.presenter',

                    )
                    ->where('rec_note.type','=','present')
                    ->groupBy('item_card.date','item_card.quantity','ware_id','warehouse_name','rec_note.type','rec_note.picture','order_id'
                        ,'note_id','present_id',
                        'present.presenter')
                    ->orderBy('rec_note.id')
                    ->get()
                );
            }

            else{
                $warehouse_id = DB::table('employee')->find($emp1->id);
                return response()->json(DB::table('item_card')
                    ->join('receipt note as rec_note','rec_note.id','item_card.rec_id')
                    ->join('present','present.receipt_note_id','rec_note.id')
                    ->join('warehouse as w','w.id','item_card.warehouse_id')
                    ->select(
                        'item_card.date',
                        'item_card.quantity',
                        'w.id as ware_id',
                        'w.name as warehouse_name',
                        'rec_note.type',
                        'rec_note.picture',
                        'rec_note.order_id as order_id',
                        'rec_note.id as note_id',
                        'present.id as present_id',
                        'present.presenter',

                    )
                    ->where('rec_note.type','=','present')
                    ->where('warehouse_id', '=', $warehouse_id->warehouse_id)
                    ->groupBy('item_card.date','item_card.quantity','ware_id','warehouse_name','rec_note.type','rec_note.picture','order_id'
                        ,'note_id','present_id',
                        'present.presenter')
                    ->orderBy('rec_note.id')
                    ->get()
                );
                // $emp_id = $emp1->id;
                // $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                // $all = Receipt_noteModel
                //     ::join('receipt as r', 'receipt note.id', '=', 'r.recept_note_id')
                //     ->join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                //     ->join('item as it', 'i.item_id', '=', 'it.id')
                //     ->select(
                //         'i.date',
                //         'i.quantity',
                //         'i.warehouse_id',
                //         'receipt note.type',
                //         'receipt note.picture',
                //         'receipt note.order_id',
                //         'r.Supplier_id',
                //         'receipt note.id as receipt_note_id',
                //         'it.name'
                //     )
                //     ->where('receipt note.type', 'receipt')->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
                return response()->json(['status' => 'success', 'message' => $all]);
            }

        }

    }


    public function getInstallations(Request $req)
    {
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            //  $role= Model_has_roles
            //      ::join('employee','employee.id','model_has_roles.model_id')
            //      ->join('roles','roles.id','model_has_roles.role_id')
            //      ->where('model_id','=',$emp1->id)
            //      ->first();

            $role_id=$emp1->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            if ($role_name->name == 'مدير النظام'){
                $warehouse_id = DB::table('employee')->find($emp1->id);
                return response()->json(DB::table('item_card')
                    ->join('receipt note as rec_note','rec_note.id','item_card.rec_id')
                    ->join('installation record','installation record.receipt_note_id','rec_note.id')
                    ->join('warehouse as w','w.id','item_card.warehouse_id')
                    ->select(
                        'item_card.date',
                        'item_card.quantity',
                        'w.id as ware_id',
                        'w.name as warehouse_name',
                        'rec_note.type',
                        'rec_note.picture',
                        'rec_note.order_id as order_id',
                        'rec_note.id as note_id',
                        'installation record.id as install_id',
                        'installation record.user_id',

                    )
                    ->where('rec_note.type','=','install')
                    ->groupBy('item_card.date','item_card.quantity','ware_id','warehouse_name','rec_note.type','rec_note.picture','order_id'
                        ,'note_id','install_id',
                        'installation record.user_id')
                    ->orderBy('rec_note.id')
                    ->get()
                );
            }

            else{
                $warehouse_id = DB::table('employee')->find($emp1->id);
                return response()->json(DB::table('item_card')
                    ->join('receipt note as rec_note','rec_note.id','item_card.rec_id')
                    ->join('installation record','installation record.receipt_note_id','rec_note.id')
                    ->join('warehouse as w','w.id','item_card.warehouse_id')
                    ->select(
                        'item_card.date',
                        'item_card.quantity',
                        'w.id as ware_id',
                        'w.name as warehouse_name',
                        'rec_note.type',
                        'rec_note.picture',
                        'rec_note.order_id as order_id',
                        'rec_note.id as note_id',
                        'installation record.id as install_id',
                        'installation record.user_id',

                    )
                    ->where('rec_note.type','=','install')
                    ->where('warehouse_id', '=', $warehouse_id->warehouse_id)
                    ->groupBy('item_card.date','item_card.quantity','ware_id','warehouse_name','rec_note.type','rec_note.picture','order_id'
                        ,'note_id','install_id',
                        'installation record.user_id')
                    ->orderBy('rec_note.id')
                    ->get()
                );
                // $emp_id = $emp1->id;
                // $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                // $all = Receipt_noteModel
                //     ::join('receipt as r', 'receipt note.id', '=', 'r.recept_note_id')
                //     ->join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                //     ->join('item as it', 'i.item_id', '=', 'it.id')
                //     ->select(
                //         'i.date',
                //         'i.quantity',
                //         'i.warehouse_id',
                //         'receipt note.type',
                //         'receipt note.picture',
                //         'receipt note.order_id',
                //         'r.Supplier_id',
                //         'receipt note.id as receipt_note_id',
                //         'it.name'
                //     )
                //     ->where('receipt note.type', 'receipt')->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
                return response()->json(['status' => 'success', 'message' => $all]);
            }

        }

    }


    public function getContracts(Request $req)
    {
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            //  $role= Model_has_roles
            //      ::join('employee','employee.id','model_has_roles.model_id')
            //      ->join('roles','roles.id','model_has_roles.role_id')
            //      ->where('model_id','=',$emp1->id)
            //      ->first();

            $role_id=$emp1->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            if ($role_name->name == 'مدير النظام'){
                $warehouse_id = DB::table('employee')->find($emp1->id);
                return response()->json(DB::table('item_card')
                    ->join('receipt note as rec_note','rec_note.id','item_card.rec_id')
                    ->join('contract','contract.receipt_note_id','rec_note.id')
                    ->join('warehouse as w','w.id','item_card.warehouse_id')
                    ->select(
                        'item_card.date',
                        'item_card.quantity',
                        'w.id as ware_id',
                        'w.name as warehouse_name',
                        'rec_note.type',
                        'rec_note.picture',
                        'rec_note.order_id as order_id',
                        'rec_note.id as note_id',
                        'contract.id as contract_id',
                        'contract.price1',
                        'contract.end_date'

                    )
                    ->where('rec_note.type','=','contract')
                    ->groupBy('item_card.date','item_card.quantity','ware_id','warehouse_name','rec_note.type','rec_note.picture','order_id'
                        ,'note_id','contract_id',
                        'contract.end_date','contract.price1')
                    ->orderBy('rec_note.id')
                    ->get()
                );
            }

            else{
                $warehouse_id = DB::table('employee')->find($emp1->id);
                return response()->json(DB::table('item_card')
                    ->join('receipt note as rec_note','rec_note.id','item_card.rec_id')
                    ->join('contract','contract.receipt_note_id','rec_note.id')
                    ->join('warehouse as w','w.id','item_card.warehouse_id')
                    ->select(
                        'item_card.date',
                        'item_card.quantity',
                        'w.id as ware_id',
                        'w.name as warehouse_name',
                        'rec_note.type',
                        'rec_note.picture',
                        'rec_note.order_id as order_id',
                        'rec_note.id as note_id',
                        'contract.id as contract_id',
                        'contract.price1',
                        'contract.end_date'

                    )
                    ->where('rec_note.type','=','contract')
                    ->where('warehouse_id', '=', $warehouse_id->warehouse_id)
                    ->groupBy('item_card.date','item_card.quantity','ware_id','warehouse_name','rec_note.type','rec_note.picture','order_id'
                        ,'note_id','contract_id',
                        'contract.end_date','contract.price1')
                    ->orderBy('rec_note.id')
                    ->get()
                );
                // $emp_id = $emp1->id;
                // $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                // $all = Receipt_noteModel
                //     ::join('receipt as r', 'receipt note.id', '=', 'r.recept_note_id')
                //     ->join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                //     ->join('item as it', 'i.item_id', '=', 'it.id')
                //     ->select(
                //         'i.date',
                //         'i.quantity',
                //         'i.warehouse_id',
                //         'receipt note.type',
                //         'receipt note.picture',
                //         'receipt note.order_id',
                //         'r.Supplier_id',
                //         'receipt note.id as receipt_note_id',
                //         'it.name'
                //     )
                //     ->where('receipt note.type', 'receipt')->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
                return response()->json(['status' => 'success', 'message' => $all]);
            }

        }

    }
}
