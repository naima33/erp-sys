<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\fiscal_periodModel;
use App\Models\ledgerModel;
use App\Models\AccountModel;
use Carbon\Carbon;
use App\Models\credit_listModel;
use App\Models\debit_listModel;
use App\Models\EmployeesModel;
use App\Models\Statement_financial_positionModel;
use Elibyy\TCPDF\Facades\TCPDF;
use PDF;
use Illuminate\Support\Facades\View;

class Statement_financial_positionController extends Controller
{
    public function Print_Statement_financial(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $fiscal_period = fiscal_periodModel::find($id);
            $res=Statement_financial_positionModel::where('fiscal_period_id',$id)->first();

                // echo 'exist';
                $Statement_financial_id=$res->id;
                $Creditors = credit_listModel::where('statement_list_id',$Statement_financial_id)->get();
                $debitors = debit_listModel::where('statement_list_id',$Statement_financial_id)->get();
                $result=[
                    'creditor'=>$Creditors,
                    'debitor'=> $debitors,
                    'مجموع_الأصول'=> $res->total_credits,
                    'مجموع_الخصوم' =>$res->total_debits,
                    'result' => $res->result,
                    'fiscal_period_name'=>$fiscal_period->name,
                    'from_date'=>$fiscal_period->from_date,
                    'to_date'=>$fiscal_period->to_date
                ];

               // return $result;
                $filename = 'statement_financial.pdf';
                        $view = View::make('statement_financial', ['results' => $result]);
                        $html = $view->render();

                        $pdf = new TCPDF;

                        $pdf::SetTitle('statement financial');
                        $pdf::SetFont('aealarabiya', '', 18);
                        $pdf::setRTL(true);
                        $pdf::AddPage();

                        $pdf::writeHTML($html, true, false, true, false, '');

                        $pdf::Output(public_path($filename), 'F');

                        return response()->download(public_path($filename));
           // }

        }
    }

    public function get_Statement_financial(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $fiscal_period = fiscal_periodModel::find($id);
            $res=Statement_financial_positionModel::where('fiscal_period_id',$id)->get();


                echo 'exist';
                $Statement_financial_id=$res[0]->id;
                $Creditors = credit_listModel::where('statement_list_id',$Statement_financial_id)->get();
                $debitors = debit_listModel::where('statement_list_id',$Statement_financial_id)->get();
                $result=[
                    'creditor'=>$Creditors,
                    'debitor'=> $debitors,
                    'مجموع_الأصول'=> $res[0]->total_credits,
                    'مجموع_الخصوم' =>$res[0]->total_debits,
                    'result' => $res[0]->result,
                    'fiscal_period_name'=>$fiscal_period->name,
                    'from_date'=>$fiscal_period->from_date,
                    'to_date'=>$fiscal_period->to_date
                ];

               // return $result;

                        return response()->json(['statu'=>'succes','message'=>$result]);
           // }

        }
    }

    public function Create_Statement_financial(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            $m=0;$n=0;
            //log
            addLog("create",$emp_id,"Create_Statement_financial");

            $fiscal_period = fiscal_periodModel::find($id);
            $Now = Carbon::now();
            // echo $Now . ' ';
            // echo $fiscal_period->to_date . ' ';
            if ($fiscal_period->to_date < $Now) {
                // echo 'if';
                $Statement_financial = new Statement_financial_positionModel();
                $Statement_financial->fiscal_period_id = $id;
                $Statement_financial->Net_profit = 0;
                $Statement_financial->save();

                $Statement_financial_id = $Statement_financial->id;
                $ledger = ledgerModel::where('fiscal_period_id', $id)->get();
                $sizeL = count($ledger);
                $balance = array();
                $debitors = array();
                $Creditors = array();
                for ($i = 0; $i < $sizeL; $i++) {
                    $Base_account = AccountModel::where('id', $ledger[$i]->account_id)->first();
                    $balance[$i] = $Base_account->balance;
                    $j = $Base_account->level_number;
                    $account = $Base_account;

                    while ($j > 1) {
                        $father = $account->father_id;
                        $account = AccountModel::where('id', $father)->first();
                        // echo ' account '.$account->id.' ';
                        $j = $account->level_number;
                    }

                    $type = $account->name;
                    if ($type == 'خصوم') {
                        $Credit = new credit_listModel();
                        $Creditors[$m] = $balance[$i]+100;
                        $Credit->account_id = $Base_account->id;
                        $Credit->statement_list_id = $Statement_financial_id;
                        $Credit->amount = $balance[$i]+100;
                        $Credit->save();
                        $m++;
                    } elseif ($type = 'أصول') {
                        $debitors[$n] = $balance[$i]+100;
                        $debit = new debit_listModel();
                        $debit->account_id = $Base_account->id;
                        $debit->statement_list_id = $Statement_financial_id;
                        $debit->amount = $balance[$i]+100;
                        $debit->save();
                        $n++;
                    }
                }

                $size = count($Creditors);
                $sum_cred = 0;
                for ($k = 0; $k < $size; $k++) {
                    $sum_cred = $sum_cred + $Creditors[$k];
                }

                $size = count($debitors);
                $sum_deb = 0;
                for ($k = 0; $k < $size; $k++) {
                    $sum_deb = $sum_deb + $debitors[$k];
                }
                $r=null;
                //return $sum_cred.' '. $sum_deb;
                //if ($sum_deb == $sum_cred) {
                    $Statement_financial = Statement_financial_positionModel::find($Statement_financial_id);
                  //  return $Statement_financial;
                    $Statement_financial->result = 'equal';
                    $Statement_financial->total_credits = $sum_cred;
                    $Statement_financial->total_debits = $sum_deb;
                    $Statement_financial->fiscal_period_id = $id;
                    $Statement_financial->save();
                    $r='equal';
             //   }
                $Creditors = credit_listModel::join('account','credit_list.account_id','=','account.id')
                ->select('name')
                ->where('statement_list_id',$Statement_financial_id)
                ->get();
                $debitors = debit_listModel::join('account','debit_list.account_id','=','account.id')
                ->select('name')
                ->where('statement_list_id',$Statement_financial_id)
                ->get();
                $result=[
                    'creditor'=>$Creditors,
                    'debitor'=> $debitors,
                    'مجموع_الأصول'=> $sum_cred,
                    'مجموع_الخصوم' =>$sum_deb,
                    'result' => $r,
                    'fiscal_period_name'=>$fiscal_period->name,
                    'from_date'=>$fiscal_period->from_date,
                    'to_date'=>$fiscal_period->to_date
                ];
                return  [$result];
                //  response()->json([$result]);
            } else
                return "هذه الفترة لم تنتهي بعد";
        }
    }

    public function financial_Analayses(Request $req,$BaseFiscal, $Other_fiscal)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;

            $Bfiscal = fiscal_periodModel::where('id', $BaseFiscal)->first();
            $OFiscal = fiscal_periodModel::where('id', $Other_fiscal)->first();
            $base_StLise = Statement_financial_positionModel::where('fiscal_period_id', $BaseFiscal)->first();
            $other_StList = Statement_financial_positionModel::where('fiscal_period_id', $Other_fiscal)->first();

            $TotalDebit = $base_StLise->total_debits - $other_StList->total_debits;
            echo 'd ' . $TotalDebit . '  ';
            $DebitResult = ($TotalDebit / $base_StLise->total_debits) * 100;
            if ($DebitResult > 0)
                $DebitResult = -$DebitResult;
            else
                $DebitResult = -$DebitResult;
            $DebitResult = round($DebitResult, 2);

            $TotalCredit = $base_StLise->total_credits - $other_StList->total_credits;
            echo 'c ' . $TotalCredit . '  ';
            $CreditResult = ($TotalCredit / $base_StLise->total_credits) * 100;
            if ($CreditResult > 0)
                $CreditResult = -$CreditResult;
            else
                $CreditResult = -$CreditResult;
            $CreditResult = round($CreditResult, 2);
            echo $base_StLise->year;
            $base_year = $Bfiscal->name;
            $other_year = $OFiscal->name;

            $result =
                [
                    'first_year' => $Bfiscal->year,
                    'second_year' => $OFiscal->year,
                    'totalDo' => $other_StList->total_debits,
                    'totalCo' => $other_StList->total_credits,
                    'totalDB' => $base_StLise->total_debits,
                    'totalCB' => $base_StLise->total_credits,
                    'changeD' => $TotalDebit,
                    'changeC' => $TotalCredit,
                    'base_year' => $base_year,
                    'other_year' => $other_year,
                    'credit' => $CreditResult,
                    'debit' => $DebitResult,
                ];
            $filename = 'financial_analyses.pdf';
            $view = View::make('financialAnalysis', ['result' => $result]);
            $html = $view->render();

            $pdf = new TCPDF;

            $pdf::SetTitle('financial analyses');
            $pdf::SetFont('aealarabiya', '', 18);
            $pdf::setRTL(true);
            $pdf::AddPage();

            $pdf::writeHTML($html, true, false, true, false, '');

            $pdf::Output(public_path($filename), 'F');

            return response()->download(public_path($filename));
            //return $DebitResult .' $ '.$CreditResult;
        }
    }

    public function financial_Analayses_create(Request $req,$BaseFiscal, $Other_fiscal)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;

            $Bfiscal = fiscal_periodModel::where('id', $BaseFiscal)->first();
            $OFiscal = fiscal_periodModel::where('id', $Other_fiscal)->first();
            $base_StLise = Statement_financial_positionModel::where('fiscal_period_id', $BaseFiscal)->first();
            $other_StList = Statement_financial_positionModel::where('fiscal_period_id', $Other_fiscal)->first();

            $TotalDebit = $base_StLise->total_debits - $other_StList->total_debits;
            echo 'd ' . $TotalDebit . '  ';
            $DebitResult = ($TotalDebit / $base_StLise->total_debits) * 100;
            if ($DebitResult > 0)
                $DebitResult = -$DebitResult;
            else
                $DebitResult = -$DebitResult;
            $DebitResult = round($DebitResult, 2);

            $TotalCredit = $base_StLise->total_credits - $other_StList->total_credits;
            echo 'c ' . $TotalCredit . '  ';
            $CreditResult = ($TotalCredit / $base_StLise->total_credits) * 100;
            if ($CreditResult > 0)
                $CreditResult = -$CreditResult;
            else
                $CreditResult = -$CreditResult;
            $CreditResult = round($CreditResult, 2);
            echo $base_StLise->year;
            $base_year = $Bfiscal->name;
            $other_year = $OFiscal->name;

            $result =
                [
                    'first_year' => $Bfiscal->year,
                    'second_year' => $OFiscal->year,
                    'totalDo' => $other_StList->total_debits,
                    'totalCo' => $other_StList->total_credits,
                    'totalDB' => $base_StLise->total_debits,
                    'totalCB' => $base_StLise->total_credits,
                    'changeD' => $TotalDebit,
                    'changeC' => $TotalCredit,
                    'base_year' => $base_year,
                    'other_year' => $other_year,
                    'credit' => $CreditResult,
                    'debit' => $DebitResult,
                ];

            return response()->json(['status','succes','message',$result]);
            //return $DebitResult .' $ '.$CreditResult;
        }
    }

}
