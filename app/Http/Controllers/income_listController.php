<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\fiscal_periodModel;
use App\Models\ledgerModel;
use App\Models\AccountModel;
use App\Models\ExpensesModel;
use App\Models\income_listModel;
use App\Models\EmployeesModel;
use App\Models\RevenuesModel;
use Carbon\Carbon;
use Elibyy\TCPDF\Facades\TCPDF;
use PDF;
use Illuminate\Support\Facades\View;

use PhpParser\Node\Expr\BinaryOp\Equal;

class income_listController extends Controller
{
    public function income_list_print(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        // else {
        //     $emp_id = $emp->id;

        //     $m=0;$n=0;
        //     //log
        //     addLog("create",$emp_id,"income_list");

        //     $fiscal_period = fiscal_periodModel::find($id);
        //     $Now = Carbon::now();
        //     echo $Now . ' ';
        //     echo $fiscal_period->to_date . ' ';
        //     if ($fiscal_period->to_date < $Now)
        //      {
        //         $income_list = new income_listModel();
        //         $income_list->fiscal_period_id = $id;
        //         $income_list->save();

        //         $income_list_id= $income_list->id;
        //         $ledger = ledgerModel::where('fiscal_period_id', $id)->get();
        //         $size = count($ledger);
        //         echo ' '.$size.' ';
        //         $balance = array();
        //         $Expenses = array();
        //         $Revenues = array();
        //         for ($i = 0; $i < $size; $i++) {
        //             $Base_account = AccountModel::where('id', $ledger[$i]->account_id)->first();
        //             $balance[$i] = $Base_account->balance;
        //             $j = $Base_account->level_number;
        //             $account = $Base_account;

        //             while ($j > 1) {
        //                 $father = $account->father_id;
        //                 $account = AccountModel::where('id', $father)->first();
        //                 // echo ' account '.$account->id.' ';
        //                 $j = $account->level_number;
        //             }
        //             $type = $account->name;
        //             echo  ' ' .$type. ' ';
        //             if ($type== 'مصروفات') {
        //                 $Expenses[$m] = $balance[$i];
        //                 $Expense = new ExpensesModel();
        //                 $Expense->account_id = $Base_account->id;
        //                 $Expense->income_list_id= $income_list_id;
        //                 $Expense->amount = $balance[$i];
        //                 $m++;
        //             }
        //             if ($type=='إيرادات') {
        //                 $Revenues[$n] = $balance[$i];
        //                 $Revenue = new RevenuesModel();
        //                 $Revenue->account_id = $Base_account->id;
        //                 $Revenue->list_id = $income_list_id;
        //                 $Revenue->amount = $balance[$i];
        //                 $n++;
        //             }
        //         }
        //         $size = count($Expenses);
        //         $sum_exp = 0;
        //         for ($k = 0; $k < $size; $k++) {

        //             $sum_exp = $sum_exp + $Expenses[$k];
        //         }
        //         $size = count($Revenues);
        //         $sum_rev = 0;
        //         for ($k = 0; $k < $size; $k++) {
        //             $sum_rev = $sum_rev + $Revenues[$k];
        //         }

        //         $r=null;
        //         if ($sum_exp > $sum_rev) {
        //             $income_list = income_listModel::find($income_list_id);
        //             $income_list->result = 'lose';
        //             $income_list->fiscal_period_id = $id;
        //             $income_list->total_expences=$sum_exp;
        //             $income_list->total_revenue	=$sum_rev;
        //             $income_list->save();
        //             $r='lose';
        //         } elseif ($sum_exp < $sum_rev) {
        //             $income_list = income_listModel::find($income_list_id);
        //             $income_list->result = 'profit';
        //             $income_list->fiscal_period_id = $id;
        //             $income_list->total_expences=$sum_exp;
        //             $income_list->total_revenue	=$sum_rev;
        //             $income_list->save();
        //             $r='profit';
        //             //نحسب صافي الربح ونخزن في جدول قائمة الربح
        //         }
        //          else {
        //             $income_list = income_listModel::find($income_list_id);
        //             $income_list->result = 'equal';
        //             $income_list->fiscal_period_id = $id;
        //             $income_list->total_expences=$sum_exp;
        //             $income_list->total_revenue	=$sum_rev;
        //             $income_list->save();
        //             $r='equal';
        //         }
        //         $Expenses = ExpensesModel::where('income_list_id',$income_list_id)->get();
        //         $revenues = RevenuesModel::where('list_id',$income_list_id)->get();
        //         $result=[
        //         'مصروفات'=>$Expenses,
        //         'إيرادات'=>$revenues,
        //         'مجموع المصروفات'=>$sum_exp,
        //         'مجموع الإيرادات'=>$sum_rev,
        //         'result'=>$r
        //         ];
        //         // return response()->json(['status' => 'success', 'message' => 'done']);
        //         $filename = 'financial_analyses.pdf';
        //         $view = View::make('financialAnalysis', ['result' => $result]);
        //         $html = $view->render();

        //         $pdf = new TCPDF;

        //         $pdf::SetTitle('financial analyses');
        //         $pdf::SetFont('aealarabiya', '', 18);
        //         $pdf::setRTL(true);
        //         $pdf::AddPage();

        //         $pdf::writeHTML($html, true, false, true, false, '');

        //         $pdf::Output(public_path($filename), 'F');

        //         return response()->download(public_path($filename));
        //     }
        //      else
        //         return response()->json("هذه الفترة لم تنتهي بعد");
        // }
        else{
            $fiscal_period = fiscal_periodModel::find($id);
            $res=income_listModel::where('fiscal_period_id',$id)->get();
            $Income_list_id=$res[0]->id;
            $Creditors = ExpensesModel::where('income_list_id',$Income_list_id)->get();
            $debitors = RevenuesModel::where('list_id',$Income_list_id)->get();
            $result=[
                'expences'=>$Creditors,
                'revenue'=> $debitors,
                'total_expences'=> $res[0]->total_expences,
                'total_revenue' =>$res[0]->total_revenue,
                'result' => $res[0]->result,
                'fiscal_period_name'=>$fiscal_period->name,
                'from_date'=>$fiscal_period->from_date,
                'to_date'=>$fiscal_period->to_date
            ];
             // return $result;
             $filename = 'Income_List.pdf';
             $view = View::make('IncomeList', ['results' => $result]);
             $html = $view->render();

             $pdf = new TCPDF;

             $pdf::SetTitle('Income List');
             $pdf::SetFont('aealarabiya', '', 18);
             $pdf::setRTL(true);
             $pdf::AddPage();

             $pdf::writeHTML($html, true, false, true, false, '');

             $pdf::Output(public_path($filename), 'F');

             return response()->download(public_path($filename));
// }
        }
    }

    public function income_list_get(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }

        //     //log
        //     addLog("create",$emp_id,"income_list");

        else{
            $fiscal_period = fiscal_periodModel::find($id);
            $res=income_listModel::where('fiscal_period_id',$id)->get();
            $Income_list_id=$res[0]->id;
            $Creditors = ExpensesModel::where('income_list_id',$Income_list_id)->get();
            $debitors = RevenuesModel::where('list_id',$Income_list_id)->get();
            $result=[
                'expences'=>$Creditors,
                'revenue'=> $debitors,
                'total_expences'=> $res[0]->total_expences,
                'total_revenue' =>$res[0]->total_revenue,
                'result' => $res[0]->result,
                'fiscal_period_name'=>$fiscal_period->name,
                'from_date'=>$fiscal_period->from_date,
                'to_date'=>$fiscal_period->to_date
            ];
             // return $result;

             return response()->json(['status'=>'success','message'=>$filename]);
// }
        }
    }
    public function income_list_create(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        else {
            $emp_id = $emp->id;

            $m=0;$n=0;
            //log
            addLog("create",$emp_id,"income_list");

            $fiscal_period = fiscal_periodModel::find($id);
             $Now = Carbon::now();
            // echo $Now . ' ';
         //   echo $fiscal_period->to_date . ' ';
            if ($fiscal_period->to_date < $Now)
             {
                $income_list = new income_listModel();
                $income_list->fiscal_period_id = $id;
                $income_list->save();

                $income_list_id= $income_list->id;
                $ledger = ledgerModel::where('fiscal_period_id', $id)->get();
                $size = count($ledger);
                //return $size;
             //   echo ' '.$size.' ';
                $balance = array();
                $Expenses = array();
                $Revenues = array();
                for ($i = 0; $i < $size; $i++) {
                    $Base_account = AccountModel::where('id', $ledger[$i]->account_id)->first();
                    $balance[$i] = $Base_account->balance;
                    $j = $Base_account->level_number;
                    $account = $Base_account;

                    while ($j > 1) {
                        $father = $account->father_id;
                        $account = AccountModel::where('id', $father)->first();
                        // echo ' account '.$account->id.' ';
                        $j = $account->level_number;
                    }
                    $type = $account->name;
                   // echo  ' ' .$type. ' ';
                    if ($type== 'مصروفات') {
                        $Expenses[$m] = $balance[$i]+100;
                        $Expense = new ExpensesModel();
                        $Expense->account_id = $Base_account->id;
                        $Expense->income_list_id= $income_list_id;
                        $Expense->amount = $balance[$i];
                        $Expense->save();
                        $m++;
                    }
                    if ($type=='إيرادات') {
                        $Revenues[$n] = $balance[$i]+100;
                        $Revenue = new RevenuesModel();
                        $Revenue->account_id = $Base_account->id;
                        $Revenue->list_id = $income_list_id;
                        $Revenue->amount = $balance[$i];
                        $Revenue->save();
                        $n++;
                    }
                }
                $sizeEx = count($Expenses);
                $sizeRev = count($Revenues);

             if($sizeEx>0 && $sizeRev>0)
             {
                $sum_exp = 0;
                for ($k = 0; $k < $sizeEx; $k++) {
                    $sum_exp = $sum_exp + $Expenses[$k];
                }

                $sum_rev = 0;
                for ($k = 0; $k < $sizeRev; $k++) {
                    $sum_rev = $sum_rev + $Revenues[$k];
                }
                $r=null;
                if ($sum_exp > $sum_rev) {
                    $income_list = income_listModel::find($income_list_id);
                    $income_list->result = 'lose';
                    $income_list->fiscal_period_id = $id;
                    $income_list->total_expences=$sum_exp;
                    $income_list->total_revenue	=$sum_rev;
                    $income_list->save();
                    $r='lose';
                } elseif ($sum_exp < $sum_rev) {
                    $income_list = income_listModel::find($income_list_id);
                    $income_list->result = 'profit';
                    $income_list->fiscal_period_id = $id;
                    $income_list->total_expences=$sum_exp;
                    $income_list->total_revenue	=$sum_rev;
                    $income_list->save();
                    $r='profit';
                    //نحسب صافي الربح ونخزن في جدول قائمة الربح
                }
                 else {
                    $income_list = income_listModel::find($income_list_id);
                    $income_list->result = 'equal';
                    $income_list->fiscal_period_id = $id;
                    $income_list->total_expences=$sum_exp;
                    $income_list->total_revenue	=$sum_rev;
                    $income_list->save();
                    $r='equal';
                }
                $Expenses = ExpensesModel
                 ::join('account','account.id','=','expenses.account_id')
                ->where('income_list_id',$income_list_id)
                ->get();
                $revenues = RevenuesModel::join('account','account.id','=','revenues.account_id')
                ->where('list_id',$income_list_id)->get();
                $result=[
                    'Expenses'=>$Expenses,
                    'revenues'=>$revenues,
                    'total_Expenses'=>$sum_exp,
                    'total_revenues'=>$sum_rev,
                    'result'=>$r
                    ];
               return [$result];
             }
             else
             return 0;
            }
             else
                return response()->json("هذه الفترة لم تنتهي بعد");
        }
    }

}

