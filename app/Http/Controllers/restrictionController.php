<?php

namespace App\Http\Controllers;

use App\Models\credit_entriesModel;
use App\Models\debit_entriesModel;
use App\Models\EmployeesModel;
use App\Models\fiscal_periodModel;
use App\Models\ledgerModel;
use App\Models\restrictionModel;
use Carbon\Carbon;
use App\Models\CurrencyModel;
use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class restrictionController extends Controller
{
    public function Non_Migrated_Restrictions(Request $req, $id1)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $fiscal_period_id = fiscal_periodModel::find($id1);

            if ($fiscal_period_id != null) {
                $Restrictions = restrictionModel::where('restriction.statu', 0)
                    ->where('fiscal_period_id', $id1)->get();

                $filename = 'hello_world.pdf';
                $view = View::make('accountReport', ['Restrictions' => $Restrictions]);
                $html = $view->render();

                $pdf = new TCPDF;

                $pdf::SetTitle('Hello World');
                $pdf::SetFont('aealarabiya', '', 18);
                $pdf::setRTL(true);
                $pdf::AddPage();

                $pdf::writeHTML($html, true, false, true, false, '');

                $pdf::Output(public_path($filename), 'F');

                return response()->download(public_path($filename));
                //return response()->json($Restrictions);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'fiscal_period not found',
                ]);
            }
        }
    }

    public function ShowNon_Migrated_Restrictions(Request $req, $id1)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $result = array();
            $allCredit = array();
            $allDebit = array();
            $fiscal_period_id = fiscal_periodModel::find($id1);
            if ($fiscal_period_id != null) {
                $Restrictions = restrictionModel::join('currency', 'restriction.currency_id', '=', 'currency.id')
                    ->join('fiscal_period', 'restriction.fiscal_period_id', '=', 'fiscal_period.id')
                    ->select(
                        'restriction.id',
                        'restriction.amount_after_transfer',
                        'restriction.explain_procces',
                        'restriction.date',
                        'restriction.type',
                        'currency.id as curr_id',
                        'currency.name as currency_name',
                        'fiscal_period.id as fis_id',
                        'fiscal_period.name as fisacal_name'
                    )->where('restriction.statu', 0)
                    ->where('fiscal_period_id', $id1)->get();

                //
                //return $Restrictions;
                $size = count($Restrictions);
                for ($i = 0; $i < $size; $i++) {
                    $rest_id = $Restrictions[$i]->id;
                    $period = $Restrictions[$i]->fis_id;
                    $credits = credit_entriesModel::join('account', 'credit_entries.credit_account_id', '=', 'account.id')->select('account.name')
                        ->where('restriction_id', $rest_id)->where('fiscal_period', $period)->get();
                    // $account_name=accountModel::where('id',$credits->credit_account_id)->first();
                    $debits = debit_entriesModel::join('account', 'debit_entries.debit_account_id', '=', 'account.id')->select('account.name')
                        ->where('restriction_id', $rest_id)->where('fiscal_period', $period)->get();
                    $sizec = count($credits);
                    $sized = count($debits);
                    for ($j = 0; $j < $sizec; $j++) {
                        $allCredit[$j] = $credits[$j]->name;
                    }
                    for ($j = 0; $j < $sized; $j++) {
                        $allDebit[$j] = $debits[$j]->name;
                    }
                    $result[$i] =
                        [
                        'credit' => $allCredit,

                        'debits' => $allDebit,
                        'amount' => $Restrictions[$i]->amount_after_transfer,
                        'explain procces' => $Restrictions[$i]->explain_procces,
                        'date' => $Restrictions[$i]->date,
                        'type' => $Restrictions[$i]->type,
                        'currency' => $Restrictions[$i]->currency_name,
                        'fiscal name' => $Restrictions[$i]->fisacal_name,

                    ];
                }

                return response()->json($result);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'fiscal_period not found',
                ]);
            }
        }
    }

    // public function add_restriction(Request $req)
    // {
    //     $token = $req->bearerToken();
    //     $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

    //     if ($emp == null) {
    //         return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
    //     } else {
    //         $emp_id = $emp->id;
    //         // app('auth')->guard()->setUser(User::query()->find($id));
    //         // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
    //         //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
    //         // }
    //         $Restrictions = new restrictionModel();
    //         $Restrictions->amount_after_transfer = $req->amount;
    //         $Restrictions->currency_id = $req->currency_id;
    //         $Restrictions->fiscal_period_id = $req->fiscal_period_id;
    //         $Restrictions->explain_procces = $req->explain_procces;
    //         $Restrictions->date = $req->date;
    //         $Restrictions->save();
    //         $id = $Restrictions->id;

    //         for ($i = 0; $i < count($req->input('credit_account_id')); $i++) {
    //             $Credit = new credit_entriesModel();
    //             $Credit->restriction_id = $id;
    //             $Credit->credit_account_id = $req->input('credit_account_id')[$i];
    //             $Credit->credit_amount = $req->input('credit_amount')[$i];
    //             $Credit->fiscal_period = $req->fiscal_period_id;
    //             $Credit->save();
    //         }
    //         for ($j = 0; $j < count($req->input('debit_account_id')); $j++) {
    //             $debit = new debit_entriesModel();
    //             $debit->restriction_id = $id;
    //             $debit->debit_account_id = $req->input('debit_account_id')[$j];
    //             $debit->debit_amount = $req->input('debit_amount')[$j];
    //             $debit->fiscal_period = $req->fiscal_period_id;
    //             $debit->save();
    //         }
    //         return "تمت إضافة القيد بنجاح";
    //     }
    // }
    public function add_restriction(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $Restrictions = new restrictionModel();
            $amount=$req->amount;

            $Restrictions->currency_id = $req->currency_id;
            $Amount_transfer=CurrencyModel::where('id',$req->currency_id)->first();
            $Restrictions->amount_after_transfer =$amount * $Amount_transfer->exchange_rate;
            $Restrictions->fiscal_period_id = $req->fiscal_period_id;
            $Restrictions->explain_procces = $req->explain_procces;
            $Restrictions->date = $req->date;
            $Restrictions->save();
            $id = $Restrictions->id;

            for ($i = 0; $i < count($req->input('credit_account_id')); $i++) {
                $Credit = new credit_entriesModel();
                $Credit->restriction_id = $id;
                $Credit->credit_account_id = $req->input('credit_account_id')[$i];
                $Credit->credit_amount = $req->input('credit_amount')[$i];
                $Credit->fiscal_period = $req->fiscal_period_id;
                $Credit->save();
            }
            for ($j = 0; $j < count($req->input('debit_account_id')); $j++) {
                $debit = new debit_entriesModel();
                $debit->restriction_id = $id;
                $debit->debit_account_id = $req->input('debit_account_id')[$j];
                $debit->debit_amount = $req->input('debit_amount')[$j];
                $debit->fiscal_period = $req->fiscal_period_id;
                $debit->save();
            }
          return response()->json('تم إضافة القيد');

        }
    }


    public function showRestriction(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $result = array();
            $allCredit = array();
            $allDebit = array();
            $allCreditAmount = array();
            $allDebitAmount = array();
            $fiscal_period_id = fiscal_periodModel::find($id);
            if ($fiscal_period_id != null) {
                $Restrictions = restrictionModel::join('currency', 'restriction.currency_id', '=', 'currency.id')
                    ->join('fiscal_period', 'restriction.fiscal_period_id', '=', 'fiscal_period.id')
                    ->select(
                        'restriction.id',
                        'restriction.amount_after_transfer',
                        'restriction.explain_procces',
                        'restriction.date',
                        'restriction.type',
                        'currency.id as curr_id',
                        'currency.name as currency_name',
                        'fiscal_period.id as fis_id',
                        'fiscal_period.name as fisacal_name',
                        'restriction.statu'
                    )
                    ->where('fiscal_period_id', $id)->get();

                //
                //return $Restrictions;
                $size = count($Restrictions);
                for ($i = 0; $i < $size; $i++) {
                    $rest_id = $Restrictions[$i]->id;
                    $period = $Restrictions[$i]->fis_id;
                    $credits = credit_entriesModel::join('account', 'credit_entries.credit_account_id', '=', 'account.id')->select('account.name', 'credit_entries.credit_account_id as credit_id','credit_entries.credit_amount')
                        ->where('restriction_id', $rest_id)->where('fiscal_period', $period)->get();
                    // $account_name=accountModel::where('id',$credits->credit_account_id)->first();
                    $debits = debit_entriesModel::join('account', 'debit_entries.debit_account_id', '=', 'account.id')->select('account.name', 'debit_entries.debit_account_id as debit_id','debit_entries.debit_amount')
                        ->where('restriction_id', $rest_id)->where('fiscal_period', $period)->get();
                    $sizec = count($credits);
                    $sized = count($debits);
                    for ($j = 0; $j < $sizec; $j++) {
                        // $allCredit[$j] = $credits[$j]->name;
                        $allCredit[$j] = $credits[$j]->name;
                    }
                    for ($j = 0; $j < $sizec; $j++) {
                        // $allCredit[$j] = $credits[$j]->name;
                        $allCreditAmount[$j] = $credits[$j]->credit_amount;
                    }
                    for ($j = 0; $j < $sized; $j++) {
                        $allDebit[$j] = $debits[$j]->name;
                        // $allDebit[$j] =
                        // $debits[$j]->name
                        // ;
                    }
                    for ($j = 0; $j < $sized; $j++) {
                        $allDebitAmount[$j] = $debits[$j]->debit_amount;
                        // $allDebit[$j] =
                        // $debits[$j]->name
                        // ;
                    }
                    $result[$i] =
                        [
                        'credit' => $allCredit,
                        'creditAmount' => $allCreditAmount,
                        'id' => $rest_id,
                        'debits' => $allDebit,
                        'debitsAmount' => $allDebitAmount,
                        'amount' => $Restrictions[$i]->amount_after_transfer,
                        'explain_procces' => $Restrictions[$i]->explain_procces,
                        'date' => $Restrictions[$i]->date,
                        'type' => $Restrictions[$i]->type,
                        'currency' => $Restrictions[$i]->currency_name,
                        'fiscal_name' => $Restrictions[$i]->fisacal_name,
                        'fiscal_period_id' =>$id,
                        'statu' => $Restrictions[$i]->statu,
                    ];
                }

                return response()->json($result);
            } else {
                return response()->json('fiscal not found');
            }
        }
    }

    public function RestrictionUpdate(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $res_id = $req->id;
            $res = restrictionModel::find($res_id);
            if ($res == null) {
                return 'not found';
            } else {
                $restriction = restrictionModel::where('id', $res_id)->first();
                if ($restriction->statu == 1) {
                    $restriction->amount_after_transfer = $req->amount;
                    $restriction->currency_id = $req->currency_id;
                    $restriction->fiscal_period_id = $req->fiscal_period_id;
                    $restriction->explain_procces = $req->explain_procces;
                    $restriction->date = $req->date;
                    $restriction->save();
                    $id = $res_id;

                    $allCredit = credit_entriesModel::where('restriction_id', $res_id)->get();
                    $sizeC = count($allCredit);

                    for ($i = 0; $i < $sizeC; $i++) {
                        $Credit = credit_entriesModel::where('id', $allCredit[$i]->id)->first();
                        $Credit->restriction_id = $id;
                        $Credit->credit_account_id = $req->input('credit')[$i];
                        $Credit->credit_amount = $req->input('creditAmount')[$i];
                        $Credit->fiscal_period = $req->fiscal_period_id;
                        $Credit->save();
                    }
                    $allDebit = debit_entriesModel::where('restriction_id', $res_id)->get();
                    $sizeD = count($allDebit);
                    for ($j = 0; $j < $sizeD; $j++) {
                        $debit = debit_entriesModel::where('id', $allDebit[$j]->id)->first();
                        $debit->restriction_id = $id;
                        $debit->debit_account_id = $req->input('debits')[$j];
                        $debit->debit_amount = $req->input('debitsAmount')[$j];
                        $debit->fiscal_period = $req->fiscal_period_id;
                        $debit->save();
                    }
                    return response()->json('تم تعديل بنجاح ');
                } else {
                    return 'can not update';
                }
            }
        }
    }

    public function Migrate_Restriction(Request $req, $res_id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            $emp_id = $emp->id;
            // app('auth')->guard()->setUser(User::query()->find($id));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            // }
            $restriction = restrictionModel::find($res_id);
            if ($restriction == null) {
                return 'not found';
            } else {
                if ($restriction->statu == 1) {
                    $restriction->statu = 2;
                    $restriction->save();
                    $allCredit = credit_entriesModel::where('restriction_id', $res_id)->get();
                    $allDebit = debit_entriesModel::where('restriction_id', $res_id)->get();
                    $sizeC = count($allCredit);
                    $sizeD = count($allDebit);
                    for ($i = 0; $i < $sizeC; $i++) {
                        $ledger = new ledgerModel();
                        $ledger->fiscal_period_id = $restriction->fiscal_period_id;
                        $ledger->restriction_id = $allCredit[$i]->restriction_id;
                        $ledger->amount = $allCredit[$i]->credit_amount;
                        $ledger->account_id = $allCredit[$i]->credit_account_id;
                        $ledger->type = 'credit';
                        $ledger->id_auther_account = $allDebit[0]->debit_account_id;
                        $ledger->date = Carbon::now();
                        $ledger->save();
                    }

                    for ($i = 0; $i < $sizeD; $i++) {
                        $ledger = new ledgerModel();
                        $ledger->fiscal_period_id = $restriction->fiscal_period_id;
                        $ledger->restriction_id = $allDebit[$i]->restriction_id;
                        $ledger->amount = $allDebit[$i]->debit_amount;
                        $ledger->account_id = $allDebit[$i]->debit_account_id;
                        $ledger->type = 'debit';
                        $ledger->id_auther_account = $allCredit[0]->credit_account_id;
                        $ledger->date = Carbon::now();
                        $ledger->save();
                    }
                    return 'done';
                } else {
                    return 'القيد مرحل من قبل';
                }

            }
        }
    }
}
