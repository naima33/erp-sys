<?php

namespace App\Http\Controllers;

use App\Models\EmployeesModel;
use App\Models\WarehouseModel;
use App\Models\ItemCardModel;
use Carbon\Carbon;
use App\Models\ItemModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemsController extends Controller
{
    public function addItem(Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("create", $emp_id, "addItem");

            $item = new ItemModel();
            $item->name = $req->name;
            $item->code = $req->code;
            $item->price = $req->price;
            $item->category_id = $req->category_id;
            $item->unit = $req->unit;
            $item->Minimum_quantity = $req->Minimum_quantity;
            $item->Maximum_quantity = $req->Maximum_quantity;
            $item->save();
            $item_id=$item->id;
            $all_warehouse=WarehouseModel::get();
            $size=count($all_warehouse);
            for($i=0;$i<$size;$i++)
            {
               $now=Carbon::now();
               $item_card=new ItemCardModel();
               $item_card->item_id=$item_id;
               $item_card->type='input+';
               $item_card->date=$now;
               $item_card->code=$req->code;
               $item_card->quantity=0;
               $item_card->current_quantity=0;
               $item_card->warehouse_id=$all_warehouse[$i]->id;
               $item_card->rec_id=null;
               $item_card->del_id=null;
               $item_card->save();
            }
            return response()->json(['success' => "item is added"]);
        }

    }

    protected function getRulesadd()
    {
        return $rules = [
            'name' => 'required|unique:item,name',
            'code' => 'required|numeric|unique:item,code',
            'price' => 'required|numeric',
            'category_id' => 'required',
            'unit' => 'required',
        ];
    }

    protected function getMessageadd()
    {
        return $message = [
            'name.required' => 'item name is required',
            'name.unique' => 'item name must be unique',
            'code.required' => 'item code is required',
            'code.numeric' => 'item code must be numeric',
            'code.unique' => 'item code must be unique',
            'price.required' => 'item price is required',
            'price.numeric' => 'item price must be numeric',
            'category_id.required' => 'item category_id is required',
            'unit.required' => 'item unit is required',
        ];
    }

    public function show($id, Request $req)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض معلومات مادة')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            if (ItemModel::find($id)) {
                $item = ItemModel::find($id);

                return response()->json($item);
            } else {
                return response()->json(['error' => "not found item"]);
            }
        }
    }

    public function getAllItem(Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            // echo $warehouse_id->warehouse_id;
            $AllItem = ItemModel::join('category', 'item.category_id', '=', 'category.id')
                ->join('item_card', 'item_card.item_id', '=', 'item.id')
                ->select(DB::raw('sum(item_card.current_quantity) as quantity'), 'category.name as category_name', 'item.id', 'item.name',
                    'item.code', 'item.price', 'item.unit', 'item.Minimum_quantity', 'item.Maximum_quantity')
                ->where('item_card.type', 'input')->where('item_card.warehouse_id', '=', $warehouse_id->warehouse_id)
                ->groupBy('item.id', 'category.name', 'item.name', 'item.code', 'item.price',
                    'item.unit', 'item.Minimum_quantity', 'item.Maximum_quantity')->get();

            return response()->json($AllItem);
        }
    }

    public function update(Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("update", $emp_id, "updateItem");

            $item = new ItemModel();
            $item = ItemModel::find($req->id);
            if ($item != null) {
                $item->code = $req->code;
                $item->name = $req->name;
                $item->category_id = $req->category_id;
                $item->unit = $req->unit;
                $item->price = $req->price;
                if ($item->save()) {
                    return response()->json(['seccess' => ' updated item']);
                } else {
                    return response()->json(['error' => " not  updated seccessfuly"]);
                }

            } else {
                return response()->json(['error' => "not found"]);
            }
        }

    }

    protected function getRulesup()
    {
        return $rules = [
            'code' => 'required|numeric|unique:item,code',
            'price' => 'required|numeric',
            'category_id' => 'required',
            'unit' => 'required',

        ];
    }

    protected function getMessageup()
    {
        return $message = [
            'code.required' => 'item code is required',
            'code.numeric' => 'item code must be numeric',
            'code.ubnique' => 'item code must be unique',
            'price.required' => 'item price is required',
            'price.numeric' => 'item price must be numeric',
            'category_id.required' => 'item category_id is required',
            'unit.required' => 'item unit is required',
        ];
    }

    public function search($name, Request $req)
    {

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $search_i = $name;
            $data = ItemModel::where('name', 'Like', '%' . $search_i . '%')->orWhere('price', 'Like', '%' . $search_i . '%')
                ->orWhere('unit', 'Like', '%' . $search_i . '%')
                ->orWhere('code', 'Like', '%' . $search_i . '%')->get();

            return response()->json($data);
        }
    }
    public function Items(Request $req)
    {
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token','id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
       $all=ItemModel::all();
       return response()->json($all);
          }
    }

}
