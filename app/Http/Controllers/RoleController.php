<?php

namespace App\Http\Controllers;

use App\Models\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\EmployeesModel;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    public $roles;
    public $messages;

    public function GetAllRoles(Request $req)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض الأدوار')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token','id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

        return Response()->json(Role::get());
            }
    }

    public function CreateRole(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token','id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("create",$emp_id,"CreateRole");


            //validation
        $validator = $this->CreateValidator($request);
        if($validator->fails())
        {
            return response()->json($validator->errors());
        }

        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions([$request->input('permission')]);

        return response()->json(['status' =>"success",'message' =>"نمت إضافة الدور بنجاح"]);
      }
    }

    public function GetRole($id,Request $request)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض صلاحيات الدور')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token','id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

        if (DB::table('roles')->find($id))
        {
            $rolePermissions = Role::join("role_has_permissions","role_has_permissions.role_id","=","roles.id")
                ->join("permissions","permissions.id","=","role_has_permissions.permission_id")
                ->where("role_has_permissions.role_id",$id)
                ->get();

            return ($rolePermissions);
        }
        return response()->json(['status' =>"error",'message' =>"هذا الدور غير موجود"]);
     }
    }

    public function EditRole(Request $request, $id)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token','id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("update",$emp_id,"EditRole");


            if (DB::table('roles')->find($id))
        {
            //validation
            $validator = $this->EditValidator($request);
            if($validator->fails())
            {
                return response()->json($validator->errors());
            }

            $role = Role::find($id);
            $role->name = $request->input('name');
            $role->save();

            $role->syncPermissions($request->input('permission'));

            return response()->json(['status' =>"success",'message' =>"نم تعديل معلومات الدور بنجاح"]);
        }
        return response()->json(['status' =>"error",'message' =>"هذا الدور غير موجود"]);
     }
    }

    public function DeleteRole($id,Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token','id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("delete",$emp_id,"DeleteRole");


            if (DB::table('roles')->find($id))
        {
            Role::findById($id)->delete();

            return response()->json(['status' =>"success",'message' =>"نم حذف الدور بنجاح"]);
        }
        return response()->json(['status' =>"error",'message' =>"هذا الدور غير موجود"]);
        }
    }

    public function CreateValidator(Request $request)
    {
        $this->roles=[
            'name' => 'required|unique:roles|max:255',
            'permission' => 'required',
        ];

        $this->messages=[
            'name.required' => 'حقل الاسم مطلوب',
            'name.unique' => 'هذا الاسم موجود مسبقاً',
            'name.max' => 'حقل الاسم يجب ألا يتجاوز 255 محرف',
            'permission.required' => 'يجب إدخال صلاحية واحدة على الأقل',
        ];

        return Validator::make($request->all(),$this->roles ,$this->messages);
    }

    public function EditValidator(Request $request)
    {
        $this->roles=[
            'name' => 'required|max:255',
            'permission' => 'required',
        ];

        $this->messages=[
            'name.required' => 'حقل الاسم مطلوب',
            'name.max' => 'حقل الاسم يجب ألا يتجاوز 255 محرف',
            'permission.required' => 'يجب إدخال صلاحية واحدة على الأقل',
        ];

        return Validator::make($request->all(),$this->roles ,$this->messages);
    }

}
