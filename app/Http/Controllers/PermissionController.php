<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use App\Models\EmployeesModel;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public $roles;
    public $messages;

    public function GetAllPermissions(Request $req)
    {
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token','id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

        return Permission::get();
            }
    }

    public function CreatePermission(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token','id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("create",$emp_id,"CreatePermission");


            $validator = $this->Validator($request);
        if($validator->fails())
        {
            return response()->json($validator->errors());
        }

        Permission::create(['name' => $request->input('name'),'guard_name' => 'web']);

        return response()->json(['Success' =>"نمت إضافة الصلاحية بنجاح"]);
     }
    }

    public function DeletePermissionFromRole(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token','id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
            $emp_id = $emp1->id;

            //log
            addLog("delete",$emp_id,"DeletePermissionFromRole");


            DB::table('role_has_permissions')->where('permission_id', $request->input('permission_id'))->where('role_id', $request->input('role_id'))->delete();

        return response()->json(['status' =>"success",'message' =>"نم حذف الصلاحية بنجاح"]);
            }
    }

    public function Validator(Request $request)
    {
        $this->roles=[
            'name' => 'required|unique:permissions|max:255',
        ];

        $this->messages=[
            'name.required' => 'حقل الاسم مطلوب',
            'name.unique' => 'هذا الاسم موجود مسبقاً',
            'name.max' => 'حقل الاسم يجب ألا يتجاوز 255 محرف',
        ];

        return Validator::make($request->all(),$this->roles ,$this->messages);
    }
}
