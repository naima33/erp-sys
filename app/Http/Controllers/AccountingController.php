<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\EmployeesModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AccountingController extends Controller
{
    public function GetAllFiscalPeriod(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;

        //        //permission

        //        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        //        if (!app('auth')->guard()->getUser()->can('عرض كل اللجان')){
        //            return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        //        }

        return response()->json(DB::table('fiscal_period')->get());
    }

    public function GetRestrictionsInFiscalPeriod(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;

        //        //permission

        //        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        //        if (!app('auth')->guard()->getUser()->can('عرض كل اللجان')){
        //            return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        //        }

        if (DB::table('restriction')->find($id)) {
            return response()->json(DB::table('restriction')->where('fiscal_period_id', $id)->get());
        }

        return response()->json(['status' => "error", 'message' => "هذه الفترة غير موجودة"]);
    }

    public function GetStateOfTheRestriction(Request $req, $id)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;

        //        //permission

        //        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        //        if (!app('auth')->guard()->getUser()->can('عرض كل اللجان')){
        //            return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        //        }

        if (DB::table('restriction')->find($id)) {
            return response()->json(DB::table('restriction')->where('id', $id)->select('statu')->get());
        }

        return response()->json(['status' => "error", 'message' => "هذا القيد غير موجود"]);
    }

    public function GetAccountInformation(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;

        //permission
        if (session()->get('id') == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);

        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        if (!app('auth')->guard()->getUser()->can('عرض كل اللجان')) {
            return response()->json(['status' => "error", 'message' => "عذراً ليس لديك صلاحية الوصول"]);
        }

        return response()->json(DB::table('restriction')
            ->join('credit_entries', 'credit_entries.restriction_id', 'restriction.id')
            ->join('debit_entries', 'debit_entries.restriction_id', 'restriction.id')
            ->where('credit_account_id')->orWhere('debit_account_id')
            ->select('state')->get());
    }

    // public function CreateTrialBalance(Request $request)
    // {
    //     $token = $request->bearerToken();
    //     $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

    //     if ($emp == null) {
    //         return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
    //     }
    //     $id = $emp->id;

    //     //        //permission

    //     //        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
    //     //        if (!app('auth')->guard()->getUser()->can('عرض كل اللجان')){
    //     //            return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
    //     //        }

    //     //log
    //     addLog("create", $id, "CreateTrialBalance");

    //     $ledger = DB::table('ledger')->where('fiscal_period_id', $request->input('fiscal_period_id'))->get();

    //     $sum_credit = $sum_debit = 0;
    //     foreach ($ledger as $account) {
    //         if ($account->type == 'credit')
    //             $sum_credit += $account->amount;

    //         else if ($account->type == 'debit')
    //             $sum_debit += $account->amount;
    //     }
    //     DB::table('trial_balance')->insertGetId(
    //         ['fiscal_period_id' => $request->input('fiscal_period_id'), 'total_credit_balances' => $sum_credit, 'total_debit_balances' => $sum_debit]
    //     );

    //     return response()->json(['status' => "success", 'message' => "تمت الإضافة بنجاح"]);
    // }
    public function CreateTrialBalance(Request $request,$F_id)
    {
        $token = $request->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;
        //log
        addLog("create", $id, "CreateTrialBalance");

        $ledger = DB::table('ledger')
        ->join('account as a1','a1.id','=','ledger.account_id')
        ->join('account as a2','a2.id','=','ledger.id_auther_account')
        ->select('ledger.id','ledger.fiscal_period_id','ledger.type','a1.name','a2.name','ledger.amount')
        ->where('ledger.fiscal_period_id', $F_id)->get();

        $sum_credit = $sum_debit = 0;
        foreach ($ledger as $account) {
            if ($account->type == 'credit')
                $sum_credit += $account->amount;

            else if ($account->type == 'debit')
                $sum_debit += $account->amount;
        }

         if($sum_credit==$sum_debit)
         {
            DB::table('trial_balance')->insertGetId(
                ['fiscal_period_id' => $F_id, 'total_credit_balances' => $sum_credit, 'total_debit_balances' => $sum_debit]
            );
            $result=
            [
             'allaccount' =>$ledger,
             'sum_credit' =>$sum_credit,
             'sum_debit'  =>$sum_debit
            ];
            return response()->json([$result]);
           // return response()->json(['status' => "success", 'message' => "تمت الإضافة بنجاح"]);
         }

        else
        return response()->json(['status' => "error", 'message' => "الميزان ليس متوازن"]);
    }

    // public function CreateTrialBalance(Request $request,$F_id)
    // {
    //     return  DB::table('trial_balance')->where('fiscal_period_id',$F_id)->get();

    // }

    public function DivTheFiscalPeriod(Request $request)
    {
        $token = $request->bearerToken();
        $emp = EmployeesModel::select('token', 'id')->where('token', $token)->first();

        if ($emp == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        }
        $id = $emp->id;

        //log
        addLog("create", $id, "DivTheFiscalPeriod");


        //        //permission

        //        app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        //        if (!app('auth')->guard()->getUser()->can('عرض كل اللجان')){
        //            return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        //        }
        //

        $add_date = 12 / $request->input('number_of_parts');
        $from_date = Carbon::create($request->input('from_year'), $request->input('from_month'), $request->input('from_day'));
        $to_date = Carbon::create($request->input('to_year'), $request->input('to_month'), $request->input('to_day'));

        $from_year = $request->input('from_year');
        $from_month = $request->input('from_month');
        $from_day = $request->input('from_day');

        for ($i = 0; $i < $request->input('number_of_parts') - 1; $i++) {

            $add = $from_month + $add_date;
            if ($add > 12) {
                $from_year += 1;
                $from_month = $add - 12;
                DB::table('fiscal_period')->insertGetId(
                    ['name' => $request->input('name'), 'from_date' => $from_date, 'to_date' => $from_date = Carbon::create($from_year, $from_month, $from_day)]
                );
            } else {
                $from_month += $add_date;
                DB::table('fiscal_period')->insertGetId(
                    ['name' => $request->input('name'), 'from_date' => $from_date, 'to_date' => $from_date = Carbon::create($from_year, $from_month, $from_day)]
                );
            }
        }

        DB::table('fiscal_period')->insertGetId(
            ['name' => $request->input('name'), 'from_date' => $from_date, 'to_date' => $to_date]
        );

        return response()->json(['status' => "success", 'message' => "تمت الإضافة بنجاح"]);
    }

    public function CommitteeEditValidator(Request $request)
    {
        $this->roles = [
            'name' => 'required|max:100',
            'members' => 'required|max:10',
        ];

        $this->messages = [
            'name.required' => 'حقل الاسم مطلوب',
            'members.required' => 'يجب تحديد عضو واحد على الأقل',
            'members.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }
}
