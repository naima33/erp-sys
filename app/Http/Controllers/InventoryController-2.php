<?php

namespace App\Http\Controllers;

use App\Models\ItemCardModel;
use App\Models\ItemModel;
use App\Models\User;
use App\Models\rolesModel;
use App\Models\EmployeesModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use mysql_xdevapi\Collection;
use function Sodium\add;
use function Symfony\Component\String\s;


class InventoryController extends Controller
{
    public $roles;
    public $messages;

    public function GetAllCommittees(Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);


            $emp_id = $emp1->id;
            //echo $emp1->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            //echo $warehouse_id->warehouse_id;


            $role_id = $emp1->role_id;
           // echo $emp1->role_id;
            $role_name = rolesModel::where('id', $role_id)->first();
            //$role_name= 'super_admin';
            $commm=array();
            if ($role_name->name == 'مدير النظام') {

                return DB::table('committee')
                    ->join('member', 'member.id_committee', 'committee.id')
                    ->join('employee', 'employee.id', 'member.emp_id')
                    ->select('committee.id as committee_id','committee.name as committee_name','committee.warehouse_id as committee_warehouse_id',
                        'employee.id as employee_id','employee.f_name','employee.l_name')
                    ->get();
            } else if ($role_name->name == 'مدير المستودعات') {

                return DB::table('committee')
                    ->join('member', 'member.id_committee', 'committee.id')
                    ->join('employee', 'employee.id', 'member.emp_id')
                    ->where('committee.warehouse_id','=',$warehouse_id->warehouse_id)
                    ->select('committee.id as committee_id','committee.name as committee_name','committee.warehouse_id as committee_warehouse_id',
                        'employee.id as employee_id','employee.f_name','employee.l_name')
                    ->get();
            }
        }
    }

    public function GetAllMembersInCommittee($id, Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            if (DB::table('committee')->find($id)) {
                $rolePermissions = DB::table('committee')->join("member", "member.id_committee", "=", "committee.id")
                    ->join("employee", "employee.id", "=", "member.emp_id")
                    ->where("member.id_committee", $id)
                    ->get();

                return response()->json($rolePermissions);
            }
            return response()->json(['status' => "error", 'message' => "هذا الدور غير موجود"]);
        }
    }

    public function CreateCommittee(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id','warehouse_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("create",$id,"CreateCommittee");



            //validation
            $validator = $this->CommitteeCreateValidator($request);
            if ($validator->fails()) {
                return response()->json($validator->errors());
            }

            $role_id=$emp1->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            //echo $role_name->name;
            if ($role_name->name == ''){
                $committee_id = DB::table('committee')->insertGetId(
                    ['name' => $request->input('name'),'warehouse_id'=>0]
                );

                $data = collect($request->members)->toArray();
                foreach ($data as $member) {
                    DB::table('member')->insertGetId(
                        ['id_committee' => $committee_id, 'emp_id' => $member]
                    );
                }

                return response()->json(['status' => "success", 'message' => "نمت إضافة اللجنة بنجاح"]);
            }
            else if ($role_name->name == 'مدير المستودعات'){
                $committee_id = DB::table('committee')->insertGetId(
                    ['name' => $request->input('name'),'warehouse_id'=>$emp1->warehouse_id]
                );

                $data = collect($request->members)->toArray();
                foreach ($data as $member) {
                    DB::table('member')->insertGetId(
                        ['id_committee' => $committee_id, 'emp_id' => $member]
                    );
                }

                return response()->json(['status' => "success", 'message' => "نمت إضافة اللجنة بنجاح"]);
            }

            else
                return response()->json(['status' => "error", 'message' => "ليس لديك الصلاحية للقيام بهذا الإجراء"]);

        }
    }

    public function EditCommittee(Request $request, $id1)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("update",$id,"EditCommittee");

            $role_id=$emp1->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            //echo $id1;
            if ($role_name->name == 'مدير النظام'){
                if (DB::table('committee')->find($id1)) {
                    //validation
                    $validator = $this->CommitteeEditValidator($request);
                    if ($validator->fails()) {
                        return response()->json($validator->errors());
                    }

                    DB::table('committee')
                        ->where('id', $id1)
                        ->update(
                            ['name' => $request->input('name')]
                        );

                    DB::table('member')
                        ->where('id_committee', $id1)
                        ->delete();

                    $data = collect($request->input('members'))->toArray();
                    foreach ($data as $member) {
                        DB::table('member')->insertGetId(
                            ['id_committee' => $id1, 'emp_id' => $member]
                        );
                    }
                    return response()->json(['status' => "success", 'message' => "نم تعديل معلومات اللجنة بنجاح"]);
                }
                return response()->json(['status' => "error", 'message' => "هذه اللجنة غير موجودة"]);
            }

            else if ($role_name->name == 'مدير المستودعات'){
                if (DB::table('committee')->find($id1)) {
                    //validation
                    $validator = $this->CommitteeEditValidator($request);
                    if ($validator->fails()) {
                        return response()->json($validator->errors());
                    }
                   $warehouse_id= DB::table('committee')->select('warehouse_id')->find($id1);
                    //echo $warehouse_id->warehouse_id;
                    if ($warehouse_id->warehouse_id!=0){
                        DB::table('committee')
                            ->where('id', $id1)
                            ->update(
                                ['name' => $request->input('name')]
                            );

                        DB::table('member')
                            ->where('id_committee', $id1)
                            ->delete();

                        $data = collect($request->input('members'))->toArray();
                        foreach ($data as $member) {
                            DB::table('member')->insertGetId(
                                ['id_committee' => $id1, 'emp_id' => $member]
                            );
                        }
                        return response()->json(['status' => "success", 'message' => "نم تعديل معلومات اللجنة بنجاح"]);

                    }
                    else
                        return response()->json(['status' => "error", 'message' => "*لا يمكنك تعديل معلومات هذه اللجنة"]);

                }
                return response()->json(['status' => "error", 'message' => "هذه اللجنة غير موجودة"]);
            }
            return response()->json(['status' => "error", 'message' => "لا يمكنك تعديل معلومات هذه اللجنة"]);

        }
    }

    public function DeleteCommittee($id1, Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);


            $id = $emp1->id;

            //log
            addLog("delete",$id,"DeleteCommittee");



            $role_id=$emp1->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            //echo $id1;

            $warehouse_id= DB::table('committee')->select('warehouse_id')->find($id1);

            if (DB::table('committee')->find($id1)) {
                if ($role_name->name == 'مدير النظام'){
                    DB::table('committee')->where('id', $id1)->delete();

                    return response()->json(['status' => "success", 'message' => "نم حذف اللجنة بنجاح"]);
                }
                else if ($role_name->name == 'مدير المستودعات') {
                    if ($warehouse_id->warehouse_id != 0) {
                        DB::table('committee')->where('id', $id1)->delete();

                        return response()->json(['status' => "success", 'message' => "نم حذف اللجنة بنجاح"]);
                    }
                    return response()->json(['status' => "error", 'message' => "ليست لديك الصلاحية*"]);

                }
                else  return response()->json(['status' => "error", 'message' => "ليست لديك الصلاحية"]);

            }

            return response()->json(['status' => "error", 'message' => "هذه اللجنة غير موجودة"]);
        }
    }

    public function CommitteeCreateValidator(Request $request)
    {
        $this->roles = [
            'name' => 'required|unique:committee|max:100',
            'members' => 'required|max:10',
        ];

        $this->messages = [
            'name.required' => 'حقل الاسم مطلوب',
            'name.unique' => 'هذا الاسم موجود مسبقاً',
            'name.max' => 'حقل الاسم يجب ألا يتجاوز 100 كيلوبايت',
            'members.required' => 'يجب تحديد عضو واحد على الأقل',
            'members.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function CommitteeEditValidator(Request $request)
    {
        $this->roles = [
            'name' => 'required|max:100',
            'members' => 'required|max:10',
        ];

        $this->messages = [
            'name.required' => 'حقل الاسم مطلوب',
            'members.required' => 'يجب تحديد عضو واحد على الأقل',
            'members.max' => 'هذا الحقل يجب ألا يتجاوز 10 كيلوبايت',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    // public function GetAllInventories(Request $req)
    // {
    //     //permission
    //     // if (session()->get('id')==null)
    //     //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

    //     // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
    //     // if (!app('auth')->guard()->getUser()->can('عرض كل عمليات الجرد')){
    //     //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
    //     // }
    //     $token = $req->bearerToken();
    //     $emp1 = EmployeesModel::select('token', 'id','role_id','warehouse_id')->where('token', $token)->first();
    //     if ($emp1 == null)
    //         return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
    //     else {
    //         // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
    //         // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
    //         //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);


    //         $role_id = $emp1->role_id;
    //         $role_name = rolesModel::where('id', $role_id)->first();

    //         //echo $id1;

    //         if ($role_name->name == 'super_admin') {

    //             return response()->json(DB::table('inventory')->join('inventory_commettee', 'inventory_commettee.id_inventory', 'inventory.id')
    //                 ->join('inventory_item', 'inventory_item.id_onventory_committee', 'inventory_commettee.id')
    //                 ->join('item', 'item.id', 'inventory_item.item_id')
    //                 ->select('inventory.id as inventory_id','inventory.name as inventory_name', 'inventory.date', 'item.name as item_name', 'real_quantity', 'storing_quantity')->get());
    //         }
    //         else if ($role_name->name == 'manager') {

    //             return response()->json(DB::table('inventory')->join('inventory_commettee', 'inventory_commettee.id_inventory', 'inventory.id')
    //                 ->join('inventory_item', 'inventory_item.id_onventory_committee', 'inventory_commettee.id')
    //                 ->join('item', 'item.id', 'inventory_item.item_id')
    //                 ->join('committee','committee.id','inventory_commettee.id_inventory')
    //                 ->where('committee.warehouse_id','=',$emp1->warehouse_id)
    //                 ->select('inventory.id as inventory_id','inventory.name as inventory_name', 'inventory.date', 'item.name as item_name', 'real_quantity', 'storing_quantity')->get());
    //         }
    //     }
    // }

    public function GetAllInventories(Request $req)
    {
        //     // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
    //     // if (!app('auth')->guard()->getUser()->can('عرض كل عمليات الجرد')){
    //     //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
    //     // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id','warehouse_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $result=DB::table('inventory')->get();
            return $result;
        }
    }


    public function getInventoryByID()
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض كل عمليات الجرد')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id','warehouse_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);


            $role_id = $emp1->role_id;
            $role_name = rolesModel::where('id', $role_id)->first();

            //echo $id1;

            if ($role_name->name == 'مدير النظام') {

                return response()->json(DB::table('inventory')->join('inventory_commettee', 'inventory_commettee.id_inventory', 'inventory.id')
                    ->join('inventory_item', 'inventory_item.id_onventory_committee', 'inventory_commettee.id')
                    ->join('item', 'item.id', 'inventory_item.item_id')
                    ->select('inventory.id as inventory_id','inventory.name as inventory_name', 'inventory.date', 'item.name as item_name', 'real_quantity', 'storing_quantity')->get());
            }
            else if ($role_name->name == 'مدير المستودعات') {

                return response()->json(DB::table('inventory')->join('inventory_commettee', 'inventory_commettee.id_inventory', 'inventory.id')
                    ->join('inventory_item', 'inventory_item.id_onventory_committee', 'inventory_commettee.id')
                    ->join('item', 'item.id', 'inventory_item.item_id')
                    ->join('committee','committee.id','inventory_commettee.id_inventory')
                    ->where('committee.warehouse_id','=',$emp1->warehouse_id)
                    ->select('inventory.id as inventory_id','inventory.name as inventory_name', 'inventory.date', 'item.name as item_name', 'real_quantity', 'storing_quantity')->get());
            }
        }
    }

    public function GetAllItemsInInventory($id, Request $req)
    {
        //permission
        // if (session()->get('id')==null)
        //     return response()->json(['status' =>"error",'message' =>"عذراً أنت غير مسجل"]);

        // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
        // if (!app('auth')->guard()->getUser()->can('عرض كل المواد في عملية جرد معينة')){
        //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);
        // }

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            if (DB::table('inventory')->find($id)) {
                $inventory_items = DB::table('inventory')->join("inventory_commettee", "inventory_commettee.id_inventory", "=", "inventory.id")
                    ->join("inventory_item", "inventory_item.id_onventory_committee", "=", "inventory_commettee.id")
                    ->where("inventory.id", $id)
                    ->get();

                return response()->json($inventory_items);
            }
            return response()->json(['status' => "error", 'message' => "هذه العملية غير موجودة"]);
        }
    }


    public function CreateInventory(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("create",$id,"CreateInventory");


//            //validation
//            $validator = $this->InventoryCreateValidator($request);
//            if ($validator->fails()) {
//                return response()->json($validator->errors());
//            }

                    $id_inventory = DB::table('inventory')->insertGetId(
                ['name' => $request->input('name'), 'result' => 0, 'date' => Carbon::now()]
            );

            $inventory_commettee = DB::table('inventory_commettee')->insertGetId(
                ['id_committee' => $request->input('id_committee'), 'id_inventory' => $id_inventory]
            );
            return response()->json(['status' => "success", 'message' => "نمت إضافة عملية الجرد بنجاح",'inventory_committee' => $inventory_commettee]);
        }
    }

    public function InventoryResult(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("create",$id,"InventoryResult");


//            //validation
//            $validator = $this->InventoryResultCreateValidator($request);
//            if ($validator->fails()) {
//                return response()->json($validator->errors());
//            }

            $emp_id=$emp1->id;
            $warehouse_id=EmployeesModel::select('warehouse_id')->where('id',$emp_id)->first();
            //echo $warehouse_id->warehouse_id;


            $committee_name = DB::table('committee')->join('inventory_commettee','inventory_commettee.id_committee','committee.id')
                ->where('inventory_commettee.id',$request->input('inventory_committee'))->first();
            $inventory_date = DB::table('inventory')->join('inventory_commettee','inventory_commettee.id_inventory','inventory.id')
                ->where('inventory_commettee.id',$request->input('inventory_committee'))->first();
            $item_names=array();
            $sorting_quantities=array();
            $result=array();
            $real_quantity=array();


            $role_id=$emp1->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            //echo $id1;
            if ($role_name->name == 'مدير النظام'){
                if ($request->input('warehouse_id') == null){
                    for ($i = 0; $i < count($request->input('item')); $i++) {
                        $item_name = $request->input('item')[$i];
                        $item_id = DB::table('item')->select('id', 'code')->where('name', $item_name)->first();
                        $item_ids = DB::table('item_card')->where('item_id', '=', $item_id->id)
                            ->where('type', '=', 'input')
                            ->get();
                        $storing_quantity = 0;
                        foreach ($item_ids as $item) {
                            $storing_quantity += $item->current_quantity;
                        }

                        $inventory_item[] = DB::table('inventory_item')->insertGetId(
                            [
                                'item_id' => $item_id->id, 'id_onventory_committee' => $request->input('inventory_committee'),
                                'storing_quantity' => $storing_quantity, 'real_quantity' => $request->input('real_quantity')[$i]
                            ]
                        );

                        $item_names[$i]=$request->input('item')[$i];
                        $sorting_quantities[$i]=$storing_quantity;
                        $result[$i] = $storing_quantity - $request->input('real_quantity')[$i];
                        $real_quantity[$i]= $request->input('real_quantity')[$i];

                        DB::table('inventory_item')
                            ->join('inventory_commettee', 'inventory_commettee.id', 'inventory_item.id_onventory_committee')
                            ->join('inventory', 'inventory.id', 'inventory_commettee.id_inventory')
                            ->where('inventory_commettee.id', $request->input('inventory_committee'))
                            ->update(['inventory.result' => 1]);

                    }
                    return response()->json(['status' => "success", 'message' => "نم تعديل نتيجة عملية الجرد بنجاح" ,"item_name"=> $item_names,"sorting_quantities"=> $sorting_quantities,"results"=> $result,"real_quantity"=> $real_quantity,"committee_name"=> $committee_name->name,"date"=> $inventory_date->date]);

                }

                else {
                    for ($i = 0; $i < count($request->input('item')); $i++) {
                        $item_name = $request->input('item')[$i];
                        $item_id = DB::table('item')->select('id', 'code')->where('name', $item_name)->first();
                        $item_ids = DB::table('item_card')->where('item_id', '=', $item_id->id)
                            ->where('warehouse_id', '=',$request->input('warehouse_id'))
                            ->where('type', '=', 'input')
                            ->get();
                        $storing_quantity = 0;
                        foreach ($item_ids as $item) {
                            $storing_quantity += $item->current_quantity;
                        }

                        $inventory_item[] = DB::table('inventory_item')->insertGetId(
                            [
                                'item_id' => $item_id->id, 'id_onventory_committee' => $request->input('inventory_committee'),
                                'storing_quantity' => $storing_quantity, 'real_quantity' => $request->input('real_quantity')[$i]
                            ]
                        );

                        $item_names[$i]=$request->input('item')[$i];
                        $sorting_quantities[$i]=$storing_quantity;
                        $result[$i] = $storing_quantity - $request->input('real_quantity')[$i];
                        $real_quantity[$i]= $request->input('real_quantity')[$i];

                        DB::table('inventory_item')
                            ->join('inventory_commettee', 'inventory_commettee.id', 'inventory_item.id_onventory_committee')
                            ->join('inventory', 'inventory.id', 'inventory_commettee.id_inventory')
                            ->where('inventory_commettee.id', $request->input('inventory_committee'))
                            ->update(['inventory.result' => 1]);

                    }
                    return response()->json(['status' => "success", 'message' => "نم تعديل نتيجة عملية الجرد بنجاح" ,"item_name"=> $item_names,"sorting_quantities"=> $sorting_quantities,"results"=> $result,"real_quantity"=> $real_quantity,"committee_name"=> $committee_name->name,"date"=> $inventory_date->date]);

                }

            }

            elseif ($role_name->name == 'مدير المستودعات') {
                echo $warehouse_id->warehouse_id;
                    for ($i = 0; $i < count($request->input('item')); $i++) {
                        $item_name = $request->input('item')[$i];
                        $item_id = DB::table('item')->select('id', 'code')->where('name', $item_name)->first();
                        $item_ids = DB::table('item_card')->where('item_id', '=', $item_id->id)
                            ->where('type', '=', 'input')
                            ->where('warehouse_id', '=',$warehouse_id->warehouse_id)
                            ->get();
                        $storing_quantity = 0;
                        foreach ($item_ids as $item) {
                            $storing_quantity += $item->current_quantity;
                        }

                        $inventory_item[] = DB::table('inventory_item')->insertGetId(
                            [
                                'item_id' => $item_id->id, 'id_onventory_committee' => $request->input('inventory_committee'),
                                'storing_quantity' => $storing_quantity, 'real_quantity' => $request->input('real_quantity')[$i]
                            ]
                        );

                        $item_names[$i] = $request->input('item')[$i];
                        $sorting_quantities[$i] = $storing_quantity;
                        $result[$i] = $storing_quantity - $request->input('real_quantity')[$i];
                        $real_quantity[$i] = $request->input('real_quantity')[$i];

                        DB::table('inventory_item')
                            ->join('inventory_commettee', 'inventory_commettee.id', 'inventory_item.id_onventory_committee')
                            ->join('inventory', 'inventory.id', 'inventory_commettee.id_inventory')
                            ->where('inventory_commettee.id', $request->input('inventory_committee'))
                            ->update(['inventory.result' => 1]);

                    }
                    return response()->json(['status' => "success", 'message' => "نم تعديل نتيجة عملية الجرد بنجاح", "item_name" => $item_names, "sorting_quantities" => $sorting_quantities, "results" => $result, "real_quantity" => $real_quantity, "committee_name" => $committee_name->name, "date" => $inventory_date->date]);

            }
        }
    }

    public function GetInventoryItem(Request $request)
    {
        //permission

        $token = $request->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

           return DB::table('inventory_item')->where('id_onventory_committee',$request->input('inventory_committee'))->get();

        }

    }

    public function DeleteInventory($id, Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $id = $emp1->id;

            //log
            addLog("delete",$id,"DeleteInventory");


            if (DB::table('inventory')->find($id)) {
                DB::table('inventory')->where('id', $id)->delete();

                return response()->json(['status' => "success", 'message' => "نم حذف العملية بنجاح"]);
            }

            return response()->json(['status' => "error", 'message' => "هذه العمليلة غير موجودة"]);
        }
    }

    public function InventoryCreateValidator(Request $request)
    {
        $this->roles = [
            'name' => 'required|unique:inventory|max:100',
            'item' => 'required',
            'committee_id' => 'required',
        ];

        $this->messages = [
            'name.required' => 'حقل الاسم مطلوب',
            'name.unique' => 'هذا الاسم موجود مسبقاً',
            'name.max' => 'حقل الاسم يجب ألا يتجاوز 100 كيلوبايت',
            'item.required' => 'يرجى تحديد مادة واحدة على الأقل',
            'committee_id.required' => 'يرجى تحديد لجنة الجرد',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function InventoryResultCreateValidator(Request $request)
    {
        $this->roles = [
            'real_quantity' => 'required',
            'item_id' => 'required',
        ];

        $this->messages = [
            'required' => 'هذا الحقل مطلوب',
        ];

        return Validator::make($request->all(), $this->roles, $this->messages);
    }

    public function GetLog(){
        return response()->json(DB::table('log')->get());
    }
}
