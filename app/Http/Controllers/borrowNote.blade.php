
<div  width="200" height="200">
    <div style="text-align: right;">الجمهورية العربية السورية</div>
    
</div>


<div style="text-align: center;">مذكرة إعارة</div>

<br><br>
<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
      <th style="border: 1px solid black;border-collapse: collapse">رقم المذكرة</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">رقم الأمر</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">نوع االمذكرة</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">اسم الموظف</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">المادة</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">الكمية</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">اسم المستودع</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">التاريخ</th>
    </tr>
    @foreach ($results as $result)
    <tr>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['delivery_note_id']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['order_id']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['type']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['emp_f_name']}} {{$result['emp_l_name']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['name']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['quantity']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['warehouse_name']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$result['date']}}</td>
    </tr>
    @endforeach


  </table>
