<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
//use Dotenv\Validator;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use \Illuminate\Support\Facades\DB;
use Hash;

class UserController extends Controller
{
    public function GetAllUsers()
    {
        $user = User::get();
        return $user;
    }

    public function CreateUser(Request $request)
    {


        $input = $request->all();
        //$input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole([2]);

        return ('User created successfully');
    }

    public function GetUser($id)
    {
        $rolePermissions = Role::join("model_has_roles","model_has_roles.role_id","=","roles.id")
            ->where("model_has_roles.model_id",$id)
            ->get();

        return ($rolePermissions);
    }

    public function EditUser(Request $request, $id)
    {

        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($request->input('roles'));

        return ('User updated successfully');
    }

    public function DeleteUser($id)
    {
        User::find($id)->delete();
        return ('User deleted successfully');
    }
}
