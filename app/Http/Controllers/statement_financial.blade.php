<?php
use App\Models\CurrencyModel;
use App\Models\credit_entriesModel;
use App\Models\debit_entriesModel;
use App\Models\accountModel;
?>
<div   style="overflow: hidden;width=20 ;height=20">
    <div style="margin: 2px;float:right;width: 10;height : 100%;">الجمهورية العربية السورية</div>
    <img src="logo.jpg" alt="Italian Trulli" style="float: left;width: 100%;height : 100%;object-fit: cover;">
</div>


<div style="text-align: center;"> قائمة المركز المالي</div>

<br><br>
<table style="border: 1px solid black;border-collapse: collapse">
    <tr>
      <th style="border: 1px solid black;border-collapse: collapse">الفترة المالية </th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">تاريخ بداية الفترة </th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">تاريخ نهاية الفترة </th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">الدائنين</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">المدينين</th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">مجموع الدائنين </th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">مجموع المدينين </th>
      <th style="border: 1px solid black;border-collapse: collapse;text-align: center;">النتيجة  </th>
    </tr>

    <tr>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['fiscal_period_name']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['from_date']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['to_date']}}</td>

        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">

        <table>

        @foreach ($results['creditor'] as $cred)
        <tr style="border: 1px solid black;border-collapse: collapse;text-align: center; ">
           <?php
            $account_name=accountModel::where('id',$cred->account_id)->first();
           ?>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center; "> {{$account_name->name}} </td>
        </tr>
         @endforeach
        </table>
        </td>

        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">
        <table>
            @foreach ($results['debitor'] as $deb)
            <tr style="border: 1px solid black;border-collapse: collapse;text-align: center; ">
           <?php
            $account_name=accountModel::where('id',$deb->debit_account_id)->first();
           ?>
            <td style="border: 1px solid black;border-collapse: collapse;text-align: center; "> {{$account_name->name}} </td>
            </tr>
         @endforeach
        </table>
        </td>

        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['مجموع_الخصوم']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['مجموع_الأصول']}}</td>
        <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">{{$results['result']}}</td>
    </tr>



  </table>
