<?php

namespace App\Http\Controllers;

use App\Models\DeliveryModel;
use App\Models\Delivery_noteModel;
use App\Models\Receipt_noteModel;
use App\Models\EmployeesModel;
use App\Models\ItemBorrowModel;
use App\Models\ItemCardModel;
use App\Models\ItemModel;
use App\Models\rolesModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Elibyy\TCPDF\Facades\TCPDF;
use PDF;
use Illuminate\Support\Facades\View;

class DeliveryController extends Controller
{
    public function deliver_Items(Request $req)
    {
        //permission

        // $emp_id=session()->get('id');
        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp1 == null) {
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        } else {
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            $emp_id = $emp1->id;

            //log
            addLog("create",$emp_id,"deliver_Items");

            $item_in = null;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();

            for ($i = 0; $i < count($req->input('item_name')); $i++) {
                $item_name = $req->input('item_name')[$i];

                $item_id = ItemModel::select('id')->where('name', $item_name)->first();

                $item_in = ItemCardModel::select('quantity', 'current_quantity')->where('item_id', $item_id->id)
                    ->where('type', 'input')->where('warehouse_id', $warehouse_id->warehouse_id)
                    ->where('current_quantity', '>', '0')
                    ->get();
                $size = count($item_in);

                $sum_in = 0;

                for ($j = 0; $j < $size; $j++) {
                    $sum_in = $sum_in + $item_in[$j]->current_quantity;
                }

                $curr_quantity = $sum_in;
                if ($curr_quantity < $req->input('quantity')[$i]) {
                    // return response()->json(['error'=>'quantity of the item '.$item_name. ' is not enough']);
                    return response()->json(['status' => 'error', 'message' => 'quantity of the item ' . $item_name . ' is not enough']);
                }

                $sum_in = 0;
                $sum_out = 0;
            }

            $delivery_note = new Delivery_noteModel();
            $delivery_note->date = Carbon::now();
            $delivery_note->type = 'delivery';
            $delivery_note->order_id = $req->order_id;
            if ($req->pic != null) {
                $file_extension = $req->pic->getClientOriginalExtension();
                $file_name = time() . '.' . $file_extension;
                $path = 'images/delivery';
                $req->pic->move($path, $file_name);
                $delivery_note->picture = $file_name;
            }
            $delivery_note->save();
            $del_id = $delivery_note->id;

            for ($i = 0; $i < count($req->input('item_name')); $i++) {
                $item_name = $req->input('item_name')[$i];
                $item_id = ItemModel::select('id', 'code')->where('name', $item_name)->first();
                $item_in = ItemCardModel::select('id', 'quantity', 'current_quantity')->where('item_id', $item_id->id)
                    ->where('type', 'input')->where('warehouse_id', $warehouse_id->warehouse_id)
                    ->where('current_quantity', '>', '0')->orderBy('date')
                    ->get();

                $items = array();
                $size = count($item_in);
                $rest = $req->input('quantity')[$i];
                for ($k = 0; $k < $size; $k++) {

                    if ($item_in[$k]->current_quantity < $rest) {
                        $item = ItemCardModel::find($item_in[$k]->id);

                        if ($item != null) {
                            $rest = $rest - $item->current_quantity;

                            $item->current_quantity = 0;
                            $item->save();
                        }
                        if ($rest == 0) {
                            break;
                        } else {
                            continue;
                        }
                    }
                    if ($item_in[$k]->current_quantity > $rest) {

                        $item = ItemCardModel::find($item_in[$k]->id);
                        if ($item != null) {
                            if ($k == 0) {
                                $rest = $item->current_quantity - $req->input('quantity')[$i];
                            } else {
                                $rest = $item->current_quantity - $rest;
                            }
                            $item->current_quantity = $rest;
                            $item->save();
                        }

                        break;
                    }

                    if ($item_in[$k]->current_quantity = $rest) {
                        $item = ItemCardModel::find($item_in[$k]->id);
                        if ($item != null) {
                            if ($k == 0) {
                                $rest = $item->current_quantity - $rest;
                            } else {
                                $rest = $item->current_quantity - $rest;
                            }
                            $item->current_quantity = $rest;
                            $item->save();
                        }

                        break;
                    }
                }
                $item_card = new ItemCardModel();
                $item_card->item_id = $item_id->id;
                $item_card->type = "output";
                $item_card->date = Carbon::now();
                $item_card->code = $item_id->code;
                $item_card->quantity = $req->input('quantity')[$i];
                $item_card->del_id = $del_id;
                $item_card->warehouse_id = $warehouse_id->warehouse_id;
                $item_card->save();
            }

            $delivery = new DeliveryModel();
            $delivery->delivery_note_id = $del_id;
            $delivery->name_of_destination = $req->destination;
            $delivery->save();
            return response()->json(['status' => 'success', 'message' => 'item is delivered to the distination']);
        }
    }

    public function Borrow_Items(Request $req)
    {
        //permission

        $token = $req->bearerToken();
        $emp1 = EmployeesModel::select('token', 'id')->where('token', $token)->first();
        if ($emp1 == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp1->id;


            //log
            addLog("create",$emp_id,"Borrow_Items");

            $item_in = null;

            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            for ($i = 0; $i < count($req->input('item_name')); $i++) {
                $item_name = $req->input('item_name')[$i];

                $item_id = ItemModel::select('id')->where('name', $item_name)->first();

                $item_in = ItemCardModel::select('quantity', 'current_quantity')->where('item_id', $item_id->id)
                    ->where('type', 'input')->where('warehouse_id', $warehouse_id->warehouse_id)
                    ->where('current_quantity', '>', '0')
                    ->get();
                $size = count($item_in);

                $sum_in = 0;

                for ($j = 0; $j < $size; $j++) {
                    $sum_in = $sum_in + $item_in[$j]->current_quantity;
                }

                $curr_quantity = $sum_in;
                if ($curr_quantity < $req->input('quantity')[$i]) {
                    // return response()->json(['error'=>'quantity of the item '.$item_name. ' is not enough']);
                    return response()->json(['status' => 'error', 'message' => 'quantity of the item ' . $item_name . ' is not enough']);
                }

                $sum_in = 0;
                $sum_out = 0;
            }
            $delivery_note = new Delivery_noteModel();
            $delivery_note->date = Carbon::now();
            $delivery_note->type = 'borrow';
            $delivery_note->order_id = 46;
            $delivery_note->save();
            $del_id = $delivery_note->id;

            for ($i = 0; $i < count($req->input('item_name')); $i++) {
                $item_name = $req->input('item_name')[$i];
                $item_id = ItemModel::select('id', 'code')->where('name', $item_name)->first();
                $item_card = new ItemCardModel();
                $item_card->item_id = $item_id->id;
                $item_card->type = "output";
                $item_card->date = Carbon::now();
                $item_card->code = $item_id->code;
                $item_card->quantity = $req->input('quantity')[$i];
                $item_card->del_id = $del_id;
                $item_card->warehouse_id = $warehouse_id->warehouse_id;
                $item_card->save();
            }

            $borrow = new ItemBorrowModel();
            $full_name = $req->user_name;
             $split = explode(" ", $full_name);
            $f_name =   $split[0];
            $l_name =   $split[1];
            //echo $f_name;
            $name = EmployeesModel::select('id', 'f_name', 'l_name')
                ->where('f_name', $f_name)->where('l_name', $l_name)->first();
            $borrow->user_id = $name->id;
            $borrow->id_delivery_note = $del_id;

            if ($borrow->save()) {
                return response()->json(['status' => 'success', 'message' => 'items was borrowed to employee '.$full_name]);
            }
        }
    }

    public function getAllDelivery(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $role_id=$emp->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            if($role_name->name=='مدير المستودعات' || $role_name->name=="موظف مستودع" || $role_name->name=="أمين مستودع")
            {

                $emp_id = $emp->id;
                $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                $all = Delivery_noteModel
                    ::join('item_card as i', 'delivery note.id', '=', 'i.del_id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                    ->select(
                         'delivery note.date',
                        'w.name as warehouse_name',
                        'delivery note.type',
                        'delivery note.picture',
                        'delivery note.order_id',
                        'delivery note.id as delivery_note_id',

                    )
                    ->where('delivery note.type', 'delivery')->Where('i.warehouse_id', $warehouse_id->warehouse_id)
                    ->groupBy('delivery note.date','delivery_note_id','delivery note.type','delivery note.picture','delivery note.order_id','warehouse_name')
                    ->get();
                    return response()->json(['status' => 'success', 'message' => $all]);
             }
                elseif($role_name->name=='مدير النظام')
                {

                    $emp_id = $emp->id;
                    $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                    $all = Delivery_noteModel
                    ::join('item_card as i', 'delivery note.id', '=', 'i.del_id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                    ->select(
                        'delivery note.date',
                        'w.name as warehouse_name',
                        'delivery note.type',
                        'delivery note.picture',
                        'delivery note.order_id',
                        'delivery note.id as delivery_note_id',

                    )
                        ->where('delivery note.type', 'delivery')
                        ->groupBy('delivery note.date','delivery_note_id','delivery note.type','delivery note.picture','delivery note.order_id','warehouse_name')
                        ->get();
                     return response()->json(['status' => 'success', 'message' => $all]);
                }


        }
    }

    public function getAllBorrow(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $role_id=$emp->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();
            // app('auth')->guard()->setUser(User::query()->find(session()->get('id')));
            // if (!app('auth')->guard()->getUser()->can('عرض كل الأصناف')){
            //     return response()->json(['status' =>"error",'message' =>"عذراً ليس لديك صلاحية الوصول"]);

            if($role_name->name=='مدير المستودعات' || $role_name->name=="موظف مستودع" || $role_name->name=="أمين مستودع")
            {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();

            $all = Delivery_noteModel
                ::join('item borrowing as b', 'delivery note.id', '=', 'b.id_delivery_note')
                ->join('employee as emp', 'b.user_id', '=', 'emp.id')
                ->join('item_card as card', 'delivery note.id', '=', 'card.del_id')
                ->join('warehouse as w', 'card.warehouse_id', '=', 'w.id')
                ->select(
                    'card.date',
                    'w.id',
                    'w.name as warehouse_name',
                    'delivery note.type as type',
                    'delivery note.picture',
                    'delivery note.order_id as order_id',
                    'emp.f_name as emp_f_name',
                    'emp.l_name as emp_l_name',
                    'delivery note.id as delivery_note_id',

                )
                ->where('delivery note.type', 'borrow')->Where('card.warehouse_id', $warehouse_id->warehouse_id)
                ->groupBy('card.date','w.id','type','warehouse_name','delivery note.picture','order_id','emp_f_name','emp_l_name','delivery_note_id')
                ->get();
            //return 'none';
            return response()->json(['status' => 'success', 'message' => $all]);
           }
           elseif($role_name->name=='مدير النظام')
           {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();

            $all = Delivery_noteModel
                ::join('item borrowing as b', 'delivery note.id', '=', 'b.id_delivery_note')
                ->join('employee as emp', 'b.user_id', '=', 'emp.id')
                ->join('item_card as card', 'delivery note.id', '=', 'card.del_id')

                ->join('warehouse as w', 'card.warehouse_id', '=', 'w.id')
                ->select(
                    'card.date',

                    'w.id',
                    'w.name as warehouse_name',
                    'delivery note.type as type',
                    'delivery note.picture',
                    'delivery note.order_id as order_id',
                    'emp.f_name as emp_f_name',
                    'emp.l_name as emp_l_name',
                    'delivery note.id as delivery_note_id',

                )
                ->where('delivery note.type', 'borrow')
                ->groupBy('card.date','w.id','type','warehouse_name','delivery note.picture','order_id','emp_f_name','emp_l_name','delivery_note_id')

                ->get();

            return response()->json(['status' => 'success', 'message' => $all]);
           }
        }
    }

    public function getAllTransfer(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $role_id=$emp->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();
            if($role_name->name=='مدير المستودعات' || $role_name->name=="موظف مستودع" || $role_name->name=="أمين مستودع")
            {
                $emp_id = $emp->id;
                $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();

                $all = Delivery_noteModel
                    ::join('transfer to warehouse as t', 'delivery note.id', '=', 't.id_delivery_note')
                    ->join('warehouse as other_warehouse','t.to_warehouse_id','=','other_warehouse.id')
                    ->join('item_card as i', 'delivery note.id', '=', 'i.del_id')
                    ->join('item as it', 'i.item_id', '=', 'it.id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                    ->select(
                        'i.date',
                        'i.warehouse_id',
                        'delivery note.type',
                        'delivery note.picture',
                        'delivery note.order_id',
                        'other_warehouse.name as other_warehouse',
                        'delivery note.id as delivery_note_id',
                         't.status as status',
                        'w.name as warehouse_name'
                    )
                    ->where('delivery note.type', 'transfer')->Where('i.warehouse_id', $warehouse_id->warehouse_id)
                    ->groupBy('i.date','i.warehouse_id','delivery note.type','delivery note.picture','delivery note.order_id','other_warehouse',
                      'delivery_note_id','status','warehouse_name')
                    ->get();

                return response()->json(['status' => 'success','type'=>'delivery','message' => $all]);
            }
             elseif($role_name->name=='مدير النظام')
             {
                $emp_id = $emp->id;
                $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();

                $all = Delivery_noteModel
                ::join('transfer to warehouse as t', 'delivery note.id', '=', 't.id_delivery_note')
                ->join('warehouse as other_warehouse','t.to_warehouse_id','=','other_warehouse.id')
                ->join('item_card as i', 'delivery note.id', '=', 'i.del_id')
                ->join('item as it', 'i.item_id', '=', 'it.id')
                ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                ->select(
                    'i.date',
                    'i.warehouse_id',
                    'delivery note.type',
                    'delivery note.picture',
                    'delivery note.order_id',
                    'other_warehouse.name as other_warehouse',
                    'delivery note.id as delivery_note_id',
                    't.status as status',
                    'w.name as warehouse_name'
                )
                    ->where('delivery note.type', 'transfer')
                    ->groupBy('i.date','i.warehouse_id','delivery note.type','delivery note.picture','delivery note.order_id','other_warehouse',
                      'delivery_note_id','status','warehouse_name')
                    ->get();

                return response()->json(['status' => 'success','type'=>'delivery', 'message' => $all]);
             }
        }
    }

    public function getAllTransferReceipt(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $role_id=$emp->role_id;

            $role_name=rolesModel::where('id',$role_id)->first();
            if($role_name->name=='مدير المستودعات' || $role_name->name=="موظف مستودع" || $role_name->name=="أمين مستودع")
            {
                $emp_id = $emp->id;
                $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();

                $all = Receipt_noteModel
                ::join('transfer as t', 'receipt note.id', '=', 't.note_id')
                ->join('warehouse as other_warehouse','t.from_warehouse_id','=','other_warehouse.id')
                ->join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                ->select(
                    'i.date',
                    'i.warehouse_id',
                    'receipt note.type',
                    'receipt note.picture',
                    'receipt note.order_id',
                    'other_warehouse.name as other_warehouse',
                    'receipt note.id as receipt_note_id',
                    't.status as status',
                    'w.name as warehouse_name'
                )
                    ->where('receipt note.type', 'transfer')->Where('i.warehouse_id', $warehouse_id->warehouse_id)
                    ->groupBy('i.date','i.warehouse_id','receipt note.type','receipt note.picture','receipt note.order_id','other_warehouse',
                      'receipt_note_id','status','warehouse_name')
                    ->get();

                return response()->json(['status' => 'success','type'=>'receipt', 'message' => $all]);
            }
             elseif($role_name->name=='مدير النظام')
             {
                $emp_id = $emp->id;
                $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();

                $all =  Receipt_noteModel
                ::join('transfer as t', 'receipt note.id', '=', 't.note_id')
                ->join('warehouse as other_warehouse','t.from_warehouse_id','=','other_warehouse.id')
                ->join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                ->select(
                    'i.date',
                    'i.warehouse_id',
                    'receipt note.type',
                    'receipt note.picture',
                    'receipt note.order_id',
                    'other_warehouse.name as other_warehouse',
                    'receipt note.id as receipt_note_id',
                    't.status as status',
                    'w.name as warehouse_name'
                )
                    ->where('receipt note.type', 'transfer')
                    ->groupBy('i.date','i.warehouse_id','receipt note.type','receipt note.picture','receipt note.order_id','other_warehouse',
                    'receipt_note_id','status','warehouse_name')
                    ->get();

                return response()->json(['status' => 'success','type'=>'receipt', 'message' => $all]);
             }
        }
    }

    public function PrintDeliveryNote($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = Delivery_noteModel
            ::join('delivery as d', 'delivery note.id', '=', 'd.delivery_note_id')
            ->join('item_card as i', 'delivery note.id', '=', 'i.del_id')
            ->join('item as it', 'i.item_id', '=', 'it.id')
            ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
            ->select(
                'i.date',
                'i.quantity',
                'w.id',
                'w.name as warehouse_name',
                'delivery note.type',
                'delivery note.picture',
                'delivery note.order_id',
                'd.name_of_destination',
                'delivery note.id as delivery_note_id',
                'it.name'
            )
            ->where('delivery note.id',$id)->where('delivery note.type', 'delivery')->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
           // return response()->json(['status' => 'success', 'message' => $all]);

           $filename = 'deliverynote.pdf';
           $view = View::make('deliverynote', ['results' => $all]);
           $html = $view->render();

           $pdf = new TCPDF;

           $pdf::SetTitle('deliverynote.blade');
           $pdf::SetFont('aealarabiya', '', 18);
           $pdf::setRTL(true);
           $pdf::AddPage();

           $pdf::writeHTML($html, true, false, true, false, '');

           $pdf::Output(public_path($filename), 'F');

           return response()->download(public_path($filename));
        }
    }

    public function PrintBorrowNote($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = Delivery_noteModel
            ::join('item borrowing as b', 'delivery note.id', '=', 'b.id_delivery_note')
            ->join('employee as emp', 'b.user_id', '=', 'emp.id')
            ->join('item_card as i', 'delivery note.id', '=', 'i.del_id')
            ->join('item as it', 'i.item_id', '=', 'it.id')
            ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
            ->select(
                'i.date',
                'i.quantity',
                'w.id',
                'w.name as warehouse_name',
                'delivery note.type as type',
                'delivery note.picture',
                'delivery note.order_id as order_id',
                'emp.f_name as emp_f_name',
                'emp.l_name as emp_l_name',
                'delivery note.id as delivery_note_id',
                'it.name'
            )
            ->where('delivery note.id',$id)->where('delivery note.type', 'borrow')->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
        //return 'none';
           // return response()->json(['status' => 'success', 'message' => $all]);
            $name='borrow Note '.$all[0]->emp_f_name.' '.$all[0]->emp_l_name;
        //    $filename = $name.'pdf';
        $filename = 'Borrownote.pdf';
           $view = View::make('borrowNote', ['results' => $all]);
           $html = $view->render();
           $pdf = new TCPDF;
           $pdf::SetTitle('borrow Note');
           $pdf::SetFont('aealarabiya', '', 18);
           $pdf::setRTL(true);
           $pdf::AddPage();
           $pdf::writeHTML($html, true, false, true, false, '');
           $pdf::Output(public_path($filename), 'F');

           return response()->download(public_path($filename));
        }
    }

    public function PrintTransferNote($id,$type,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
           if($type=='receipt')
           {
            $all = Receipt_noteModel
            ::join('transfer as t', 'receipt note.id', '=', 't.note_id')
            ->join('warehouse as other_warehouse','t.from_warehouse_id','=','other_warehouse.id')
            ->join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
            ->join('item as it', 'i.item_id', '=', 'it.id')
            ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
            ->select(
                'i.date',
                'i.quantity',
                'w.id',
                'w.name as warehouse_name',
                'receipt note.type',
                'receipt note.picture',
                'receipt note.order_id',
                'other_warehouse.name as other_warehouse',
                'receipt note.id as note_id',
                'it.name',
                'w.name as warehouse_name'
            )
            ->where('receipt note.id',$id)->where('receipt note.type', 'transfer')->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
           }
           else if($type=='delivery')
           {
            $all = Delivery_noteModel
            ::join('transfer to warehouse as t', 'delivery note.id', '=', 't.id_delivery_note')
                    ->join('warehouse as other_warehouse','t.to_warehouse_id','=','other_warehouse.id')
                    ->join('item_card as i', 'delivery note.id', '=', 'i.del_id')
                    ->join('item as it', 'i.item_id', '=', 'it.id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
            ->select(
                'i.date',
                'i.quantity',
                'i.warehouse_id',
                'delivery note.type',
                'delivery note.picture',
                'delivery note.order_id',
                'd.name_of_destination',
                'delivery note.id as note_id',
                'it.name'
            )
            ->where('delivery note.id',$id)->where('delivery note.type', 'delivery')
            ->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
           }
           $result=[
            'type'=>$type,
            'all'=>$all
           ];
           $filename = 'transferNote.pdf';
           $view = View::make('transferNote', ['results' => $result]);
           $html = $view->render();

           $pdf = new TCPDF;

           $pdf::SetTitle('transfer Note');
           $pdf::SetFont('aealarabiya', '', 18);
           $pdf::setRTL(true);
           $pdf::AddPage();

           $pdf::writeHTML($html, true, false, true, false, '');

           $pdf::Output(public_path($filename), 'F');

           return response()->download(public_path($filename));
        }
    }

    public function GetDeliveryNote($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = Delivery_noteModel
            ::join('delivery as d', 'delivery note.id', '=', 'd.delivery_note_id')
            ->join('item_card as i', 'delivery note.id', '=', 'i.del_id')
            ->join('item as it', 'i.item_id', '=', 'it.id')
            ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
            ->select(
                'i.date',
                'i.quantity',
                'w.id',
                'w.name as warehouse_name',
                'delivery note.type',
                'delivery note.picture',
                'delivery note.order_id',
                'd.name_of_destination',
                'delivery note.id as delivery_note_id',
                'it.name'
            )
            ->where('delivery note.id',$id)
            ->where('delivery note.type', 'delivery')
            ->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
           // return response()->json(['status' => 'success', 'message' => $all]);


           return response()->json(['status' => 'success', 'message' => $all]);
        }
    }

    public function GetBorrowNote($id,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            $all = Delivery_noteModel
            ::join('item borrowing as b', 'delivery note.id', '=', 'b.id_delivery_note')
            ->join('employee as emp', 'b.user_id', '=', 'emp.id')
            ->join('item_card as i', 'delivery note.id', '=', 'i.del_id')
            ->join('item as it', 'i.item_id', '=', 'it.id')
            ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
            ->select(
                'i.date',
                'i.quantity',
                'w.id',
                'w.name as warehouse_name',
                'delivery note.type as type',
                'delivery note.picture',
                'delivery note.order_id as order_id',
                'emp.f_name as emp_f_name',
                'emp.l_name as emp_l_name',
                'delivery note.id as delivery_note_id',
                'it.name'
            )
            ->where('delivery note.id',$id)->where('delivery note.type', 'borrow')->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
        //return 'none';
            return response()->json(['status' => 'success', 'message' => $all]);
           // $name='borrow Note '.$all[0]->emp_f_name.' '.$all[0]->emp_l_name;

          // return response()->download(public_path($all));
        }
    }

    public function GetTransferNote($id,$type,Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $emp_id = $emp->id;
            $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
           if($type=='receipt')
           {
            $all = Receipt_noteModel
            ::join('transfer as t', 'receipt note.id', '=', 't.note_id')
            ->join('warehouse as other_warehouse','t.from_warehouse_id','=','other_warehouse.id')
            ->join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
            ->join('item as it', 'i.item_id', '=', 'it.id')
            ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
            ->select(
                'i.date',
                'i.quantity',
                'w.id',
                'w.name as warehouse_name',
                'receipt note.type',
                'receipt note.picture',
                'receipt note.order_id',
                'other_warehouse.name as other_warehouse',
                'receipt note.id as note_id',
                'it.name',
                'w.name as warehouse_name'
            )
            ->where('receipt note.id',$id)->where('receipt note.type', 'transfer')->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
           }
           else if($type=='delivery')
           {
            $all = Delivery_noteModel
            ::join('transfer to warehouse as t', 'delivery note.id', '=', 't.id_delivery_note')
                    ->join('warehouse as other_warehouse','t.to_warehouse_id','=','other_warehouse.id')
                    ->join('item_card as i', 'delivery note.id', '=', 'i.del_id')
                    ->join('item as it', 'i.item_id', '=', 'it.id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
            ->select(
                'i.date',
                'i.quantity',
                'i.warehouse_id',
                'delivery note.type',
                'delivery note.picture',
                'delivery note.order_id',
                'd.name_of_destination',
                'delivery note.id as note_id',
                'it.name'
            )
            ->where('delivery note.id',$id)->where('delivery note.type', 'delivery')
            ->Where('i.warehouse_id', $warehouse_id->warehouse_id)->get();
           }
           $result=[
            'type'=>$type,
            'all'=>$all
           ];
           return response()->json(['status' => 'success', 'message' => $all]);
        }
    }

    public function OutputReport(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $role_id=$emp->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            if($role_name->name=='مدير المستودعات' || $role_name->name=="موظف مستودع" || $role_name->name=="أمين مستودع")
            {
                $emp_id = $emp->id;
                $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
             //   echo $warehouse_id->warehouse_id;
                $all = Delivery_noteModel
                    ::join('item_card as i', 'delivery note.id', '=', 'i.del_id')
                    ->join('item as it', 'i.item_id', '=', 'it.id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                    ->select(
                        'i.date',
                        'i.quantity',
                        'w.id',
                        'w.name as warehouse_name',
                        'delivery note.type',
                        'delivery note.picture',
                        'delivery note.order_id',
                        'delivery note.id as delivery_note_id',
                        'it.name as item_name'
                    )
                    ->Where('i.warehouse_id', $warehouse_id->warehouse_id)->where('i.type','output')->get();
                    //return response()->json(['status' => 'success', 'message' => $all]);
                }
                elseif($role_name->name=='مدير النظام')
                {
                    $emp_id = $emp->id;
                    $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                    $all = Delivery_noteModel
                        ::join('item_card as i', 'delivery note.id', '=', 'i.del_id')
                        ->join('item as it', 'i.item_id', '=', 'it.id')
                        ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                        ->select(
                            'i.date',
                            'i.quantity',
                            'w.id',
                            'w.name as warehouse_name',
                            'delivery note.type',
                            'delivery note.picture',
                            'delivery note.order_id',

                            'delivery note.id as delivery_note_id',
                            'it.name as item_name'
                        )->where('i.type','output')
                        ->get();
                     //return response()->json(['status' => 'success', 'message' => $all]);
                }
                $filename = 'OutPutReport.pdf';
                $view = View::make('OutputReport', ['results' => $all]);
                $html = $view->render();

                $pdf = new TCPDF;

                $pdf::SetTitle('OutPut Report');
                $pdf::SetFont('aealarabiya', '', 18);
                $pdf::setRTL(true);
                $pdf::AddPage();

                $pdf::writeHTML($html, true, false, true, false, '');

                $pdf::Output(public_path($filename), 'F');

                return response()->download(public_path($filename));

        }
    }

    public function GetOutputs(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $role_id=$emp->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            if($role_name->name=='مدير المستودعات' || $role_name->name=="موظف مستودع" || $role_name->name=="أمين مستودع")
            {
                $emp_id = $emp->id;
                $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
            //    echo $warehouse_id->warehouse_id;
                $all = Delivery_noteModel
                    ::join('item_card as i', 'delivery note.id', '=', 'i.del_id')
                    ->join('item as it', 'i.item_id', '=', 'it.id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                    ->select(
                        'i.date',
                        'i.quantity',
                        'w.id',
                        'w.name as warehouse_name',
                        'delivery note.type',
                        'delivery note.picture',
                        'delivery note.order_id',
                        'delivery note.id as delivery_note_id',
                        'it.name as item_name'
                    )
                    ->Where('i.warehouse_id', $warehouse_id->warehouse_id)->where('i.type','output')->get();
                    return response()->json(['status' => 'success', 'message' => $all]);
                }
                elseif($role_name->name=='مدير النظام')
                {
                    $emp_id = $emp->id;
                    $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                    $all = Delivery_noteModel
                        ::join('item_card as i', 'delivery note.id', '=', 'i.del_id')
                        ->join('item as it', 'i.item_id', '=', 'it.id')
                        ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                        ->select(
                            'i.date',
                            'i.quantity',
                            'w.id',
                            'w.name as warehouse_name',
                            'delivery note.type',
                            'delivery note.picture',
                            'delivery note.order_id',
                            'delivery note.id as delivery_note_id',
                            'it.name as item_name'
                        )->where('i.type','output')
                        ->get();
                     return response()->json(['status' => 'success', 'message' => $all]);
                }

        }
    }

    public function InputReport(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $role_id=$emp->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();
         //  echo $role_name->name;
           if($role_name->name=='مدير المستودعات' || $role_name->name=="موظف مستودع" || $role_name->name=="أمين مستودع")
           {
          //  echo 'kk';
                $emp_id = $emp->id;
                $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                $all = Receipt_noteModel
                    ::join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                    ->join('item as it', 'i.item_id', '=', 'it.id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                    ->select(
                        'i.date',
                        'i.quantity',
                        'w.id',
                        'w.name as warehouse_name',
                        'receipt note.type',
                        'receipt note.picture',
                        'receipt note.order_id',
                        'receipt note.id as receipt_note_id',
                        'it.name as item_name'
                    )
                    ->Where('i.warehouse_id', $warehouse_id->warehouse_id)->where('i.type','input')->get();
                   // return response()->json(['status' => 'success', 'message' => $all]);
                }
                elseif($role_name->name=='مدير النظام')
                {
                   // echo 'kk1';
                    $emp_id = $emp->id;
                    $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                    $all = Receipt_noteModel
                    ::join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                    ->join('item as it', 'i.item_id', '=', 'it.id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                    ->select(
                        'i.date',
                        'i.quantity',
                        'w.id',
                        'w.name as warehouse_name',
                        'receipt note.type',
                        'receipt note.picture',
                        'receipt note.order_id',
                        'receipt note.id as receipt_note_id',
                        'it.name as item_name'
                    )->where('i.type','input')
                        ->get();
                    // return response()->json(['status' => 'success', 'message' => $all]);
                }

                $filename = 'InPutReport.pdf';
                $view = View::make('InputReport', ['results' => $all]);
                $html = $view->render();

                $pdf = new TCPDF;

                $pdf::SetTitle('InPut Report');
                $pdf::SetFont('aealarabiya', '', 18);
                $pdf::setRTL(true);
                $pdf::AddPage();

                $pdf::writeHTML($html, true, false, true, false, '');

                $pdf::Output(public_path($filename), 'F');

                return response()->download(public_path($filename));
        }
    }

    public function GetInputs(Request $req)
    {
        $token = $req->bearerToken();
        $emp = EmployeesModel::select('token', 'id','role_id')->where('token', $token)->first();

        if ($emp == null)
            return response()->json(['status' => "error", 'message' => "عذراً أنت غير مسجل"]);
        else {
            $role_id=$emp->role_id;
            $role_name=rolesModel::where('id',$role_id)->first();

            if($role_name->name=='مدير المستودعات' || $role_name->name=="موظف مستودع" || $role_name->name=="أمين مستودع")
            {
                $emp_id = $emp->id;
                $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                $all = Receipt_noteModel
                    ::join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                    ->join('item as it', 'i.item_id', '=', 'it.id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                    ->select(
                        'i.date',
                        'i.quantity',
                        'w.id',
                        'w.name as warehouse_name',
                        'receipt note.type',
                        'receipt note.picture',
                        'receipt note.order_id',
                        'receipt note.id as receipt_note_id',
                        'it.name as item_name'
                    )
                    ->Where('i.warehouse_id', $warehouse_id->warehouse_id)->where('i.type','input')->get();
                    return response()->json(['status' => 'success', 'message' => $all]);
                }
                elseif($role_name->name=='مدير النظام')
                {
                    $emp_id = $emp->id;
                    $warehouse_id = EmployeesModel::select('warehouse_id')->where('id', $emp_id)->first();
                    $all = Receipt_noteModel
                    ::join('item_card as i', 'receipt note.id', '=', 'i.rec_id')
                    ->join('item as it', 'i.item_id', '=', 'it.id')
                    ->join('warehouse as w', 'i.warehouse_id', '=', 'w.id')
                    ->select(
                        'i.date',
                        'i.quantity',
                        'w.id',
                        'w.name as warehouse_name',
                        'receipt note.type',
                        'receipt note.picture',
                        'receipt note.order_id',
                        'receipt note.id as receipt_note_id',
                        'it.name as item_name'
                    )->where('i.type','input')
                        ->get();
                     return response()->json(['status' => 'success', 'message' => $all]);
                }


        }
    }

}
