<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'f_name' => 'saja',
            'l_name' => 'hasna',
            'phone' => '0965325465',
            'salary' => '50',
            'address' => 'damascus',
            'role_id' => '1',
            'emp_statu' => '1',
            'email' => 'super@gmail.com',
            'password' => '12345678',
            'warehouse_id'=>'8'
        ]);

        $role = Role::create(['name' => 'super_admin']);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);
    }
}
